use bitregister::{
	bitregister,
	bitvalue,
	prelude::{u24, BitRegister},
	repr::ReprCastSliceFrom
};

pub unsafe trait ReadableRegister: BitRegister<AddressRepr = u8>
where
	u8: ReprCastSliceFrom<Self::BitRepr>
{
}
pub unsafe trait WritableRegister: BitRegister<AddressRepr = u8>
where
	u8: ReprCastSliceFrom<Self::BitRepr>
{
}

bitregister! {
	pub bitregister(u16 @ 0x00: u8) ChipIdReg {
		@[shl = 0]
		pub id:
			@[mask = 0xFFFF]
			bitvalue(u16) ChipId {
				Chip0811 = 0x0811
			}
	}
}
unsafe impl ReadableRegister for ChipIdReg {}

bitregister! {
	pub bitregister(u16 @ 0x02: u8) IdVerReg {
		@[shl = 0]
		pub ver:
			@[mask = 0xFFFF]
			bitvalue(u16) IdVer {
				Ver03 = 0x03,
				Ver01 = 0x01
			}
	}
}
unsafe impl ReadableRegister for IdVerReg {}

bitregister! {
	pub bitregister(u8 @ 0x03: u8) ResetControl {
		@[shl = 1]
		pub soft_reset: bitvalue(u8) SoftReset {
			Disabled = 0b0,
			Reset = 0b1
		},
		@[shl = 0]
		pub hibernate: bitvalue(u8) Hibernate {
			Disabled = 0b0,
			Enabled = 0b1
		}
	}
}
unsafe impl ReadableRegister for ResetControl {}
unsafe impl WritableRegister for ResetControl {}

bitregister! {
	pub bitregister(u8 @ 0x04: u8) ClockControl {
		@[shl = 3]
		pub ts_off: bitvalue(u8) TemperatureSensorClock {
			Disabled = 0b1,
			Enabled = 0b0
		},
		@[shl = 2]
		pub gpio_off: bitvalue(u8) GpioClock {
			Disabled = 0b1,
			Enabled = 0b0
		},
		@[shl = 1]
		pub tsc_off: bitvalue(u8) TouchscreenClock {
			Disabled = 0b1,
			Enabled = 0b0
		},
		@[shl = 0]
		pub adc_off: bitvalue(u8) AdcClock {
			Disabled = 0b1,
			Enabled = 0b0
		}
	}
}
unsafe impl ReadableRegister for ClockControl {}
unsafe impl WritableRegister for ClockControl {}

bitregister! {
	pub bitregister(u8 @ 0x08: u8) SpiInterfaceConfiguration {
		@[shl = 2]
		pub auto_incr: bitvalue(u8) SpiAutoIncrement {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 1]
		pub spi_clk_mod1: bitvalue(u8) SpiClkMod1 {
			Down = 0b0,
			Up = 0b1
		},
		@[shl = 0]
		pub spi_clk_mod0: bitvalue(u8) SpiClkMod0 {
			Up = 0b1,
			Down = 0b0
		}
	}
}
unsafe impl ReadableRegister for SpiInterfaceConfiguration {}
unsafe impl WritableRegister for SpiInterfaceConfiguration {}

bitregister! {
	pub bitregister(u8 @ 0x09: u8) InterruptControl {
		@[shl = 2]
		pub int_polarity: bitvalue(u8) InterruptPinPolarity {
			ActiveLow = 0b0,
			ActiveHigh = 0b1
		},
		@[shl = 1]
		pub int_type: bitvalue(u8) InterruptType {
			Level = 0b0,
			Edge = 0b1
		},
		@[shl = 0]
		pub global_int: bitvalue(u8) GlobalInterrupts {
			Disabled = 0b0,
			Enabled = 0b1
		}
	}
}
unsafe impl ReadableRegister for InterruptControl {}
unsafe impl WritableRegister for InterruptControl {}

bitregister! {
	pub bitregister(u8 @ 0x0A: u8) InterruptEnable {
		@[shl = 7]
		pub gpio: bitvalue(u8) GpioInterrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 6]
		pub adc: bitvalue(u8) AdcInterrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 5]
		pub temp_sens: bitvalue(u8) TemperatureSensorInterrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 4]
		pub fifo_empty: bitvalue(u8) TouchFifoEmptyInterrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 3]
		pub fifo_full: bitvalue(u8) TouchFifoFullInterrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 2]
		pub fifo_oflow: bitvalue(u8) TouchFifoOverflowInterrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 1]
		pub fifo_th: bitvalue(u8) TouchFifoThresholdInterrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 0]
		pub touch_det: bitvalue(u8) TouchDetectInterrupts {
			Disabled = 0b0,
			Enabled = 0b1
		}
	}
}
unsafe impl ReadableRegister for InterruptEnable {}
unsafe impl WritableRegister for InterruptEnable {}

bitregister! {
	pub bitregister(u8 @ 0x0B: u8) InterruptStatus {
		@[shl = 7]
		pub gpio: bitvalue(u8) GpioAnyInterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 6]
		pub adc: bitvalue(u8) AdcAnyInterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 5]
		pub temp_sens: bitvalue(u8) TemperatureSensorInterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 4]
		pub fifo_empty: bitvalue(u8) TouchFifoEmptyStatus {
			Active = 0b1,
			Inactive = 0b0
		},
		@[shl = 3]
		pub fifo_full: bitvalue(u8) TouchFifoFullStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 2]
		pub fifo_oflow: bitvalue(u8) TouchFifoOverflowStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 1]
		pub fifo_th: bitvalue(u8) TouchFifoThresholdStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 0]
		pub touch_det: bitvalue(u8) TouchDetectInterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		}
	}
}
unsafe impl ReadableRegister for InterruptStatus {}
unsafe impl WritableRegister for InterruptStatus {}

bitregister! {
	pub bitregister(u8 @ 0x0C: u8) GpioInterruptEnable {
		@[shl = 7]
		pub ieg7: bitvalue(u8) Gpio7Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 6]
		pub ieg6: bitvalue(u8) Gpio6Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 5]
		pub ieg5: bitvalue(u8) Gpio5Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 4]
		pub ieg4: bitvalue(u8) Gpio4Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 3]
		pub ieg3: bitvalue(u8) Gpio3Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 2]
		pub ieg2: bitvalue(u8) Gpio2Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 1]
		pub ieg1: bitvalue(u8) Gpio1Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 0]
		pub ieg0: bitvalue(u8) Gpio0Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		}
	}
}
unsafe impl ReadableRegister for GpioInterruptEnable {}
unsafe impl WritableRegister for GpioInterruptEnable {}
bitregister! {
	pub bitregister(u8 @ 0x0D: u8) GpioInterruptStatus {
		@[shl = 7]
		pub isg7: bitvalue(u8) Gpio7InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 6]
		pub isg6: bitvalue(u8) Gpio6InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 5]
		pub isg5: bitvalue(u8) Gpio5InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 4]
		pub isg4: bitvalue(u8) Gpio4InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 3]
		pub isg3: bitvalue(u8) Gpio3InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 2]
		pub isg2: bitvalue(u8) Gpio2InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 1]
		pub isg1: bitvalue(u8) Gpio1InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 0]
		pub isg0: bitvalue(u8) Gpio0InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		}
	}
}
unsafe impl ReadableRegister for GpioInterruptStatus {}

bitregister! {
	pub bitregister(u8 @ 0x0E: u8) AdcInterruptEnable {
		@[shl = 7]
		pub ieac7: bitvalue(u8) Adc7Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 6]
		pub ieac6: bitvalue(u8) Adc6Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 5]
		pub ieac5: bitvalue(u8) Adc5Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 4]
		pub ieac4: bitvalue(u8) Adc4Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 3]
		pub ieac3: bitvalue(u8) Adc3Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 2]
		pub ieac2: bitvalue(u8) Adc2Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 1]
		pub ieac1: bitvalue(u8) Adc1Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 0]
		pub ieac0: bitvalue(u8) Adc0Interrupts {
			Disabled = 0b0,
			Enabled = 0b1
		}
	}
}
unsafe impl ReadableRegister for AdcInterruptEnable {}
unsafe impl WritableRegister for AdcInterruptEnable {}
bitregister! {
	pub bitregister(u8 @ 0x0F: u8) AdcInterruptStatus {
		@[shl = 7]
		pub isa7: bitvalue(u8) Adc7InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 6]
		pub isa6: bitvalue(u8) Adc6InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 5]
		pub isa5: bitvalue(u8) Adc5InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 4]
		pub isa4: bitvalue(u8) Adc4InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 3]
		pub isa3: bitvalue(u8) Adc3InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 2]
		pub isa2: bitvalue(u8) Adc2InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 1]
		pub isa1: bitvalue(u8) Adc1InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		},
		@[shl = 0]
		pub isa0: bitvalue(u8) Adc0InterruptStatus {
			Inactive = 0b0,
			Active = 0b1
		}
	}
}
unsafe impl ReadableRegister for AdcInterruptStatus {}

bitregister! {
	pub bitregister(u8 @ 0x40: u8) TouchscreenControl {
		@[shl = 7]
		pub tsc_sta: bitvalue(u8) TouchStatus {
			TouchDetected = 0b1,
			Idle = 0b0
		},
		@[shl = 4]
		pub track: bitvalue(u8) TouchTrackingIndex {
			I4 = 0b001,
			I8,
			I16,
			I32,
			I64,
			I92,
			I127,
			NoTracking = 0b000
		},
		@[shl = 1]
		pub op_mod: bitvalue(u8) TouchMode {
			ModeXYZ = 0b000,
			ModeXY = 0b001,
			ModeX = 0b010,
			ModeY = 0b011,
			ModeZ = 0b100
		},
		@[shl = 0]
		pub en: bitvalue(u8) Touchscreen {
			Disabled = 0b0,
			Enabled = 0b1
		}
	}
}
unsafe impl ReadableRegister for TouchscreenControl {}
unsafe impl WritableRegister for TouchscreenControl {}

bitregister! {
	pub bitregister(u8 @ 0x41: u8) TouchscreenConfiguration {
		@[shl = 6]
		pub ave_ctrl: bitvalue(u8) TouchAverageSamples {
			Samples1 = 0b00,
			Samples2,
			Samples4,
			Samples8
		},
		@[shl = 3]
		pub touch_det_delay: bitvalue(u8) TouchDetectionDelay {
			Time10us = 0b000,
			Time50us,
			Time100us,
			Time500us,
			Time1ms,
			Time5ms,
			Time10ms,
			Time50ms
		},
		@[shl = 0]
		pub settling: bitvalue(u8) TouchSettlingTime {
			Time10us = 0b000,
			Time100us,
			Time500us,
			Time1ms,
			Time5ms,
			Time10ms,
			Time50ms,
			Time100ms,
		}
	}
}
unsafe impl ReadableRegister for TouchscreenConfiguration {}
unsafe impl WritableRegister for TouchscreenConfiguration {}

bitvalue! {
	pub bitvalue(u16) TouchCoordinateX {
		0 ..= 0xFFF
	}
}
bitvalue! {
	pub bitvalue(u16) TouchCoordinateY {
		0 ..= 0xFFF
	}
}
bitvalue! {
	pub bitvalue(u8) TouchCoordinateZ {
		0 ..= 0xFF
	}
}

bitregister! {
	@[reset = Self { tr_x: TouchCoordinateX::MAX }]
	pub bitregister(u16 @ 0x42: u8) TouchWindowTopRightX {
		@[shl = 0]
		pub tr_x: TouchCoordinateX
	}
}
unsafe impl ReadableRegister for TouchWindowTopRightX {}
unsafe impl WritableRegister for TouchWindowTopRightX {}

bitregister! {
	@[reset = Self { tr_y: TouchCoordinateY::MAX }]
	pub bitregister(u16 @ 0x44: u8) TouchWindowTopRightY {
		@[shl = 0]
		pub tr_y: TouchCoordinateY
	}
}
unsafe impl ReadableRegister for TouchWindowTopRightY {}
unsafe impl WritableRegister for TouchWindowTopRightY {}

bitregister! {
	pub bitregister(u16 @ 0x46: u8) TouchWindowBottomLeftX {
		@[shl = 0]
		pub bl_x: TouchCoordinateX
	}
}
unsafe impl ReadableRegister for TouchWindowBottomLeftX {}
unsafe impl WritableRegister for TouchWindowBottomLeftX {}

bitregister! {
	pub bitregister(u16 @ 0x48: u8) TouchWindowBottomLeftY {
		@[shl = 0]
		pub bl_y: TouchCoordinateY
	}
}
unsafe impl ReadableRegister for TouchWindowBottomLeftY {}
unsafe impl WritableRegister for TouchWindowBottomLeftY {}

bitvalue! {
	pub bitvalue(u8) TouchscreenFifoThreshold {
		1 ..= 255
	}
}
impl BitRegister for TouchscreenFifoThreshold {
	type AddressRepr = u8;

	const ADDRESS: Self::AddressRepr = 0x4A;

	fn reset_value() -> Self {
		Self::default()
	}
}
unsafe impl ReadableRegister for TouchscreenFifoThreshold {}
unsafe impl WritableRegister for TouchscreenFifoThreshold {}

bitvalue! {
	pub bitvalue(u8) TouchFifoReset {
		Disabled = 0b0,
		Reset = 0b1
	}
}
bitregister! {
	pub bitregister(u8 @ 0x4B: u8) TouchscreenFifoStatus {
		@[shl = 7]
		pub fifo_oflow: TouchFifoOverflowStatus,
		@[shl = 6]
		pub fifo_full: TouchFifoFullStatus,
		@[shl = 5]
		pub fifo_empty: TouchFifoEmptyStatus,
		@[shl = 4]
		pub fifo_th_trig: TouchFifoThresholdStatus,
		@[shl = 0]
		pub fifo_reset: TouchFifoReset
	}
}
unsafe impl ReadableRegister for TouchscreenFifoStatus {}
unsafe impl WritableRegister for TouchscreenFifoStatus {}

bitvalue! {
	pub bitvalue(u8) TouchFifoSize {
		0 ..= 0b0111_1111
	}
}
impl BitRegister for TouchFifoSize {
	type AddressRepr = u8;

	const ADDRESS: Self::AddressRepr = 0x4C;

	fn reset_value() -> Self {
		Self::default()
	}
}
unsafe impl ReadableRegister for TouchFifoSize {}

bitregister! {

	pub bitregister(u16 @ 0x4D: u8) TouchDataX {
		@[shl = 0]
		pub datax: TouchCoordinateX
	}
}
unsafe impl ReadableRegister for TouchDataX {}

bitregister! {
	pub bitregister(u16 @ 0x4F: u8) TouchDataY {
		@[shl = 0]
		pub datay: TouchCoordinateY
	}
}
unsafe impl ReadableRegister for TouchDataY {}

bitregister! {
	pub bitregister(u8 @ 0x51: u8) TouchDataZ {
		@[shl = 0]
		pub dataz: TouchCoordinateZ
	}
}
unsafe impl ReadableRegister for TouchDataZ {}

bitregister! {
	#[allow(non_camel_case_types)]
	pub bitregister(u24 @ 0xD7: u8) TouchDataXYZ_XY {
		@[shl = 12]
		pub datax: TouchCoordinateX,
		@[shl = 0]
		pub datay: TouchCoordinateY
	}
}
unsafe impl ReadableRegister for TouchDataXYZ_XY {}

bitregister! {
	#[allow(non_camel_case_types)]
	pub bitregister(u32 @ 0xD7: u8) TouchDataXYZ_XYZ {
		@[shl = 20]
		pub datax: TouchCoordinateX,
		@[shl = 8]
		pub datay: TouchCoordinateY,
		@[shl = 0]
		pub dataz: TouchCoordinateZ
	}
}
unsafe impl ReadableRegister for TouchDataXYZ_XYZ {}

bitregister! {
	pub bitregister(u8 @ 0x56: u8) TouchFractionControl {
		@[shl = 0]
		pub fraction_z: bitvalue(u8) TouchFractionZ {
			Fraction0Whole8 = 0b000,
			Fraction1Whole7,
			Fraction2Whole6,
			Fraction3Whole5,
			Fraction4Whole4,
			Fraction5Whole3,
			Fraction6Whole2,
			Fraction7Whole1
		}
	}
}
unsafe impl WritableRegister for TouchFractionControl {}

#[cfg(test)]
mod test {
	use bitregister::prelude::BitValue;

	use super::*;

	#[test]
	fn test_register_defaults() {
		assert_eq!(ChipIdReg::reset_value().to_bits(), 0x0811);
		assert_eq!(IdVerReg::reset_value().to_bits(), 0x0003);
		assert_eq!(ResetControl::reset_value().to_bits(), 0x00);
		assert_eq!(ClockControl::reset_value().to_bits(), 0x0F);
		assert_eq!(SpiInterfaceConfiguration::reset_value().to_bits(), 0x01);
		assert_eq!(InterruptControl::reset_value().to_bits(), 0x00);
		assert_eq!(InterruptEnable::reset_value().to_bits(), 0x00);
		assert_eq!(InterruptStatus::reset_value().to_bits(), 0x10);
		assert_eq!(GpioInterruptEnable::reset_value().to_bits(), 0x00);
		assert_eq!(GpioInterruptStatus::reset_value().to_bits(), 0x00);
		assert_eq!(AdcInterruptEnable::reset_value().to_bits(), 0x00);
		assert_eq!(AdcInterruptStatus::reset_value().to_bits(), 0x00);

		assert_eq!(TouchWindowTopRightX::reset_value().to_bits(), 0x0FFF);
		assert_eq!(TouchWindowTopRightY::reset_value().to_bits(), 0x0FFF);
		assert_eq!(TouchWindowBottomLeftX::reset_value().to_bits(), 0x0000);
		assert_eq!(TouchWindowBottomLeftY::reset_value().to_bits(), 0x0000);
		assert_eq!(TouchWindowBottomLeftY::reset_value().to_bits(), 0x0000);

		// caveat: The datasheet specifies the reset value here as 0x00, but also says that
		// the field must not be set to value 0x00 - thus the default is 0x01 instead
		assert_eq!(TouchscreenFifoThreshold::reset_value().to_bits(), 0x01);

		assert_eq!(TouchscreenFifoStatus::reset_value().to_bits(), 0x20);
		assert_eq!(TouchFifoSize::reset_value().to_bits(), 0x00);
		assert_eq!(TouchDataX::reset_value().to_bits(), 0x00);
		assert_eq!(TouchDataY::reset_value().to_bits(), 0x00);
		assert_eq!(TouchDataZ::reset_value().to_bits(), 0x00);
	}
}
