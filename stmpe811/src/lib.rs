#![no_std]

use core::mem::MaybeUninit;

use displaydoc::Display;

use bitregister::{
	prelude::BitValue,
	register::access::{
		ResourceAccessBase,
		ResourceAccessRead,
		ResourceAccessReadError,
		ResourceAccessWrite,
		ResourceAccessWriteError
	},
	repr::{endian::BigEndian, ReprCastSliceFrom}
};

pub mod commands;
pub use commands::*;

#[derive(Debug, Display)]
pub enum DriverError {
	/// failed to read from i2c: {0}
	ResourceAccessReadError(ResourceAccessReadError),
	/// failed to write to i2c: {0}
	ResourceAccessWriteError(ResourceAccessWriteError)
}
impl From<ResourceAccessReadError> for DriverError {
	fn from(err: ResourceAccessReadError) -> Self {
		DriverError::ResourceAccessReadError(err)
	}
}
impl From<ResourceAccessWriteError> for DriverError {
	fn from(err: ResourceAccessWriteError) -> Self {
		DriverError::ResourceAccessWriteError(err)
	}
}

pub struct ExpanderDriver<
	Access: ResourceAccessBase<AddressRepr = u8, WordRepr = u8> + ResourceAccessRead + ResourceAccessWrite
> {
	access: Access
}
impl<
		Access: ResourceAccessBase<AddressRepr = u8, WordRepr = u8>
			+ ResourceAccessRead
			+ ResourceAccessWrite
	> ExpanderDriver<Access>
{
	pub unsafe fn new(access: Access) -> Self {
		ExpanderDriver { access }
	}

	pub fn initialize(&mut self) -> Result<(), DriverError> {
		match self.read::<ChipIdReg>()? {
			ChipIdReg {
				id: ChipId::Chip0811
			} => ()
		};

		self.write(ResetControl::new(SoftReset::Reset, Hibernate::Disabled))?;
		self.write(ClockControl::new(
			TemperatureSensorClock::Disabled,
			GpioClock::Disabled,
			TouchscreenClock::Enabled,
			AdcClock::Enabled
		))?;

		self.write(TouchscreenControl::new(
			TouchStatus::Idle,
			TouchTrackingIndex::I64,
			TouchMode::ModeXY,
			Touchscreen::Disabled
		))?;
		self.write(TouchscreenConfiguration::new(
			TouchAverageSamples::Samples8,
			TouchDetectionDelay::Time100us,
			TouchSettlingTime::Time100us
		))?;
		self.write(TouchscreenFifoThreshold::from_bits(32).unwrap())?;
		self.write(TouchscreenControl::new(
			TouchStatus::Idle,
			TouchTrackingIndex::I64,
			TouchMode::ModeXY,
			Touchscreen::Enabled
		))?;
		self.write(TouchFractionControl::new(TouchFractionZ::Fraction6Whole2))?;

		Ok(())
	}

	pub fn read<R: ReadableRegister>(&mut self) -> Result<R, DriverError>
	where
		u8: ReprCastSliceFrom<R::BitRepr>
	{
		match unsafe { self.access.read_register_once::<R, BigEndian>()? } {
			Some(v) => Ok(v),
			None => panic!("Could not parse register {:?}", core::any::type_name::<R>())
		}
	}

	pub fn read_slice<'dst, R: ReadableRegister, const BUF_LEN: usize>(
		&mut self,
		dst: &'dst mut [MaybeUninit<R>]
	) -> Result<&'dst mut [R], DriverError>
	where
		u8: ReprCastSliceFrom<R::BitRepr>
	{
		unsafe {
			self.access
				.read_register::<R, BigEndian, BUF_LEN>(dst)
				.map_err(DriverError::from)
		}
	}

	pub fn write<R: WritableRegister>(&mut self, value: R) -> Result<(), DriverError>
	where
		u8: ReprCastSliceFrom<R::BitRepr>
	{
		unsafe {
			self.access
				.write_register_once::<R, BigEndian>(value)
				.map_err(DriverError::from)
		}
	}

	pub fn access_mut(&mut self) -> &mut Access {
		&mut self.access
	}
}
