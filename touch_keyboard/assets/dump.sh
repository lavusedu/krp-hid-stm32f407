#!/bin/sh

bits=$(xxd -b -s 9 -c 3 "$1" | awk '{ print "0b"$2", 0b"$3", 0b"$4"," }')

printf '#[rustfmt::skip]\n'
printf "const IMAGE: &'static [u8] = &[\n"
printf '%s\n' "$bits"
printf '];\n'
