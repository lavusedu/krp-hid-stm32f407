#![no_std]
#![no_main]

#[cfg(feature = "semihosting")]
use defmt_rtt as _;
#[cfg(feature = "semihosting")]
use panic_probe as _;

#[cfg(not(feature = "semihosting"))]
use panic_halt as _;

mod pins;
mod presenter;
mod screen;
mod touch;
mod usb;

#[macro_use]
pub mod util;

#[rtic::app(
	device = stm32f4xx_hal::pac,
	dispatchers = [EXTI0, EXTI1, EXTI3]
)]
mod app {
	use stm32f4xx_hal::{delay::Delay, prelude::*, timer::Timer};

	use crate::{
		pins::{InputButtons, OutputLeds},
		presenter::{DrawUpdate, LedUpdate, PresenterEvent, PresenterStateMachine},
		screen::{FsmcPins, ScreenDriver},
		touch::TouchDriver,
		usb::{report::TouchKeyboardReport, UsbDriver},
		util::time::Duration
	};

	#[monotonic(binds = TIM2, default = true)]
	type MicrosecMono = crate::util::time::MicrosecMonotonicTimer;

	#[shared]
	struct Shared {
		usb_driver: UsbDriver,
		presenter: PresenterState
	}

	#[local]
	struct Local {
		input_buttons: InputButtons,
		touch_driver: TouchDriver,
		screen_driver: ScreenDriver,
		output_leds: OutputLeds
	}

	#[init]
	fn init(ctx: init::Context) -> (Shared, Local, init::Monotonics) {
		#[cfg(feature = "semihosting")]
		defmt::info!("Running init...");

		let cp = ctx.core;
		let dp = ctx.device;

		let mut syscfg = dp.SYSCFG.constrain();

		let rcc = dp.RCC.constrain();
		let clocks = rcc
			.cfgr
			.sysclk(48.mhz())
			.hclk(48.mhz())
			.pclk1(24.mhz())
			.pclk2(24.mhz())
			.freeze();

		let mono = Timer::new(dp.TIM2, &clocks).monotonic();
		let mut delay = Delay::new(cp.SYST, &clocks);
		let mut interrupt_controller = dp.EXTI;

		let gpio_a = dp.GPIOA.split();
		let gpio_b = dp.GPIOB.split();
		let gpio_c = dp.GPIOC.split();
		let gpio_d = dp.GPIOD.split();
		let gpio_e = dp.GPIOE.split();
		let gpio_f = dp.GPIOF.split();
		let gpio_g = dp.GPIOG.split();
		let gpio_i = dp.GPIOI.split();

		let mut output_leds =
			OutputLeds::initialize(gpio_g.pg6, gpio_g.pg8, gpio_i.pi9, gpio_c.pc7);
		output_leds.all_high();

		// SAFETY: init is only run once
		let usb_driver = unsafe {
			UsbDriver::initialize(
				clocks,
				dp.OTG_FS_GLOBAL,
				dp.OTG_FS_DEVICE,
				dp.OTG_FS_PWRCLK,
				gpio_a.pa11,
				gpio_a.pa12
			)
		};

		let screen_driver = ScreenDriver::initialize(
			dp.FSMC,
			FsmcPins {
				pd14: gpio_d.pd14,
				pd15: gpio_d.pd15,
				pd0: gpio_d.pd0,
				pd1: gpio_d.pd1,
				pe7: gpio_e.pe7,
				pe8: gpio_e.pe8,
				pe9: gpio_e.pe9,
				pe10: gpio_e.pe10,
				pe11: gpio_e.pe11,
				pe12: gpio_e.pe12,
				pe13: gpio_e.pe13,
				pe14: gpio_e.pe14,
				pe15: gpio_e.pe15,
				pd8: gpio_d.pd8,
				pd9: gpio_d.pd9,
				pd10: gpio_d.pd10,
				pf0: gpio_f.pf0,
				pd4: gpio_d.pd4,
				pd5: gpio_d.pd5,
				pg10: gpio_g.pg10,
				pg12: gpio_g.pg12,
				pi8: gpio_i.pi8
			},
			&mut delay
		);

		let touch_driver = TouchDriver::initialize(
			&mut syscfg,
			clocks,
			&mut interrupt_controller,
			dp.I2C1,
			gpio_b.pb6,
			gpio_b.pb9,
			gpio_i.pi2
		);

		let input_buttons =
			InputButtons::initialize(&mut syscfg, &mut interrupt_controller, gpio_g.pg15);

		let presenter = PresenterState::initialize();

		output_leds.all_low();

		#[cfg(feature = "semihosting")]
		defmt::info!("Init done");
		(
			Shared {
				usb_driver,
				presenter
			},
			Local {
				input_buttons,
				touch_driver,
				screen_driver,
				output_leds
			},
			init::Monotonics(mono)
		)
	}

	#[idle]
	fn idle(_ctx: idle::Context) -> ! {
		loop {}
	}

	// USB //

	#[task(
		binds = OTG_FS,
		priority = 2
	)]
	fn usb_interrupt(_ctx: usb_interrupt::Context) {
		let _ = usb_poll::spawn();
	}

	task_timeout! {
		struct UsbTimeout {
			task = usb_timeout;
			timeout = Duration::millis(10);
		}
	}
	#[task(priority = 2)]
	fn usb_timeout(_ctx: usb_timeout::Context) {
		let _ = usb_poll::spawn();
	}

	#[task(
		priority = 3,
		shared = [usb_driver, presenter],
		local = [
			timeout: UsbTimeout = UsbTimeout::new()
		]
	)]
	fn usb_poll(mut ctx: usb_poll::Context) {
		ctx.local.timeout.cancel();

		let result = ctx.shared.usb_driver.lock(|usb_driver| usb_driver.poll());

		match result {
			Ok(None) | Err(usb_device::UsbError::WouldBlock) => (),
			Err(err) => panic!("usb error: {:?}", err),
			Ok(Some(report)) => match report {
				TouchKeyboardReport::KeyboardOutput(output) => {
					ctx.shared
						.presenter
						.lock(|presenter| presenter.on_event(PresenterEvent::UsbUpdate(output)));
				}
				_ => ()
			}
		}

		ctx.local.timeout.schedule().unwrap();
	}

	#[task(
		priority = 4,
		shared = [usb_driver]
	)]
	fn usb_output(mut ctx: usb_output::Context, output: TouchKeyboardReport) {
		let result = ctx
			.shared
			.usb_driver
			.lock(|usb_driver| usb_driver.push_report(output));

		match result {
			Ok(()) | Err(usb_device::UsbError::WouldBlock) => (),
			Err(err) => panic!("usb error: {:?}", err)
		}
	}

	// INPUT //

	task_cooldown! {
		struct InputButtonsCooldown {
			cooldown = Duration::millis(500);
		}
	}

	#[task(
		binds = EXTI15_10,
		priority = 3,
		shared = [presenter],
		local = [
			cooldown: InputButtonsCooldown = InputButtonsCooldown::new(),
			input_buttons
		]
	)]
	fn input_buttons_interrupt(mut ctx: input_buttons_interrupt::Context) {
		ctx.local.input_buttons.clear_interrupt();

		let instant = monotonics::now();
		if !ctx.local.cooldown.check(instant) {
			return
		}

		ctx.shared
			.presenter
			.lock(|presenter| presenter.on_event(PresenterEvent::ModeSwitch));
	}

	#[task(
		binds = EXTI2,
		priority = 3,
		shared = [presenter],
		local = [touch_driver]
	)]
	fn touch_interrupt(mut ctx: touch_interrupt::Context) {
		let driver = ctx.local.touch_driver;
		driver.clear_interrupt();

		let instant = monotonics::now();
		for event in driver.poll(instant) {
			ctx.shared
				.presenter
				.lock(|presenter| presenter.on_event(PresenterEvent::TouchEvent(event)));
		}
	}

	// TODO: Handle timeouts? No animations for now, so no need for timeouts?
	#[task(
		priority = 3,
		shared = [presenter]
	)]
	fn presenter_timeout(mut _ctx: presenter_timeout::Context) {
		// defmt::println!("presenter_timeout");
		// ctx.shared.presenter.lock(
		// 	|presenter| presenter.on_event(
		// 		PresenterEvent::Timeout
		// 	)
		// );
	}

	// OUTPUT //

	#[task(
		priority = 2,
		local = [screen_driver]
	)]
	fn screen_draw(ctx: screen_draw::Context, update: DrawUpdate) {
		ctx.local.screen_driver.on_update(update);
	}

	#[task(
		priority = 4,
		local = [output_leds]
	)]
	fn output_leds(ctx: output_leds::Context, update: LedUpdate) {
		ctx.local.output_leds.from_led_update(update);
	}

	// PRESENTER //

	task_timeout! {
		struct PresenterTimeout {
			task = presenter_timeout;
			timeout = Duration::millis(16);
		}
	}
	task_timeout! {
		struct DrawTimeout {
			task = screen_draw;
			timeout = Duration::millis(8);
			args = (draw_update: DrawUpdate);
		}
	}
	pub struct PresenterState {
		state_machine: PresenterStateMachine,
		timeout: PresenterTimeout,
		draw_timeout: DrawTimeout
	}
	impl PresenterState {
		pub fn initialize() -> Self {
			PresenterState {
				state_machine: PresenterStateMachine::new(),
				timeout: PresenterTimeout::new(),
				draw_timeout: DrawTimeout::new()
			}
		}

		pub fn on_event(&mut self, event: PresenterEvent) {
			self.timeout.cancel();

			let instant = monotonics::now();
			let response = self.state_machine.on_event(instant, event);

			if let Some(draw_update) = response.draw_update {
				self.draw_timeout.cancel();
				let _ = self.draw_timeout.schedule(draw_update);
			}

			if let Some(led_update) = response.led_update {
				match output_leds::spawn(led_update) {
					Ok(_) => (),
					Err(_) => panic!("Could not spawn output_leds")
				}
			}

			if let Some(usb_update) = response.usb_update {
				match usb_output::spawn(usb_update) {
					Ok(_) => (),
					Err(_) => panic!("Could not spawn usb_output")
				}
			}

			self.timeout.schedule().unwrap();
		}
	}
}
