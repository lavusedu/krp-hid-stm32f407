use core::mem::MaybeUninit;

use stm32f4xx_hal::{
	gpio::{
		gpioa::{PA11, PA12},
		Floating,
		Input
	},
	otg_fs::{UsbBus, USB},
	pac::{OTG_FS_DEVICE, OTG_FS_GLOBAL, OTG_FS_PWRCLK},
	rcc::Clocks
};

use usb_device::{
	bus::UsbBusAllocator,
	device::{UsbDevice, UsbDeviceBuilder, UsbVidPid},
	UsbError
};

use usb_hid::HidUsbClass;

pub mod report;

struct UsbDriverStatic {
	pub allocator_memory: [u32; 512],
	pub allocator: MaybeUninit<UsbBusAllocator<UsbBus<USB>>>
}
static mut USB_DRIVER_STATIC: UsbDriverStatic = UsbDriverStatic {
	allocator_memory: [0; 512],
	allocator: MaybeUninit::uninit()
};

pub struct UsbDriver {
	device: UsbDevice<'static, UsbBus<USB>>,
	hid_class: HidUsbClass<'static, UsbBus<USB>, report::TouchKeyboardReport>
}
impl UsbDriver {
	#[allow(unused_unsafe)]
	pub unsafe fn initialize(
		clocks: Clocks,
		usb_global: OTG_FS_GLOBAL,
		usb_device: OTG_FS_DEVICE,
		usb_pwrclk: OTG_FS_PWRCLK,
		pa11: PA11<Input<Floating>>,
		pa12: PA12<Input<Floating>>
	) -> Self {
		let usb = USB {
			usb_global,
			usb_device,
			usb_pwrclk,
			pin_dm: pa11.into_alternate(),
			pin_dp: pa12.into_alternate(),
			hclk: clocks.hclk()
		};
		let usb_allocator = UsbBus::new(usb, unsafe { &mut USB_DRIVER_STATIC.allocator_memory });

		unsafe {
			USB_DRIVER_STATIC.allocator.write(usb_allocator);
		}

		Self::new()
	}

	fn new() -> Self {
		let allocator = unsafe { USB_DRIVER_STATIC.allocator.assume_init_ref() };

		let hid_class = HidUsbClass::new(allocator);

		let mut device = UsbDeviceBuilder::new(
			allocator,
			// pid.codes free VID and testing PID for open hardware
			UsbVidPid(0x1209, 0x000A)
		)
			.manufacturer("lavusedu")
			.product("Board McBoardyface")
			.serial_number("DEV")
			.self_powered(true)
			// HID device
			.device_class(0x03)
			// Boot Interface subclass
			// .device_sub_class(0x01)
			// Keyboard - used in conjunction with subclass 0x01
			// .device_protocol(0x01)
			.build();
		let _ = device.force_reset();

		UsbDriver { device, hid_class }
	}

	pub fn poll(&mut self) -> Result<Option<report::TouchKeyboardReport>, UsbError> {
		let changed = self.device.poll(&mut [&mut self.hid_class]);

		if changed {
			self.hid_class.pull_report().map(Some)
		} else {
			Ok(None)
		}
	}

	pub fn push_report(&mut self, report: report::TouchKeyboardReport) -> Result<(), UsbError> {
		self.hid_class.push_report(report)
	}
}
