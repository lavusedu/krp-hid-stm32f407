use bitregister::{bitpacked, bitvalue, prelude::BitPackedValue};

use usb_hid::{
	hid_descriptor,
	report::{
		items::{MainItemInputData, MainItemNull, MainItemVariable},
		known::UsagePageKeyboard
	},
	HidReport
};

bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) KeyboardModifierRightMeta {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) KeyboardModifierRightAlt {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) KeyboardModifierRightShift {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) KeyboardModifierRightControl {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) KeyboardModifierLeftMeta {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) KeyboardModifierLeftAlt {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) KeyboardModifierLeftShift {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) KeyboardModifierLeftControl {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) KeyboardModifiers {
		@[shl = 7]
		pub right_meta: KeyboardModifierRightMeta,
		@[shl = 6]
		pub right_alt: KeyboardModifierRightAlt,
		@[shl = 5]
		pub right_shift: KeyboardModifierRightShift,
		@[shl = 4]
		pub right_control: KeyboardModifierRightControl,
		@[shl = 3]
		pub left_meta: KeyboardModifierLeftMeta,
		@[shl = 2]
		pub left_alt: KeyboardModifierLeftAlt,
		@[shl = 1]
		pub left_shift: KeyboardModifierLeftShift,
		@[shl = 0]
		pub left_control: KeyboardModifierLeftControl
	}
}

bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(core::num::NonZeroU8) KeyboardKeycode {
		0x04 ..= 0xDD

		const hack {
			min = unsafe { core::num::NonZeroU8::new_unchecked(0x04) };
			max = unsafe { core::num::NonZeroU8::new_unchecked(0xDD) };
		}
	}
}

// Descriptor:
//
// Usage Page (Keyboard)
//
// Modifiers
// Local Minimum (0)
// Local Maximum (1)
// Report Size (1)
// Report Count (8)
//
// Usage Minimum (0xE0)
// Usage Maximum (0xE7)
//
// Input (Variable)
//
// keycodes
// Local Minimum (0x00)
// Local Maximum (0xDD)
// Report Size (8)
// Report Count (6)
//
// Usage Minimum (0x00)
// Usage Maximum (0xDD)
//
// Input (HasNull)
bitpacked! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub struct KeyboardInputReport {
		pub modifiers: KeyboardModifiers,
		pub keycodes: [Option<KeyboardKeycode>; 6]
	} -> KeyboardInputReportPacked
}
hid_descriptor! {
	pub descriptor KeyboardInputReportDescriptor {
		m1: Global UsagePage(UsagePageKeyboard::PAGE)[u16],

		m2: Global LogicalMinimum(0)[u8],
		m3: Global LogicalMaximum(1)[u8],
		m4: Global ReportSize(1)[u8],
		m5: Global ReportCount(8)[u8],

		m6: Local UsageMinimum(UsagePageKeyboard::LeftControl)[u16],
		m7: Local UsageMaximum(UsagePageKeyboard::RightGui)[u16],

		m8: Main Input(
			MainItemInputData {
				variable: MainItemVariable::Variable,
				..MainItemInputData::default()
			}
		)[MainItemInputData],

		k1: Global LogicalMinimum(0)[u16],
		k2: Global LogicalMaximum(UsagePageKeyboard::KeypadHexadecimal)[u16],
		k3: Global ReportSize(8)[u8],
		k4: Global ReportCount(6)[u8],

		k5: Local UsageMinimum(0)[u16],
		k6: Local UsageMaximum(UsagePageKeyboard::KeypadHexadecimal)[u16],

		k7: Main Input(
			MainItemInputData {
				null: MainItemNull::HasNull,
				..MainItemInputData::default()
			}
		)[MainItemInputData]
	}
}
impl HidReport for KeyboardInputReport {
	type DescriptorType = KeyboardInputReportDescriptor;

	fn build_descriptor() -> <Self::DescriptorType as BitPackedValue>::PackedRepr {
		KeyboardInputReportDescriptor::build_packed()
	}
}
impl KeyboardInputReport {
	pub const fn empty() -> Self {
		Self::empty_modifiers(KeyboardModifiers::DEFAULT)
	}

	pub const fn empty_modifiers(modifiers: KeyboardModifiers) -> Self {
		KeyboardInputReport {
			modifiers,
			keycodes: [None; 6]
		}
	}

	pub const fn one_key(key: KeyboardKeycode) -> Self {
		Self::one_key_modifiers(key, KeyboardModifiers::DEFAULT)
	}

	pub const fn one_key_modifiers(key: KeyboardKeycode, modifiers: KeyboardModifiers) -> Self {
		KeyboardInputReport {
			modifiers,
			keycodes: [Some(key), None, None, None, None, None]
		}
	}
}
