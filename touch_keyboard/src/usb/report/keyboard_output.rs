use bitregister::{bitpacked, bitvalue, prelude::BitPackedValue};

use usb_hid::{
	hid_descriptor,
	report::{
		items::{MainItemOutputData, MainItemVariable},
		known::*
	},
	HidReport
};

bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) LedIndicatorScrollLock {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) LedIndicatorCapsLock {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) LedIndicatorNumLock {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) LedIndicators {
		@[shl = 2]
		pub scroll_lock: LedIndicatorScrollLock,
		@[shl = 1]
		pub caps_lock: LedIndicatorCapsLock,
		@[shl = 0]
		pub num_lock: LedIndicatorNumLock
	}
}

// Descriptor:
//
// Usage Page (LED)
// Logical Minimum (0)
// Logical Maximum (1)
// Report Size (1)
// Report Count (3)
//
// Usage Minimum (0x01)
// Usage Maximum (0x03)
//
// Output (Variable)
bitpacked! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub struct KeyboardOutputReport {
		pub led_indicators: LedIndicators
	} -> KeyboardOutputReportPacked
}
hid_descriptor! {
	pub descriptor KeyboardOutputReportDescriptor {
		l1: Global UsagePage(UsagePageLed::PAGE)[u16],

		l2: Global LogicalMinimum(0)[u8],
		l3: Global LogicalMaximum(1)[u8],
		l4: Global ReportSize(1)[u8],
		l5: Global ReportCount(8)[u8],

		l6: Local UsageMinimum(UsagePageLed::NumLock)[u16],
		l7: Local UsageMaximum(UsagePageLed::ScrollLock)[u16],
		l8: Local Usage(UsagePageLed::Shift)[u16],
		l9: Local Usage(UsagePageLed::Power)[u16],
		l10: Local UsageMinimum(UsagePageLed::StandBy)[u16],
		l11: Local UsageMaximum(UsagePageLed::Ready)[u16],

		l12: Main Output(
			MainItemOutputData {
				variable: MainItemVariable::Variable,
				..MainItemOutputData::default()
			}
		)[MainItemOutputData]
	}
}
impl HidReport for KeyboardOutputReport {
	type DescriptorType = KeyboardOutputReportDescriptor;

	fn build_descriptor() -> <Self::DescriptorType as BitPackedValue>::PackedRepr {
		KeyboardOutputReportDescriptor::build_packed()
	}
}
