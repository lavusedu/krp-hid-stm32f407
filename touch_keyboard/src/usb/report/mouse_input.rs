use bitregister::{bitpacked, bitvalue, prelude::BitPackedValue};

use usb_hid::{
	hid_descriptor,
	report::{
		items::{MainItemInputData, MainItemRelative, MainItemVariable},
		known::{UsagePageButton, UsagePageGenericDesktop}
	},
	HidReport
};

bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) MouseButton1 {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) MouseButton2 {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) MouseButton3 {
		Disabled = 0b0,
		Enabled
	}
}
bitvalue! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub bitvalue(u8) MouseButtons {
		@[shl = 2]
		pub b3: MouseButton3,
		@[shl = 1]
		pub b2: MouseButton2,
		@[shl = 0]
		pub b1: MouseButton1
	}
}

// Descriptor:
//
// // buttons
// Usage Page (Buttons)
//
// Logical Minimum (0)
// Logical Maximum (1)
// Report Size (1)
// Report Count (8)
//
// Usage Minimum (1)
// Usage Maximum (8)
//
// Input (Data, Variable, Absolute)
//
// // x and y axes
// Usage Page (Desktop)
//
// Logical Minimum (-127)
// Logical Maximum (127)
// Report Size (8)
// Report Count (2)
//
// Usage (X)
// Usage (Y)
//
// Input (Data, Variable, Relative)
bitpacked! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub struct MouseInputReport {
		pub buttons: MouseButtons,
		pub x: i8,
		pub y: i8
	} -> MouseInputReportPacked
}
hid_descriptor! {
	pub descriptor MouseInputReportDescriptor {
		b1: Global UsagePage(UsagePageButton::PAGE)[u16],

		b2: Global LogicalMinimum(0)[u8],
		b3: Global LogicalMaximum(1)[u8],
		b4: Global ReportSize(1)[u8],
		b5: Global ReportCount(8)[u8],

		b6: Local UsageMinimum(1)[u8],
		b7: Local UsageMaximum(8)[u8],

		b8: Main Input(
			MainItemInputData {
				variable: MainItemVariable::Variable,
				..MainItemInputData::default()
			}
		)[MainItemInputData],

		a1: Global UsagePage(UsagePageGenericDesktop::PAGE)[u16],

		a2: Global LogicalMinimum(-i8::MAX)[u8],
		a3: Global LogicalMaximum(i8::MAX)[u8],
		a4: Global ReportSize(8)[u8],
		a5: Global ReportCount(2)[u8],

		a6: Local Usage(UsagePageGenericDesktop::X)[u16],
		a7: Local Usage(UsagePageGenericDesktop::Y)[u16],

		a8: Main Input(
			MainItemInputData {
				variable: MainItemVariable::Variable,
				relative: MainItemRelative::Relative,
				..MainItemInputData::default()
			}
		)[MainItemInputData]
	}
}
impl HidReport for MouseInputReport {
	type DescriptorType = MouseInputReportDescriptor;

	fn build_descriptor() -> <Self::DescriptorType as BitPackedValue>::PackedRepr {
		MouseInputReportDescriptor::build_packed()
	}
}
