use bitregister::{bitpacked, prelude::BitPackedValue};

use usb_hid::{
	hid_descriptor,
	report::{items::MainItemCollectionData, known::UsagePageGenericDesktop},
	HidReport
};

pub mod keyboard_input;
pub mod keyboard_output;
pub mod mouse_input;

pub use keyboard_input::KeyboardInputReport;
pub use keyboard_output::KeyboardOutputReport;
pub use mouse_input::MouseInputReport;

use keyboard_input::KeyboardInputReportDescriptor;
use keyboard_output::KeyboardOutputReportDescriptor;
use mouse_input::MouseInputReportDescriptor;

//  Full descriptor:
//
//  Usage Page (Desktop)
//  Usage (Keyboard)
//  Collection (Application)
//  	Report ID (1)
//  	<Keyboard Input>
//  End Collection
//
//  Usage Page (Desktop)
//  Usage (Keyboard)
//  Collection (Application)
//  	Report ID (2)
//  	<Keyboard Output>
//  End Collection
//
//  Usage Page (Desktop)
//  Usage (Mouse)
//  Collection (Application)
//  	Usage (Pointer)
//  	Collection (Physical)
//  		Report ID (3)
//  		<Mouse Input>
//  	End Collection
//  End Collection
bitpacked! {
	#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
	pub enum(TouchKeyboardReportTag: u8) TouchKeyboardReport {
		KeyboardInput(KeyboardInputReport) = 1,
		KeyboardOutput(KeyboardOutputReport),
		MouseInput(MouseInputReport)
	} -> TouchKeyboardReportPacked
}
hid_descriptor! {
	pub descriptor TouchKeyboardReportDescriptor {
		f1: Global UsagePage(UsagePageGenericDesktop::PAGE)[u16],
		f2: Local Usage(UsagePageGenericDesktop::Keyboard)[u16],
		f3: Main Collection(MainItemCollectionData::Application)[MainItemCollectionData],
		f4: Global ReportId(TouchKeyboardReportTag::KeyboardInput)[u8],
		f5: Inline KeyboardInputReportDescriptor,
		f6: Main EndCollection,

		f7: Global UsagePage(UsagePageGenericDesktop::PAGE)[u16],
		f8: Local Usage(UsagePageGenericDesktop::Keyboard)[u16],
		f9: Main Collection(MainItemCollectionData::Application)[MainItemCollectionData],
		f10: Global ReportId(TouchKeyboardReportTag::KeyboardOutput)[u8],
		f11: Inline KeyboardOutputReportDescriptor,
		f12: Main EndCollection,

		f13: Global UsagePage(UsagePageGenericDesktop::PAGE)[u16],
		f14: Local Usage(UsagePageGenericDesktop::Mouse)[u16],
		f15: Main Collection(MainItemCollectionData::Application)[MainItemCollectionData],
		f16: Local Usage(UsagePageGenericDesktop::Pointer)[u16],
		f17: Main Collection(MainItemCollectionData::Physical)[MainItemCollectionData],
		f18: Global ReportId(TouchKeyboardReportTag::MouseInput)[u8],
		f19: Inline MouseInputReportDescriptor,
		f20: Main EndCollection,
		f21: Main EndCollection
	}
}
impl HidReport for TouchKeyboardReport {
	type DescriptorType = TouchKeyboardReportDescriptor;

	fn build_descriptor() -> <Self::DescriptorType as BitPackedValue>::PackedRepr {
		TouchKeyboardReportDescriptor::build_packed()
	}
}
