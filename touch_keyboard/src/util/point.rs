use core::ops::{Add, Div, Mul, Neg, Sub};

// In case we want f64 or f16
type Real = f32;

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
#[derive(Copy, Clone)]
pub struct Point2 {
	pub x: Real,
	pub y: Real
}
impl Point2 {
	pub const fn zero() -> Self {
		Self { x: 0.0, y: 0.0 }
	}

	pub fn length(&self) -> Real {
		libm::hypotf(self.x, self.y)
	}

	pub fn distance(&self, rhs: &Self) -> Real {
		(*self - *rhs).length()
	}
}
impl Add for Point2 {
	type Output = Self;

	fn add(self, rhs: Self) -> Self::Output {
		Point2 {
			x: self.x + rhs.x,
			y: self.y + rhs.y
		}
	}
}
impl Sub for Point2 {
	type Output = Self;

	fn sub(self, rhs: Self) -> Self::Output {
		Point2 {
			x: self.x - rhs.x,
			y: self.y - rhs.y
		}
	}
}
impl Neg for Point2 {
	type Output = Self;

	fn neg(self) -> Self::Output {
		Point2 {
			x: -self.x,
			y: -self.y
		}
	}
}
impl Mul<Real> for Point2 {
	type Output = Self;

	fn mul(self, rhs: Real) -> Self::Output {
		Point2 {
			x: self.x * rhs,
			y: self.y * rhs
		}
	}
}
impl Div<Real> for Point2 {
	type Output = Self;

	fn div(self, rhs: Real) -> Self::Output {
		Point2 {
			x: self.x / rhs,
			y: self.y / rhs
		}
	}
}
