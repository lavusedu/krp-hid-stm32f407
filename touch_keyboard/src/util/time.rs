use rtic::Monotonic;
use stm32f4xx_hal::{pac::TIM2, timer::monotonic::MonoTimer};

pub type MicrosecMonotonicTimer = MonoTimer<TIM2, 1_000_000>;
pub type Instant = <MicrosecMonotonicTimer as Monotonic>::Instant;
pub type Duration = <MicrosecMonotonicTimer as Monotonic>::Duration;

macro_rules! task_timeout {
	(
		struct $name: ident {
			task = $module: ident;
			timeout = $timeout: expr;
			$(
				args = (
					$( $arg_name: ident: $arg_type: ty ),+
				);
			)?
		}
	) => {
		pub struct $name {
			handle: Option<$module::SpawnHandle>
		}
		impl $name {
			const TIMEOUT: crate::util::time::Duration = $timeout;

			pub const fn new() -> Self {
				Self {
					handle: None
				}
			}

			pub fn cancel(&mut self) {
				if let Some(handle) = self.handle.take() {
					let _ = handle.cancel();
				}
			}

			pub fn schedule(
				&mut self,
				$(
					$( $arg_name: $arg_type ),+
				)?
			) -> Result<(), ()> {
				match $module::spawn_after(
					Self::TIMEOUT,
					$(
						$( $arg_name ),+
					)?
				) {
					Err(_) => Err(()),
					Ok(handle) => {
						self.handle = Some(handle);
						Ok(())
					}
				}
			}
		}
	};
}

macro_rules! task_cooldown {
	(
		struct $name: ident {
			cooldown = $cooldown: expr;
		}
	) => {
		pub struct $name {
			last: Option<crate::util::time::Instant>
		}
		impl $name {
			const COOLDOWN: crate::util::time::Duration = $cooldown;

			pub const fn new() -> Self {
				$name { last: None }
			}

			pub fn cancel(&mut self) {
				let _ = self.last.take();
			}

			pub fn check(&mut self, instant: crate::util::time::Instant) -> bool {
				match self
					.last
					.as_ref()
					.and_then(|last| instant.checked_duration_since(*last))
				{
					Some(duration) if duration < Self::COOLDOWN => return false,
					_ => ()
				}

				self.last = Some(instant);
				return true
			}
		}
	};
}
