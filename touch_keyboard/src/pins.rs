use stm32f4xx_hal::{
	gpio::{
		self,
		gpioc::PC7,
		gpiog::{PG15, PG6, PG8},
		gpioi::PI9,
		ExtiPin,
		Floating,
		Input,
		Output,
		PullUp,
		PushPull
	},
	pac::EXTI,
	syscfg::SysCfg
};

use crate::presenter::LedUpdate;

pub struct OutputLeds {
	led1: PG6<Output<PushPull>>,
	led2: PG8<Output<PushPull>>,
	led3: PI9<Output<PushPull>>,
	led4: PC7<Output<PushPull>>
}
impl OutputLeds {
	pub fn initialize(
		pg6: PG6<Input<Floating>>,
		pg8: PG8<Input<Floating>>,
		pi9: PI9<Input<Floating>>,
		pc7: PC7<Input<Floating>>
	) -> Self {
		Self {
			led1: pg6.into_push_pull_output(),
			led2: pg8.into_push_pull_output(),
			led3: pi9.into_push_pull_output(),
			led4: pc7.into_push_pull_output()
		}
	}

	pub fn all_high(&mut self) {
		self.led1.set_high();
		self.led2.set_high();
		self.led3.set_high();
		self.led4.set_high();
	}

	pub fn all_low(&mut self) {
		self.led1.set_low();
		self.led2.set_low();
		self.led3.set_low();
		self.led4.set_low();
	}

	pub fn from_led_update(&mut self, update: LedUpdate) {
		if update.led1 {
			self.led1.set_high()
		} else {
			self.led1.set_low()
		}

		if update.led2 {
			self.led2.set_high()
		} else {
			self.led2.set_low()
		}

		if update.led3 {
			self.led3.set_high()
		} else {
			self.led3.set_low()
		}

		if update.led4 {
			self.led4.set_high()
		} else {
			self.led4.set_low()
		}
	}
}

pub struct InputButtons {
	button: PG15<Input<PullUp>>
}
impl InputButtons {
	pub fn initialize(
		syscfg: &mut SysCfg,
		interrupt_controller: &mut EXTI,
		pg15: PG15<Input<Floating>>
	) -> Self {
		let mut button = pg15.into_pull_up_input();
		button.make_interrupt_source(syscfg);
		button.enable_interrupt(interrupt_controller);
		button.trigger_on_edge(interrupt_controller, gpio::Edge::Falling);

		Self { button }
	}

	pub fn clear_interrupt(&mut self) {
		self.button.clear_interrupt_pending_bit();
	}

	pub fn is_pressed(&self) -> bool {
		self.button.is_low()
	}
}
