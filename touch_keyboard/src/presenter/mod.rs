use bitregister::prelude::BitValue;

use crate::{
	touch::TouchEvent,
	usb::report::{
		keyboard_input::{KeyboardKeycode, KeyboardModifiers},
		KeyboardInputReport,
		KeyboardOutputReport,
		MouseInputReport,
		TouchKeyboardReport
	},
	util::{point::Point2, time::Instant}
};

use self::layout::*;

pub mod layout;

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
pub enum DrawUpdateTouchpadScreen {
	ModeIdle,
	ModeTouch,
	ModeHold,
	ModeDrag
}

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
pub enum PressedArrowKey {
	Up,
	Down,
	Left,
	Right
}

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
#[derive(Clone, Copy)]
#[repr(u8)]
pub enum PressedKeyboardKey {
	A = 0x04,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
	Kb1 = 0x1E,
	Kb2,
	Kb3,
	Kb4,
	Kb5,
	Kb6,
	Kb7,
	Kb8,
	Kb9,
	Kb0,
	Enter = 0x28,
	Backspace = 0x2A,
	Space = 0x2C,
	Comma = 0x36,
	Period = 0x37,
	LCtrl = 0xE0,
	LShift,
	LAlt,
	LMeta
}
impl From<PressedKeyboardKey> for KeyboardKeycode {
	fn from(key: PressedKeyboardKey) -> Self {
		KeyboardKeycode::from_bits(key as _).unwrap()
	}
}

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
#[derive(Clone, Copy, Default)]
pub struct HeldKeyboardModifiers {
	pub ctrl: bool,
	pub shift: bool,
	pub alt: bool,
	pub meta: bool
}
impl From<HeldKeyboardModifiers> for KeyboardModifiers {
	fn from(modifiers: HeldKeyboardModifiers) -> Self {
		use crate::usb::report::keyboard_input::*;

		KeyboardModifiers::new(
			KeyboardModifierRightMeta::Disabled,
			KeyboardModifierRightAlt::Disabled,
			KeyboardModifierRightShift::Disabled,
			KeyboardModifierRightControl::Disabled,
			if modifiers.meta {
				KeyboardModifierLeftMeta::Enabled
			} else {
				KeyboardModifierLeftMeta::Disabled
			},
			if modifiers.alt {
				KeyboardModifierLeftAlt::Enabled
			} else {
				KeyboardModifierLeftAlt::Disabled
			},
			if modifiers.shift {
				KeyboardModifierLeftShift::Enabled
			} else {
				KeyboardModifierLeftShift::Disabled
			},
			if modifiers.ctrl {
				KeyboardModifierLeftControl::Enabled
			} else {
				KeyboardModifierLeftControl::Disabled
			}
		)
	}
}

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
pub enum DrawUpdate {
	TouchpadScreen(DrawUpdateTouchpadScreen),
	ArrowKeysScreen(Option<PressedArrowKey>),
	KeyboardScreen(Option<PressedKeyboardKey>, HeldKeyboardModifiers)
}

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
pub struct LedUpdate {
	pub led1: bool,
	pub led2: bool,
	pub led3: bool,
	pub led4: bool
}

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
pub struct PresenterResponse {
	pub draw_update: Option<DrawUpdate>,
	pub led_update: Option<LedUpdate>,
	pub usb_update: Option<TouchKeyboardReport>
}

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
pub enum PresenterEvent {
	Timeout,
	ModeSwitch,
	UsbUpdate(KeyboardOutputReport),
	TouchEvent(TouchEvent)
}

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
enum State {
	ModePointer,
	ModeArrowKeys,
	ModeKeyboard(HeldKeyboardModifiers)
}

pub struct PresenterStateMachine {
	state: State
}
impl PresenterStateMachine {
	pub fn new() -> Self {
		Self {
			state: State::ModePointer
		}
	}

	pub fn on_event(&mut self, _instant: Instant, event: PresenterEvent) -> PresenterResponse {
		let mut response = PresenterResponse {
			draw_update: None,
			led_update: None,
			usb_update: None
		};

		match event {
			PresenterEvent::ModeSwitch => {
				response.usb_update = Some(TouchKeyboardReport::KeyboardInput(
					KeyboardInputReport::empty()
				));

				let (new_state, draw_update) = match self.state {
					State::ModePointer => (State::ModeArrowKeys, DrawUpdate::ArrowKeysScreen(None)),
					State::ModeArrowKeys => (
						State::ModeKeyboard(HeldKeyboardModifiers::default()),
						DrawUpdate::KeyboardScreen(None, HeldKeyboardModifiers::default())
					),
					State::ModeKeyboard(_) => (
						State::ModePointer,
						DrawUpdate::TouchpadScreen(DrawUpdateTouchpadScreen::ModeIdle)
					)
				};

				self.state = new_state;
				response.draw_update = Some(draw_update);
			}
			PresenterEvent::UsbUpdate(report) => {
				response.led_update = Some(Self::handle_usb_update(report));
			}
			PresenterEvent::Timeout => match self.state {
				State::ModePointer => {}
				State::ModeArrowKeys => {}
				State::ModeKeyboard(_) => {}
			},
			PresenterEvent::TouchEvent(event) => match self.state {
				State::ModePointer => Self::handle_pointer_touch_event(&mut response, event),
				State::ModeArrowKeys => Self::handle_arrow_touch_event(&mut response, event),
				State::ModeKeyboard(ref mut helf_modifiers) => {
					Self::handle_keyboard_touch_event(&mut response, helf_modifiers, event)
				}
			}
		}

		response
	}

	fn handle_usb_update(event: KeyboardOutputReport) -> LedUpdate {
		LedUpdate {
			led1: event.led_indicators.num_lock.to_bits() != 0,
			led2: event.led_indicators.caps_lock.to_bits() != 0,
			led3: event.led_indicators.scroll_lock.to_bits() != 0,
			led4: false
		}
	}

	fn handle_pointer_touch_event(response: &mut PresenterResponse, event: TouchEvent) {
		fn handle_delta(delta: Point2) -> TouchKeyboardReport {
			TouchKeyboardReport::MouseInput(MouseInputReport {
				buttons: Default::default(),
				x: (delta.x * 255.0 * 6.0) as i8,
				y: (delta.y * 255.0 * 6.0) as i8
			})
		}

		match event {
			TouchEvent::TouchBegin { .. } => {
				response.draw_update = Some(DrawUpdate::TouchpadScreen(
					DrawUpdateTouchpadScreen::ModeTouch
				));
			}
			TouchEvent::TouchEnd { .. } => {
				response.draw_update = Some(DrawUpdate::TouchpadScreen(
					DrawUpdateTouchpadScreen::ModeIdle
				));
			}
			TouchEvent::DragBegin { .. } => {
				// ignore this delta because it causes big jumps when
				// paired with mouse acceleration
				response.usb_update = Some(handle_delta(Point2::zero()));
				response.draw_update = Some(DrawUpdate::TouchpadScreen(
					DrawUpdateTouchpadScreen::ModeDrag
				));
			}
			TouchEvent::DragContinue { delta, .. } => {
				response.usb_update = Some(handle_delta(delta));
			}
			TouchEvent::DragEnd { delta, .. } => {
				response.usb_update = Some(handle_delta(delta));
				response.draw_update = Some(DrawUpdate::TouchpadScreen(
					DrawUpdateTouchpadScreen::ModeIdle
				));
			}
		}
	}

	fn handle_arrow_touch_event(response: &mut PresenterResponse, event: TouchEvent) {
		match event {
			TouchEvent::TouchBegin { position } => match position {
				_ if ARROW_UP.contains_real(position) => {
					response.usb_update = Some(TouchKeyboardReport::KeyboardInput(
						KeyboardInputReport::one_key(KeyboardKeycode::from_bits(0x52).unwrap())
					));
					response.draw_update =
						Some(DrawUpdate::ArrowKeysScreen(Some(PressedArrowKey::Up)));
				}
				_ if ARROW_DOWN.contains_real(position) => {
					response.usb_update = Some(TouchKeyboardReport::KeyboardInput(
						KeyboardInputReport::one_key(KeyboardKeycode::from_bits(0x51).unwrap())
					));
					response.draw_update =
						Some(DrawUpdate::ArrowKeysScreen(Some(PressedArrowKey::Down)));
				}
				_ if ARROW_LEFT.contains_real(position) => {
					response.usb_update = Some(TouchKeyboardReport::KeyboardInput(
						KeyboardInputReport::one_key(KeyboardKeycode::from_bits(0x50).unwrap())
					));
					response.draw_update =
						Some(DrawUpdate::ArrowKeysScreen(Some(PressedArrowKey::Left)));
				}
				_ if ARROW_RIGHT.contains_real(position) => {
					response.usb_update = Some(TouchKeyboardReport::KeyboardInput(
						KeyboardInputReport::one_key(KeyboardKeycode::from_bits(0x4F).unwrap())
					));
					response.draw_update =
						Some(DrawUpdate::ArrowKeysScreen(Some(PressedArrowKey::Right)));
				}
				_ => ()
			},
			TouchEvent::TouchEnd { .. } | TouchEvent::DragBegin { .. } => {
				response.usb_update = Some(TouchKeyboardReport::KeyboardInput(
					KeyboardInputReport::empty()
				));
				response.draw_update = Some(DrawUpdate::ArrowKeysScreen(None));
			}
			_ => ()
		}
	}

	fn handle_keyboard_touch_event(
		response: &mut PresenterResponse,
		held_modifiers: &mut HeldKeyboardModifiers,
		event: TouchEvent
	) {
		macro_rules! match_position_key {
			(
				$position: expr;
				$(
					$layout: expr => $key: ident
				),+ $(,)?
			) => {
				match $position {
					$(
						p if $layout.contains_real(p) => {
							Some(PressedKeyboardKey::$key)
						},
					)+
					_ => None
				}
			}
		}

		match event {
			TouchEvent::TouchBegin { position } => {
				let key = match_position_key!(
					position;

					KEY_SHIFT => LShift,
					KEY_ALT => LAlt,
					KEY_CTRL => LCtrl,
					KEY_META => LMeta,

					KEY_BACKSPACE => Backspace,
					KEY_SPACE => Space,
					KEY_ENTER => Enter,

					KEY_Q => Q,
					KEY_W => W,
					KEY_E => E,
					KEY_R => R,
					KEY_T => T,
					KEY_Y => Y,
					KEY_U => U,
					KEY_I => I,
					KEY_O => O,
					KEY_P => P,
					KEY_A => A,
					KEY_S => S,
					KEY_D => D,
					KEY_F => F,
					KEY_G => G,
					KEY_H => H,
					KEY_J => J,
					KEY_K => K,
					KEY_L => L,
					KEY_Z => Z,
					KEY_X => X,
					KEY_C => C,
					KEY_V => V,
					KEY_B => B,
					KEY_N => N,
					KEY_M => M,

					KEY_COMMA => Comma,
					KEY_PERIOD => Period,
					KEY_KB1 => Kb1,
					KEY_KB2 => Kb2,
					KEY_KB3 => Kb3,
					KEY_KB4 => Kb4,
					KEY_KB5 => Kb5,
					KEY_KB6 => Kb6,
					KEY_KB7 => Kb7,
					KEY_KB8 => Kb8,
					KEY_KB9 => Kb9,
					KEY_KB0 => Kb0
				);

				match key {
					Some(PressedKeyboardKey::LShift) => {
						held_modifiers.shift = !held_modifiers.shift;
					}
					Some(PressedKeyboardKey::LAlt) => {
						held_modifiers.alt = !held_modifiers.alt;
					}
					Some(PressedKeyboardKey::LCtrl) => {
						held_modifiers.ctrl = !held_modifiers.ctrl;
					}
					Some(PressedKeyboardKey::LMeta) => {
						held_modifiers.meta = !held_modifiers.meta;
					}
					Some(key) => {
						response.usb_update = Some(TouchKeyboardReport::KeyboardInput(
							KeyboardInputReport::one_key_modifiers(
								key.into(),
								(*held_modifiers).into()
							)
						));
	
						response.draw_update =
							Some(DrawUpdate::KeyboardScreen(Some(key), *held_modifiers));
					}
					_ => ()
				}
			}
			TouchEvent::TouchEnd { .. } | TouchEvent::DragBegin { .. } => {
				response.usb_update = Some(TouchKeyboardReport::KeyboardInput(
					KeyboardInputReport::empty_modifiers((*held_modifiers).into())
				));
				response.draw_update = Some(DrawUpdate::KeyboardScreen(None, *held_modifiers));
			}
			_ => ()
		}
	}
}
