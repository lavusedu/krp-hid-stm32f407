use embedded_graphics::{
	prelude::{Point, Size},
	primitives::Rectangle
};

use ili9325::pixel_map::PixelMapping;

use crate::util::point::Point2;

pub const SCREEN_SIZE: Size = ili9325::pixel_map::PixelMappingRotate90::DISPLAY_SIZE;

pub const TOUCHPAD_BORDER: u32 = 15;

#[derive(Clone, Copy)]
pub struct RectangleLayout {
	pub rectangle: Rectangle
}
impl RectangleLayout {
	const fn center_offset(size: Size) -> Point {
		Point {
			x: (size.width.saturating_sub(1) / 2) as i32,
			y: (size.height.saturating_sub(1) / 2) as i32
		}
	}

	pub const fn new_center(center: Point, size: Size) -> Self {
		let center_offset = Self::center_offset(size);
		RectangleLayout {
			rectangle: Rectangle::new(
				Point {
					x: center.x - center_offset.x,
					y: center.y - center_offset.y
				},
				size
			)
		}
	}

	pub const fn shift(self, shift: Point) -> Self {
		RectangleLayout {
			rectangle: Rectangle::new(
				Point {
					x: self.rectangle.top_left.x + shift.x,
					y: self.rectangle.top_left.y + shift.y
				},
				self.rectangle.size
			)
		}
	}

	pub const fn center(self) -> Point {
		let center_offset = Self::center_offset(self.rectangle.size);
		Point {
			x: self.rectangle.top_left.x + center_offset.x,
			y: self.rectangle.top_left.y + center_offset.y
		}
	}

	pub const fn top_left(self) -> Point {
		self.rectangle.top_left
	}

	pub const fn bottom_right(self) -> Point {
		Point {
			x: self.rectangle.top_left.x + self.rectangle.size.width.saturating_sub(1) as i32,
			y: self.rectangle.top_left.y + self.rectangle.size.height.saturating_sub(1) as i32
		}
	}

	// pub fn center_real(self) -> Point2 {
	// 	let center = self.center();
	// 	Point2 {
	// 		x: center.x as f32 / SCREEN_SIZE.width as f32,
	// 		y: center.y as f32 / SCREEN_SIZE.height as f32
	// 	}
	// }

	pub fn top_left_real(self) -> Point2 {
		let top_left = self.top_left();
		Point2 {
			x: top_left.x as f32 / SCREEN_SIZE.width as f32,
			y: top_left.y as f32 / SCREEN_SIZE.height as f32
		}
	}

	pub fn bottom_right_real(self) -> Point2 {
		let bottom_right = self.bottom_right();
		Point2 {
			x: bottom_right.x as f32 / SCREEN_SIZE.width as f32,
			y: bottom_right.y as f32 / SCREEN_SIZE.height as f32
		}
	}

	pub fn contains_real(self, point: Point2) -> bool {
		let top_left = self.top_left_real();
		let bottom_right = self.bottom_right_real();

		point.x >= top_left.x
			&& point.x <= bottom_right.x
			&& point.y >= top_left.y
			&& point.y <= bottom_right.y
	}
}
#[cfg(feature = "semihosting")]
impl defmt::Format for RectangleLayout {
	fn format(&self, f: defmt::Formatter) {
		defmt::write!(
			f,
			"RectangleLayout {{ top_left = ({}, {}), bottom_right = ({}, {}), size = ({}, {}), center = ({}, {}), top_left_real = ({}, {}), bottom_right_real = ({}, {}) }}",
			self.top_left().x, self.top_left().y,
			self.bottom_right().x, self.bottom_right().y,
			self.rectangle.size.width, self.rectangle.size.height,
			self.center().x, self.center().y,
			self.top_left_real().x, self.top_left_real().y,
			self.bottom_right_real().x, self.bottom_right_real().y
		)
	}
}

const fn make_grid<const W: usize, const H: usize>(
	offset: [i32; 2],
	step: [i32; 2]
) -> [[Point; H]; W] {
	let mut grid = [[Point::zero(); H]; W];

	if W == 0 || H == 0 {
		return grid
	}

	let start = [(W - 1) as i32 * -step[0] / 2, (H - 1) as i32 * -step[1] / 2];

	let mut x = 0;
	while x < W {
		let mut column = [Point::zero(); H];

		let mut y = 0;
		while y < H {
			column[y] = Point {
				x: offset[0] + start[0] + x as i32 * step[0],
				y: offset[1] + start[1] + y as i32 * step[1]
			};

			y += 1;
		}
		grid[x] = column;

		x += 1;
	}

	grid
}

const ARROW_MARGIN: i32 = 5;
const ARROW_SIZE: Size = Size {
	width: 90,
	height: 90
};
const ARROW_GRID: [[Point; 2]; 3] = make_grid::<3, 2>(
	[SCREEN_SIZE.width as i32 / 2, SCREEN_SIZE.height as i32 / 2],
	[
		ARROW_SIZE.width as i32 + ARROW_MARGIN,
		ARROW_SIZE.height as i32 + ARROW_MARGIN
	]
);

pub const ARROW_UP: RectangleLayout = RectangleLayout::new_center(ARROW_GRID[1][0], ARROW_SIZE);
pub const ARROW_DOWN: RectangleLayout = RectangleLayout::new_center(ARROW_GRID[1][1], ARROW_SIZE);
pub const ARROW_LEFT: RectangleLayout = RectangleLayout::new_center(ARROW_GRID[0][1], ARROW_SIZE);
pub const ARROW_RIGHT: RectangleLayout = RectangleLayout::new_center(ARROW_GRID[2][1], ARROW_SIZE);

const KEY_MARGIN: i32 = 2;
const KEY_SIZE: Size = Size {
	width: 28,
	height: 42
};

const KEY_GRID: [[Point; 5]; 10] = make_grid::<10, 5>(
	[SCREEN_SIZE.width as i32 / 2, SCREEN_SIZE.height as i32 / 2],
	[
		KEY_SIZE.width as i32 + KEY_MARGIN,
		KEY_SIZE.height as i32 + KEY_MARGIN
	]
);

const KEY_SHIFT_HALF: Point = Point {
	x: (KEY_SIZE.width as i32 + KEY_MARGIN) / 2,
	y: 0
};
const KEY_SHIFT_QUARTER: Point = Point {
	x: (KEY_SIZE.width as i32 + KEY_MARGIN) / 4,
	y: 0
};
const KEY_SHIFT_QUARTER_BACK: Point = Point {
	x: -KEY_SHIFT_QUARTER.x,
	y: KEY_SHIFT_QUARTER.y
};
const KEY_SIZE_150: Size = Size {
	width: KEY_SIZE.width + KEY_SIZE.width / 2 + KEY_MARGIN as u32,
	height: KEY_SIZE.height
};

pub const KEY_KB1: RectangleLayout = RectangleLayout::new_center(KEY_GRID[0][0], KEY_SIZE);
pub const KEY_KB2: RectangleLayout = RectangleLayout::new_center(KEY_GRID[1][0], KEY_SIZE);
pub const KEY_KB3: RectangleLayout = RectangleLayout::new_center(KEY_GRID[2][0], KEY_SIZE);
pub const KEY_KB4: RectangleLayout = RectangleLayout::new_center(KEY_GRID[3][0], KEY_SIZE);
pub const KEY_KB5: RectangleLayout = RectangleLayout::new_center(KEY_GRID[4][0], KEY_SIZE);
pub const KEY_KB6: RectangleLayout = RectangleLayout::new_center(KEY_GRID[5][0], KEY_SIZE);
pub const KEY_KB7: RectangleLayout = RectangleLayout::new_center(KEY_GRID[6][0], KEY_SIZE);
pub const KEY_KB8: RectangleLayout = RectangleLayout::new_center(KEY_GRID[7][0], KEY_SIZE);
pub const KEY_KB9: RectangleLayout = RectangleLayout::new_center(KEY_GRID[8][0], KEY_SIZE);
pub const KEY_KB0: RectangleLayout = RectangleLayout::new_center(KEY_GRID[9][0], KEY_SIZE);

pub const KEY_Q: RectangleLayout = RectangleLayout::new_center(KEY_GRID[0][1], KEY_SIZE);
pub const KEY_W: RectangleLayout = RectangleLayout::new_center(KEY_GRID[1][1], KEY_SIZE);
pub const KEY_E: RectangleLayout = RectangleLayout::new_center(KEY_GRID[2][1], KEY_SIZE);
pub const KEY_R: RectangleLayout = RectangleLayout::new_center(KEY_GRID[3][1], KEY_SIZE);
pub const KEY_T: RectangleLayout = RectangleLayout::new_center(KEY_GRID[4][1], KEY_SIZE);
pub const KEY_Y: RectangleLayout = RectangleLayout::new_center(KEY_GRID[5][1], KEY_SIZE);
pub const KEY_U: RectangleLayout = RectangleLayout::new_center(KEY_GRID[6][1], KEY_SIZE);
pub const KEY_I: RectangleLayout = RectangleLayout::new_center(KEY_GRID[7][1], KEY_SIZE);
pub const KEY_O: RectangleLayout = RectangleLayout::new_center(KEY_GRID[8][1], KEY_SIZE);
pub const KEY_P: RectangleLayout = RectangleLayout::new_center(KEY_GRID[9][1], KEY_SIZE);

pub const KEY_A: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[0][2], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_S: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[1][2], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_D: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[2][2], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_F: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[3][2], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_G: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[4][2], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_H: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[5][2], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_J: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[6][2], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_K: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[7][2], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_L: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[8][2], KEY_SIZE).shift(KEY_SHIFT_HALF);

pub const KEY_SHIFT: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[0][3], KEY_SIZE_150).shift(KEY_SHIFT_QUARTER);
pub const KEY_Z: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[1][3], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_X: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[2][3], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_C: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[3][3], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_V: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[4][3], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_B: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[5][3], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_N: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[6][3], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_M: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[7][3], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_BACKSPACE: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[9][3], KEY_SIZE_150).shift(KEY_SHIFT_QUARTER_BACK);

pub const KEY_CTRL: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[0][4], KEY_SIZE_150).shift(KEY_SHIFT_QUARTER);
pub const KEY_ALT: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[1][4], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_COMMA: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[2][4], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_SPACE: RectangleLayout = RectangleLayout::new_center(
	KEY_GRID[4][4],
	Size {
		width: KEY_SIZE.width * 3 + KEY_MARGIN as u32 * 2,
		height: KEY_SIZE.height
	}
)
.shift(KEY_SHIFT_HALF);
pub const KEY_PERIOD: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[6][4], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_META: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[7][4], KEY_SIZE).shift(KEY_SHIFT_HALF);
pub const KEY_ENTER: RectangleLayout =
	RectangleLayout::new_center(KEY_GRID[9][4], KEY_SIZE_150).shift(KEY_SHIFT_QUARTER_BACK);
