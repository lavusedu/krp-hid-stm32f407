use heapless::Vec;

use crate::util::{point::Point2, time::Instant};

use super::TouchEvent;

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
pub struct TouchConfig {
	/// Touch begin is fired when `z <= touch_begin_z_threshold`.
	pub touch_begin_z_threshold: f32,
	/// Touch end is fired when `z >= touch_end_z_threshold`.
	pub touch_end_z_threshold: f32,

	/// Drag begin is fired when `delta.length() >= drag_begin_distance_threshold`.
	pub drag_begin_distance_threshold: f32,
	/// Drag end is fired when `z >= drag_end_z_threshold`.
	pub drag_end_z_threshold: f32
}

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
#[derive(Clone, Copy)]
pub struct FifoEvent {
	/// Point on the screen where the touch happened.
	pub point: Point2,
	/// Touch resistance.
	///
	/// The meaning of the units is assigned through `TouchConfig` z thresholds.
	pub z: f32
}

pub type TouchEventResultBuffer = Vec<TouchEvent, 3>;

enum State {
	Idle,
	TouchStarted { start: Point2 },
	DragStarted { start: Point2, current: Point2 }
}

pub struct EventStateMachine {
	config: TouchConfig,
	state: State
}
impl EventStateMachine {
	pub fn new(config: TouchConfig) -> Self {
		Self {
			config,
			state: State::Idle
		}
	}

	pub fn on_event(&mut self, _instant: Instant, event: FifoEvent) -> TouchEventResultBuffer {
		let mut buffer = TouchEventResultBuffer::new();

		match self.state {
			State::Idle => {
				if event.z <= self.config.touch_begin_z_threshold {
					let _ = buffer.push(TouchEvent::TouchBegin {
						position: event.point
					});

					self.state = State::TouchStarted { start: event.point };
				}
			}
			State::TouchStarted { start } => {
				let delta = event.point - start;
				if delta.length() >= self.config.drag_begin_distance_threshold {
					let current = start + delta;
					let _ = buffer.push(TouchEvent::DragBegin { start, delta });
					self.state = State::DragStarted { start, current };

					// handle immediate drag end
					if event.z >= self.config.drag_end_z_threshold {
						// zero delta - it was reported in drag started
						let _ = buffer.push(TouchEvent::DragEnd {
							start,
							end: current,
							delta: Point2::zero()
						});
						self.state = State::Idle;
					}
				} else if event.z >= self.config.touch_end_z_threshold {
					let _ = buffer.push(TouchEvent::TouchEnd { position: start });
					self.state = State::Idle;
				}
			}
			State::DragStarted { start, current } => {
				let delta = event.point - current;
				let current = current + delta;

				if event.z >= self.config.drag_end_z_threshold {
					let _ = buffer.push(TouchEvent::DragEnd {
						start,
						end: current,
						delta
					});
					self.state = State::Idle;
				} else if delta.length() > f32::EPSILON {
					let _ = buffer.push(TouchEvent::DragContinue {
						start,
						current,
						delta
					});
					self.state = State::DragStarted { start, current };
				}
			}
		}

		buffer
	}
}
