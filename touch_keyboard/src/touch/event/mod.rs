use crate::util::point::Point2;

pub mod machine;

#[cfg_attr(feature = "semihosting", derive(defmt::Format))]
#[derive(Copy, Clone)]
pub enum TouchEvent {
	/// Fires when a new touch is detected.
	///
	/// This is always fired, even at the beginning of drags.
	TouchBegin { position: Point2 },
	/// Fires when an existing touch is released without drag.
	///
	/// The `position` is the same as the corresponding `TouchBegin`.
	TouchEnd { position: Point2 },

	/// Fires when a touch has begun and has turned into a drag.
	///
	/// The `start` is the same as the previous `TouchBegin`.
	DragBegin { start: Point2, delta: Point2 },
	/// Fires when an existing drag has an update.
	///
	/// Returns the `start` of the corresponding `DragBegin`, the `current` position (with delta) and the `delta`.
	DragContinue {
		start: Point2,
		current: Point2,
		delta: Point2
	},
	/// Fires when a drag ends.
	///
	/// Returns the `start` of the corresponding `DragBegin`, the `end` position (with delta) and the final `delta`.
	DragEnd {
		start: Point2,
		end: Point2,
		delta: Point2
	}
}
