use core::mem::MaybeUninit;

use embedded_hal::blocking::i2c::{WriteIter, WriteRead};

use bitregister::{
	prelude::MaybeUninitSliceExt,
	register::access::{
		ResourceAccessBase,
		ResourceAccessRead,
		ResourceAccessReadError,
		ResourceAccessWrite,
		ResourceAccessWriteError,
		ResourceAccessWriteIter
	}
};

pub struct I2cAccess<Bus: WriteIter<u8> + WriteRead<u8>, const ADDRESS: u8>(pub Bus);
impl<Bus: WriteIter<u8> + WriteRead<u8>, const ADDRESS: u8> ResourceAccessBase
	for I2cAccess<Bus, ADDRESS>
{
	type AddressRepr = u8;
	type WordRepr = u8;
}
#[allow(unused_unsafe)]
impl<Bus: WriteIter<u8> + WriteRead<u8>, const ADDRESS: u8> ResourceAccessRead
	for I2cAccess<Bus, ADDRESS>
{
	unsafe fn read(
		&mut self,
		address: Self::AddressRepr,
		dst: &mut [core::mem::MaybeUninit<Self::WordRepr>]
	) -> Result<usize, ResourceAccessReadError> {
		// since the i2c interface requires &mut [u8] and we have &mut [MaybeUninit<u8>] we have to initialize the memory here
		dst.fill(core::mem::MaybeUninit::zeroed());
		let dst = MaybeUninit::peel_slice_mut(dst);

		self.0
			.write_read(ADDRESS, core::slice::from_ref(&address), dst)
			.map_err(|_| ResourceAccessReadError::ReadError)?;

		Ok(dst.len())
	}
}
impl<Bus: WriteIter<u8> + WriteRead<u8>, const ADDRESS: u8> ResourceAccessWrite
	for I2cAccess<Bus, ADDRESS>
{
	unsafe fn write(
		&mut self,
		address: Self::AddressRepr,
		src: &[Self::WordRepr]
	) -> Result<(), ResourceAccessWriteError> {
		self.write_iter(address, src.into_iter().copied())
	}
}
impl<Bus: WriteIter<u8> + WriteRead<u8>, const ADDRESS: u8> ResourceAccessWriteIter
	for I2cAccess<Bus, ADDRESS>
{
	unsafe fn write_iter<I: Iterator<Item = Self::WordRepr>>(
		&mut self,
		address: Self::AddressRepr,
		iter: I
	) -> Result<(), ResourceAccessWriteError> {
		self.0
			.write(ADDRESS, core::iter::once(address).chain(iter))
			.map_err(|_| ResourceAccessWriteError::WriteError)
	}
}
