use core::mem::MaybeUninit;

use stm32f4xx_hal::{
	gpio::{
		self,
		gpiob::{PB6, PB9},
		gpioi::PI2,
		AlternateOD,
		Floating,
		Input,
		PullUp
	},
	i2c::I2c,
	pac::{EXTI, I2C1},
	prelude::*,
	rcc::Clocks,
	syscfg::SysCfg
};

use bitregister::prelude::{BitValue, MaybeUninitSliceExt};
use stmpe811::{ExpanderDriver, TouchCoordinateX, TouchCoordinateY};

mod access;
mod event;

use event::machine::{EventStateMachine, FifoEvent, TouchConfig};
pub use event::TouchEvent;

use crate::util::{point::Point2, time::Instant};

type ExpanderAccess =
	access::I2cAccess<I2c<I2C1, (PB6<AlternateOD<4>>, PB9<AlternateOD<4>>)>, EXPANDER_ADDRESS>;
const EXPANDER_ADDRESS: u8 = 0x41;

pub struct TouchDriver {
	expander: ExpanderDriver<ExpanderAccess>,
	interrupt_pin: PI2<Input<PullUp>>,

	fifo_buffer: [stmpe811::TouchDataXYZ_XYZ; Self::FIFO_BUF_LEN],
	event_state_machine: EventStateMachine
}
impl TouchDriver {
	const FIFO_BUF_LEN: usize = 8;
	const FIFO_THRESHOLD: u8 = 1;

	pub fn initialize(
		syscfg: &mut SysCfg,
		clocks: Clocks,
		interrupt_controller: &mut EXTI,
		i2c1: I2C1,
		pb6: PB6<Input<Floating>>,
		pb9: PB9<Input<Floating>>,
		pi2: PI2<Input<Floating>>
	) -> Self {
		let sck_pin = pb6.into_alternate_open_drain();
		let sda_pin = pb9.into_alternate_open_drain();

		let i2c = I2c::new(i2c1, (sck_pin, sda_pin), 200.khz(), clocks);
		let i2c: ExpanderAccess = access::I2cAccess(i2c);

		let mut expander = unsafe { ExpanderDriver::new(i2c) };
		expander
			.initialize()
			.expect("Could not initialize io expander");

		// configure touch mode
		expander
			.write(stmpe811::TouchscreenControl::new(
				stmpe811::TouchStatus::Idle,
				stmpe811::TouchTrackingIndex::NoTracking,
				stmpe811::TouchMode::ModeXYZ,
				stmpe811::Touchscreen::Enabled
			))
			.expect("Could not write to io expander");

		// set up interrupts
		expander
			.write(stmpe811::InterruptControl::new(
				stmpe811::InterruptPinPolarity::ActiveLow,
				stmpe811::InterruptType::Level,
				stmpe811::GlobalInterrupts::Enabled
			))
			.expect("Could not write to io expander");
		expander
			.write(stmpe811::InterruptEnable::new(
				stmpe811::GpioInterrupts::Disabled,
				stmpe811::AdcInterrupts::Disabled,
				stmpe811::TemperatureSensorInterrupts::Disabled,
				stmpe811::TouchFifoEmptyInterrupts::Disabled,
				stmpe811::TouchFifoFullInterrupts::Disabled,
				stmpe811::TouchFifoOverflowInterrupts::Disabled,
				stmpe811::TouchFifoThresholdInterrupts::Enabled,
				stmpe811::TouchDetectInterrupts::Disabled
			))
			.expect("Could not write to io expander");
		expander
			.write(stmpe811::TouchscreenFifoThreshold::from_bits(Self::FIFO_THRESHOLD).unwrap())
			.expect("Could not write to io expander");

		let mut interrupt_pin = pi2.into_pull_up_input();
		interrupt_pin.make_interrupt_source(syscfg);
		interrupt_pin.enable_interrupt(interrupt_controller);
		interrupt_pin.trigger_on_edge(interrupt_controller, gpio::Edge::Falling);

		TouchDriver {
			expander,
			interrupt_pin,
			fifo_buffer: [stmpe811::TouchDataXYZ_XYZ::DEFAULT; Self::FIFO_BUF_LEN],
			event_state_machine: EventStateMachine::new(TouchConfig {
				touch_begin_z_threshold: 0.80,
				touch_end_z_threshold: 0.90,
				drag_begin_distance_threshold: 0.12,
				drag_end_z_threshold: 0.95
			})
		}
	}

	pub fn clear_interrupt(&mut self) {
		self.interrupt_pin.clear_interrupt_pending_bit();
		self.expander
			.write(stmpe811::InterruptStatus {
				touch_det: stmpe811::TouchDetectInterruptStatus::Active,
				fifo_th: stmpe811::TouchFifoThresholdStatus::Active,
				fifo_empty: stmpe811::TouchFifoEmptyStatus::Active,
				..Default::default()
			})
			.expect("Could not write to io expander");
	}

	fn read_fifo_len(&mut self) -> usize {
		self.expander
			.read::<stmpe811::TouchFifoSize>()
			.expect("Could not read from io expander")
			.to_bits() as usize
	}

	fn map_touch_data(touch_data: &stmpe811::TouchDataXYZ_XYZ) -> FifoEvent {
		// Map the touch data to correspond to the pixel mapping we use in screen
		// i.e. rotate 90
		let x = touch_data.datax.to_bits() as f32 / TouchCoordinateX::MAX.to_bits() as f32;
		let y = touch_data.datay.to_bits() as f32 / TouchCoordinateY::MAX.to_bits() as f32;

		// also correct for the actual reported dimensions on the display - this is probably specific to the display unit?
		let x = ((x - 0.05).max(0.0) / (1.00 - 0.05)).min(1.0);

		let y = ((y - 0.07).max(0.0) / (0.94 - 0.07)).min(1.0);

		let event = FifoEvent {
			point: Point2 { x: y, y: x },
			z: touch_data.dataz.to_bits() as f32 / 64.0
		};

		event
	}

	pub fn poll(&mut self, instant: Instant) -> impl Iterator<Item = TouchEvent> + '_ {
		let fifo_len = self.read_fifo_len();
		let dst_len = self.fifo_buffer.len().min(fifo_len);

		let read_data = self
			.expander
			.read_slice::<_, { Self::FIFO_BUF_LEN }>(MaybeUninit::wrap_slice_mut(
				&mut self.fifo_buffer[.. dst_len]
			))
			.expect("Could not read from io expander");

		// have to move `instant` into the closure
		let event_state_machine = &mut self.event_state_machine;
		read_data.into_iter().flat_map(move |touch_data| {
			event_state_machine.on_event(instant, Self::map_touch_data(touch_data))
		})
	}
}
