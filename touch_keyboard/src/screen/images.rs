use embedded_graphics::{
	draw_target::DrawTarget,
	pixelcolor::Rgb565,
	prelude::{Dimensions, ImageDrawable, OriginDimensions, PixelColor, Size, WebColors},
	primitives::Rectangle
};

pub struct StaticBinaryImage<T: PixelColor, const WIDTH: usize> {
	data: &'static [u8],
	off_color: T,
	on_color: T
}
impl<const WIDTH: usize> StaticBinaryImage<Rgb565, WIDTH> {
	pub const fn new_rgb565(data: &'static [u8], off_color: Rgb565, on_color: Rgb565) -> Self {
		assert!(WIDTH % 8 == 0);
		assert!((data.len() * 8) % WIDTH == 0);

		StaticBinaryImage {
			data,
			off_color,
			on_color
		}
	}
}
impl<T: PixelColor, const WIDTH: usize> OriginDimensions for StaticBinaryImage<T, WIDTH> {
	fn size(&self) -> Size {
		Size {
			width: WIDTH as u32,
			height: (self.data.len() * 8 / WIDTH) as u32
		}
	}
}
impl<T: PixelColor, const WIDTH: usize> ImageDrawable for StaticBinaryImage<T, WIDTH> {
	type Color = T;

	fn draw<D: DrawTarget<Color = Self::Color>>(&self, target: &mut D) -> Result<(), D::Error> {
		target.fill_contiguous(
			&self.bounding_box(),
			(0 .. self.data.len() * 8).map(|index| {
				let byte = self.data[index / 8];

				let bit = (byte >> (7 - index % 8)) & 0b1;
				if bit == 1 {
					self.on_color
				} else {
					self.off_color
				}
			})
		)
	}

	fn draw_sub_image<D: DrawTarget<Color = Self::Color>>(
		&self,
		target: &mut D,
		area: &Rectangle
	) -> Result<(), D::Error> {
		unimplemented!()
	}
}

#[rustfmt::skip]
const RAW_BACKSPACE: &'static [u8] = &[
0b00000001, 0b11111111, 0b11111111,
0b00000011, 0b11111111, 0b11111111,
0b00000111, 0b11100111, 0b11110011,
0b00001111, 0b11100011, 0b11100011,
0b00011111, 0b11110001, 0b11000111,
0b00111111, 0b11111000, 0b10001111,
0b01111111, 0b11111100, 0b00011111,
0b11111111, 0b11111110, 0b00111111,
0b01111111, 0b11111100, 0b00011111,
0b00111111, 0b11111000, 0b10001111,
0b00011111, 0b11110001, 0b11000111,
0b00001111, 0b11100011, 0b11100011,
0b00000111, 0b11100111, 0b11110011,
0b00000011, 0b11111111, 0b11111111,
0b00000001, 0b11111111, 0b11111111,
];
pub const IMAGE_BACKSPACE: StaticBinaryImage<Rgb565, 24> =
	StaticBinaryImage::new_rgb565(RAW_BACKSPACE, Rgb565::CSS_DIM_GRAY, Rgb565::CSS_BLACK);

#[rustfmt::skip]
const RAW_ENTER: &'static [u8] = &[
0b00000000, 0b00000000, 0b00000000,
0b00000000, 0b00000000, 0b00000000,
0b00000000, 0b10000000, 0b00111000,
0b00000001, 0b10000000, 0b00111000,
0b00000011, 0b00000000, 0b00111000,
0b00000110, 0b00000000, 0b00111000,
0b00001111, 0b11111111, 0b11111000,
0b00011111, 0b11111111, 0b11111000,
0b00001111, 0b11111111, 0b11111000,
0b00000110, 0b00000000, 0b00000000,
0b00000011, 0b00000000, 0b00000000,
0b00000001, 0b10000000, 0b00000000,
0b00000000, 0b10000000, 0b00000000,
0b00000000, 0b00000000, 0b00000000,
0b00000000, 0b00000000, 0b00000000,
];
pub const IMAGE_ENTER: StaticBinaryImage<Rgb565, 24> =
	StaticBinaryImage::new_rgb565(RAW_ENTER, Rgb565::CSS_DIM_GRAY, Rgb565::CSS_BLACK);
