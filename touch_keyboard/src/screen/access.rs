use bitregister::register::access::{
	ResourceAccessBase,
	ResourceAccessRead,
	ResourceAccessReadError,
	ResourceAccessWrite,
	ResourceAccessWriteError,
	ResourceAccessWriteIter
};
use stm32f4xx_hal::fsmc_lcd::{Lcd, SubBank};

pub struct LcdWrap<S: SubBank>(pub Lcd<S>);
impl<S: SubBank> ResourceAccessBase for LcdWrap<S> {
	type AddressRepr = u16;
	type WordRepr = u16;
}
impl<S: SubBank> ResourceAccessRead for LcdWrap<S> {
	unsafe fn read(
		&mut self,
		address: Self::AddressRepr,
		dst: &mut [core::mem::MaybeUninit<Self::WordRepr>]
	) -> Result<usize, ResourceAccessReadError> {
		self.0.write_command(address);

		let mut read = 0;
		for i in 0 .. dst.len() {
			let word = self.0.read_data();
			dst[i].write(word);

			read += 1;
		}

		Ok(read)
	}
}
impl<S: SubBank> ResourceAccessWriteIter for LcdWrap<S> {
	unsafe fn write_iter<I: Iterator<Item = Self::WordRepr>>(
		&mut self,
		address: Self::AddressRepr,
		iter: I
	) -> Result<(), ResourceAccessWriteError> {
		self.0.write_command(address);

		for word in iter {
			self.0.write_data(word);
		}

		Ok(())
	}
}
impl<S: SubBank> ResourceAccessWrite for LcdWrap<S> {
	unsafe fn write(
		&mut self,
		address: Self::AddressRepr,
		src: &[Self::WordRepr]
	) -> Result<(), ResourceAccessWriteError> {
		self.write_iter(address, src.into_iter().copied())
	}
}
