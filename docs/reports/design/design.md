---
title: KRP touch keyboard
date: 2022-01-02
author: lavusedu@fel.cvut.cz
geometry: margin=1.5cm

mainfont: Fira Sans
---

Touch keyboard implementation using the STM3240G-EVAL board with the STM32F407 MCU.

# Design

This project consists of the following high-level components:

* Functional layer of an USB OTG HID device (configuring hardware, absctracting over pipes, sending and receiving data)
* ILI9325 screen driver (configuring registers, drawing)
* STMPE811 io expander driver (configuring registers, reading touch data)
* GPIO pin abstraction to respond to button and toggle leds
* The main application program that composes all the components

## USB device

The USB HID device configures itself to deliver both pointer and keyboard events. This is similar to how most modern mice with user-configurable buttons function.

## ILI9325 screen driver

The ILI9325 touchscreen is the main interaction point between the device and the user. It is used to draw the UI and visualize the keypresses, as well as to collect the touch events (through the STMPE811 io exapander).

## STMPE811 io expander

The expander is connected to the touchscreen X+, X-, Y+ and Y- pins and translates the differential coding into touch events. These events are then stored in an queue and retrieved by this driver to be handled according to the application logic.

## GPIO pin abstraction

A secondary user interface element are the LED lights used to signal keyboard lock keys - the caps lock, num lock and scroll lock. A final element is a push button used to change the current device mode.

## Application program

The application program connects other components and implements the keyboard logic. It achieves this by making use of the NVIC (nested vector interupt controller) inside the MCU. By using this controller the program is able to mimic the behavior of multiple processes running on the same core, similar to how an OS might handle such separate processes. The program also facilitates delivery of inter-process messages to allow communication.

The program is composed of the following tasks:

* `usb_interrupt` - the usb interrupt connected to the on-board USB OTG port, it spawns the `usb_poll` task
* `usb_timeout` - timeout task scheduled from `usb_poll`, it spawns the `usb_poll` to ensure the `usb_poll` task is called periodically
* `usb_poll` - task that polls the usb status using `usb_driver`, it passes host-to-device usb messages to the `presenter`
* `input_buttons_interrupt` - interrupt connected to the physical on-board push buttons, it passes an input event to the `presenter`
* `touch_interrupt` - interrupt connected to the STMPE811 io expander, it contains a state machine translating io expander events into high-level touch events (start, end, drag) and passes the touch event to the `presenter`
* `screen_draw` - output task scheduled through the `presenter`, it draws to the screen using the ILI9325 screen driver
* `output_leds` - output task spawned through the `presenter`, it toggles on-board status leds
* `usb_output` - output task spawned through the `presenter`, it generates device-to-host messages using the `usb_driver`
* `presenter_timeout` - timeout task scheduled through the `presenter`, it spawns the `presenter` to generate timing events (important for animation steps, button hold detection, etc.)

![](task_diagram.png)

### Presenter

The presenter is a state machine in the center of the application. It receives all events from all sources and generates the appropriate responses for any outputs. Depending on the current mode of operation it maps the events into appropriate draw commands and HID outputs. It also defines the screen UI element layouts.

### Modes

The application has three modes switched by the physical push button labeled as "key" on the board.

1. Touchpad mode - the color of the border of the screen changes depending on the input event (red: idle, yellow: touch, cyan: drag). This more reports pointer events.
2. Arrow keys mode - Mode containing only the arrow keys.
3. Keyboard mode - Mode containing a full QWERTY keyboard and the top number row, as well as modifier keys, space, backspace, comma, period and enter. The behavior of some of these keys depends on the language setting in the host.

# Implementation

The application program is written in Rust programming language. It it split into multiple crates and composed in the main `touch_keyboard` crate.

## bitregister

This crate contains abstraction built around the notion of configurable device registers. Each register is representable by a fixed number of bits. The registers defined fields and specify which bits of the register the fields cover and what are the allowed combinations. This greatly improves the developer experience as the register value types are properly named a their values are typechecked at compile time, preventing misconfiguration due to magic values.

The crate also builds abstraction for accesing the register values - both one at a time and multiple accesses. The register access defines a bus word width as its communication unit and register accesses are performed over the access abstraction. Thanks to this, it is for example possible to interact with 16 bit registers over both a 16 bit bus and an I2C 8 bit bus by simply switching the access implementations.

## ili9325

Crate implementing a driver for the ILI9325 touchscreen display based on the `bitregister` crate. Almost all the ILI9325 configuration registers are defined using the `bitregister` abstraction and thus are bus-agnostic.

The driver implements `embeddec-graphics` interface which allows seamless interaction with the Rust embedded graphics interaction. It also defines abstraction over mapping pixels to the internal ILI9325 GRAM - this allows switching the origin point of the draw calls using generic marker types with no runtime overhead.

## stmpe811

Crate implementing a driver to the STMPE811 io expander. It covers all the common configuration registers and the touch registers using `bitregister` abstraction.

## usb_hid

Crate implementing the HID USB class on top of an existing `usb-device` crate. It handles the USB communication according to the HID specification.

A very important part of the crate is a macro which generates HID descriptors, which are an integral part of USB HID specification.

This crate also makes use of the `bitregister` abstraction for packing and unpacking bits into words.

## touch_keyboard

The main crate combining all the previous crates and other crates from the ecosystem.

At the lowest level, this crate depends on `cortex-m` libraries from the ecosystem, abstracting over ARM cortex chip functionalities. The [`rtic`](rtic.rs) framework is used to drive all the tasks. This framework makes use of the NVIC (nested vector interrupt controller) part of Cortex-M chips to run tasks at multiple priority levels and handle preemption hazards. This framework handles the orchestration of tasks so that we can focus on the actual implementation.

Another important pair of libraries are [`embedded-hal`](https://github.com/rust-embedded/embedded-hal) and [`stm32f4xx-hal`](https://github.com/stm32-rs/stm32f4xx-hal). These library provide the common embedded interfaces and their implementations respectively, allowing easy integration with the rest of the Rust embedded ecosystem.

For debugging and semihosting support there are tools and crates from the [knurling-rs](https://knurling.ferrous-systems.com/) project, such as `flip-link`, `probe-run` and `defmt` crate for logging from the board over STLink connection.

### USB HID report descriptors

Here is the HID report descriptor in textual form. It defines three reports using the report id tag:

1. Keyboard input (device-to-host) reports up to 6 keyboard key presses plus modifier keys (shift, ctrl, alt, meta)
2. Keyboard output (host-to-device) reports status LEDs
3. Mouse input (device-to-host) reports mouse buttons and relative pointer events

```
Usage Page (Desktop)
Usage (Keyboard)
Collection (Application)
	Report ID (1)
	Usage Page (Keyboard)

	Local Minimum (0)
	Local Maximum (1)
	Report Size (1)
	Report Count (8)

	Usage Minimum (0xE0)
	Usage Maximum (0xE7)

	Input (Variable)

	Local Minimum (0x00)
	Local Maximum (0xDD)
	Report Size (8)
	Report Count (6)

	Usage Minimum (0x00)
	Usage Maximum (0xDD)

	Input (HasNull)
End Collection

Usage Page (Desktop)
Usage (Keyboard)
Collection (Application)
	Report ID (2)
	Usage Page (LED)
	Logical Minimum (0)
	Logical Maximum (1)
	Report Size (1)
	Report Count (3)

	Usage Minimum (0x01)
	Usage Maximum (0x03)

	Output (Variable)
End Collection

Usage Page (Desktop)
Usage (Mouse)
Collection (Application)
	Usage (Pointer)
	Collection (Physical)
		Report ID (3)
		Usage Page (Buttons)

		Logical Minimum (0)
		Logical Maximum (1)
		Report Size (1)
		Report Count (8)

		Usage Minimum (1)
		Usage Maximum (8)

		Input (Data, Variable, Absolute)

		Usage Page (Desktop)

		Logical Minimum (-127)
		Logical Maximum (127)
		Report Size (8)
		Report Count (2)

		Usage (X)
		Usage (Y)

		Input (Data, Variable, Relative)
	End Collection
End Collection
```

# Conclusion

Over all the Rust ecosystem provides ample tooling to handle embedded development, including support for STM32 MCUs, drawing simple graphics on embedded hardware and dealing with low level registers, buses and GPIO pins. A lot of work was focused on implementing drivers for the ILI9325 display and the STMPE811 io expander, as those libraries were not available before.

This text written from the board. The quick brown fox jumps over the lazy dog. All hail Rust!
