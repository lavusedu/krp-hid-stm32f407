#!/bin/sh

NAME=$1

pandoc --standalone --from markdown --to pdf --pdf-engine tectonic --output "lavusedu-$NAME.pdf" "$NAME.md"
mupdf "lavusedu-$NAME.pdf"