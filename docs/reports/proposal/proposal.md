---
date: 2021-09-27
author: lavusedu
geometry: margin=1.5cm

mainfont: Fira Sans
---

# lavusedu - KRP project

Project implementing an USB communication interface on the [STM32F407IG](https://www.st.com/en/evaluation-tools/stm3240g-eval.html) board.

## Function

The project will implement a touchscreen keyboard of the HID (`03h`) USB device class. The LCD display will be used to display a digital keyboard and respond to touch presses by sending the appropriate USB message to the host device.

The device shall also respond to host keyboard LED messages and use board LEDs to display the current state.

## Software

The software will be written in Rust programming language. The Rust ecosystem already provider Hardware Abstraction Layer (HAL) for STM32F4xx class of boards (https://crates.io/crates/stm32f4).

The software is expected to take care of responding to the proper USB events, handling display redrawing, touch input, and propagating touch input to the USB as key presses.
