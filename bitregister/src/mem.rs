use core::mem::MaybeUninit;

use bytemuck::{Pod, TransparentWrapper};

/// Extension trait providing functions on `MaybeUninit` which are not stable yet.
pub trait MaybeUninitSliceExt<T>: Sized {
	fn new_uninit_array<const LEN: usize>() -> [Self; LEN];

	/// Peels array elements from `MaybeUninit`.
	///
	/// ### Safety
	/// Peeled elements must be initialized.
	unsafe fn peel_array<const LEN: usize>(array: [Self; LEN]) -> [T; LEN];

	/// Wraps slice elements in `MaybeUninit`.
	fn wrap_slice(slice: &[T]) -> &[Self];

	/// Peels slice elements from `MaybeUninit`.
	///
	/// ### Safety
	/// Peeled elements must be initialized.
	unsafe fn peel_slice(slice: &[Self]) -> &[T];

	/// Wraps slice elements in `MaybeUninit`.
	fn wrap_slice_mut(slice: &mut [T]) -> &mut [Self];

	/// Peels slice elements from `MaybeUninit`.
	///
	/// ### Safety
	/// Peeled elements must be initialized.
	unsafe fn peel_slice_mut(slice: &mut [Self]) -> &mut [T];
}

impl<T> MaybeUninitSliceExt<T> for MaybeUninit<T> {
	fn new_uninit_array<const LEN: usize>() -> [Self; LEN] {
		// SAFETY: From unstable core::mem::MaybeUninit::uninit_array
		unsafe { MaybeUninit::<[MaybeUninit<T>; LEN]>::uninit().assume_init() }
	}

	unsafe fn peel_array<const LEN: usize>(array: [Self; LEN]) -> [T; LEN] {
		(&array as *const _ as *const [T; LEN]).read()
	}

	fn wrap_slice(slice: &[T]) -> &[Self] {
		unsafe { &*(slice as *const [T] as *const [Self]) }
	}

	unsafe fn peel_slice(slice: &[Self]) -> &[T] {
		&*(slice as *const [Self] as *const [T])
	}

	fn wrap_slice_mut(slice: &mut [T]) -> &mut [Self] {
		unsafe { &mut *(slice as *mut [T] as *mut [Self]) }
	}

	unsafe fn peel_slice_mut(slice: &mut [Self]) -> &mut [T] {
		&mut *(slice as *mut [Self] as *mut [T])
	}
}

pub trait MaybeUninitPodSliceExt<T: Pod>: Sized {
	fn cast_slice<U: Pod>(slice: &[Self]) -> &[MaybeUninit<U>];
	fn cast_slice_mut<U: Pod>(slice: &mut [Self]) -> &mut [MaybeUninit<U>];
}

#[derive(Copy, Clone)]
#[repr(transparent)]
/// Stupid hack - MaybeUninit is not Pod so we cannot use `bytemuck::cast_slice` functions. Instead, we implement `TransparentWrapper` and `Pod` on this
/// and just wing it.
///
/// Since we solemly swear never to use it any other way, it should be fine. Maybe.
struct StupidHackToCastMaybeUninitSlice<R: Copy>(MaybeUninit<R>);
unsafe impl<R: Copy + bytemuck::Pod> bytemuck::TransparentWrapper<MaybeUninit<R>>
	for StupidHackToCastMaybeUninitSlice<R>
{
}
unsafe impl<R: Copy + bytemuck::Pod> bytemuck::Zeroable for StupidHackToCastMaybeUninitSlice<R> {}
unsafe impl<R: Copy + 'static + bytemuck::Pod> bytemuck::Pod
	for StupidHackToCastMaybeUninitSlice<R>
{
}

impl<T: Pod> MaybeUninitPodSliceExt<T> for MaybeUninit<T> {
	fn cast_slice<U: Pod>(slice: &[Self]) -> &[MaybeUninit<U>] {
		let stupid = StupidHackToCastMaybeUninitSlice::wrap_slice(slice);
		let cast = bytemuck::cast_slice(stupid);
		StupidHackToCastMaybeUninitSlice::peel_slice(cast)
	}

	fn cast_slice_mut<U: Pod>(slice: &mut [Self]) -> &mut [MaybeUninit<U>] {
		let stupid = StupidHackToCastMaybeUninitSlice::wrap_slice_mut(slice);
		let cast = bytemuck::cast_slice_mut(stupid);
		StupidHackToCastMaybeUninitSlice::peel_slice_mut(cast)
	}
}

/// This is the lifted from unstable stdlib
pub fn collect_into_array<I: Iterator, const N: usize>(iter: &mut I) -> Option<[I::Item; N]> {
	use core::{mem, ptr};

	if N == 0 {
		// SAFETY: An empty array is always inhabited and has no validity invariants.
		return unsafe { Some(mem::zeroed()) }
	}

	struct Guard<T, const N: usize> {
		ptr: *mut T,
		initialized: usize
	}

	impl<T, const N: usize> Drop for Guard<T, N> {
		fn drop(&mut self) {
			debug_assert!(self.initialized <= N);

			let initialized_part = ptr::slice_from_raw_parts_mut(self.ptr, self.initialized);

			// SAFETY: this raw slice will contain only initialized objects.
			unsafe {
				ptr::drop_in_place(initialized_part);
			}
		}
	}

	let mut array = MaybeUninit::new_uninit_array::<N>();
	let mut guard: Guard<_, N> = Guard {
		ptr: (&mut array as *mut _ as *mut I::Item),
		initialized: 0
	};

	while let Some(item) = iter.next() {
		// SAFETY: `guard.initialized` starts at 0, is increased by one in the
		// loop and the loop is aborted once it reaches N (which is
		// `array.len()`).
		unsafe {
			array.get_unchecked_mut(guard.initialized).write(item);
		}
		guard.initialized += 1;

		// Check if the whole array was initialized.
		if guard.initialized == N {
			mem::forget(guard);

			// SAFETY: the condition above asserts that all elements are
			// initialized.
			let out = unsafe { MaybeUninit::peel_array(array) };
			return Some(out)
		}
	}

	// This is only reached if the iterator is exhausted before
	// `guard.initialized` reaches `N`. Also note that `guard` is dropped here,
	// dropping all already initialized elements.
	None
}
