use crate::repr::ReprType;

/// Represents a value which can be interpreted as bits.
pub trait BitValue: Sized {
	type BitRepr: ReprType;

	const MASK: Self::BitRepr;

	fn to_bits(&self) -> Self::BitRepr;

	fn from_bits(bits: Self::BitRepr) -> Option<Self>;
}

impl<T: BitValue> BitValue for Option<T> {
	type BitRepr = T::BitRepr;

	const MASK: Self::BitRepr = T::MASK;

	fn to_bits(&self) -> Self::BitRepr {
		match self {
			None => Self::BitRepr::DEFAULT,
			Some(v) => v.to_bits()
		}
	}

	fn from_bits(bits: Self::BitRepr) -> Option<Self> {
		Some(T::from_bits(bits))
	}
}

/// Represents a bitvalue field.
///
/// Bitvalue fields are bit values which have a certain position within the parent bitvalue.
pub unsafe trait BitvalueField<Parent: BitValue>: BitValue
where
	Parent::BitRepr: From<Self::BitRepr> + TryInto<Self::BitRepr>
{
	const SHIFT: usize;

	fn to_parent_bits(&self) -> Parent::BitRepr {
		Parent::BitRepr::from(self.to_bits()) << Self::SHIFT
	}

	fn from_parent_bits(bits: Parent::BitRepr) -> Option<Self> {
		Parent::BitRepr::try_into((bits >> Self::SHIFT) & Parent::BitRepr::from(Self::MASK))
			.ok()
			.and_then(Self::from_bits)
	}
}

/// Defines a bitvalue.
///
/// ```
/// bitvalue! {
/// 	pub bitvalue(u16) Foo {
/// 		A = 0b0,
/// 		B = 0b1
/// 	}
/// }
///
/// bitvalue! {
/// 	pub bitvalue(u8) Bar {
/// 		X = 0b11,
/// 		Y = 0b10
/// 	}
/// }
///
/// bitvalue! {
/// 	pub bitvalue(u32) Baz {
/// 		15 ..= 25465
/// 	}
/// }
/// ```
#[macro_export]
macro_rules! bitvalue {
	(
		$( #[$attr: meta] )*
		$( @[mask = $mask: expr] )?
		$visibility: vis bitvalue($repr: ident) $name: ident {
			$(
				$( #[$variant_attr: meta] )*
				$variant_name: ident $(= $variant_value: expr)?
			),+ $(,)?
		}
	) => {
		#[repr($repr)]
		$( #[$attr] )*
		#[derive(Debug, Copy, Clone)]
		$visibility enum $name {
			$(
				$( #[$variant_attr] )*
				$variant_name $(= $variant_value)?
			),+
		}
		impl $name {
			pub const ALL_VARIANTS: &'static [Self] = &[
				$( Self::$variant_name ),+
			];

			pub const DEFAULT: Self = Self::ALL_VARIANTS[0];
		}
		impl Default for $name {
			fn default() -> Self {
				Self::DEFAULT
			}
		}
		impl $crate::prelude::BitValue for $name {
			type BitRepr = $repr;

			const MASK: Self::BitRepr = {
				let mask: $repr = $( (Self::$variant_name as Self::BitRepr) )|+;
				$( let mask: $repr = $mask; )?

				mask
			};

			fn to_bits(&self) -> Self::BitRepr {
				(*self as $repr) & Self::MASK
			}

			fn from_bits(bits: Self::BitRepr) -> Option<Self> {
				match (bits & Self::MASK) {
					$(
						v if v == Self::$variant_name as Self::BitRepr => Some(Self::$variant_name),
					)+
					_ => None
				}
			}
		}
	};

	(
		$( #[$attr: meta] )*
		$( @[mask = $mask: expr] )?
		$visibility: vis bitvalue($inner: ty) $name: ident {
			$min: literal ..= $max: literal

			$(
				const hack {
					min = $min_inner: expr;
					max = $max_inner: expr;
				}
			)?
		}
	) => {
		$( #[$attr] )*
		#[derive(Debug, Copy, Clone)]
		$visibility struct $name($inner);
		impl $name {
			#[allow(unused_must_use)]
			pub const MIN: Self = Self(
				{
					$min & <Self as $crate::prelude::BitValue>::MASK
					$(; $min_inner)?
				} as _
			);
			#[allow(unused_must_use)]
			pub const MAX: Self = Self(
				{
					$max & <Self as $crate::prelude::BitValue>::MASK
					$(; $max_inner)?
				} as _
			);

			pub const DEFAULT: Self = Self::MIN;
		}
		impl Default for $name {
			fn default() -> Self {
				Self::DEFAULT
			}
		}
		impl $crate::prelude::BitValue for $name {
			type BitRepr = <$inner as $crate::prelude::BitValue>::BitRepr;

			const MASK: Self::BitRepr = {
				let mask = {
					let leading_zeros = Self::BitRepr::leading_zeros($max);

					if leading_zeros == 0 {
						Self::BitRepr::MAX
					} else {
						(
							1 << (Self::BitRepr::BITS - leading_zeros)
						) - 1
					}
				};
				$( let mask: Self::BitRepr = $mask; )?

				mask
			};

			fn to_bits(&self) -> Self::BitRepr {
				self.0.to_bits()
			}

			fn from_bits(bits: Self::BitRepr) -> Option<Self> {
				let val = bits & Self::MASK;

				if val >= Self::MIN.0.to_bits() && val <= Self::MAX.0.to_bits() {
					<$inner>::from_bits(bits).map(Self)
				} else {
					None
				}
			}
		}
	};

	(
		$( #[$attr: meta] )*
		$( @[mask = $mask: expr] )?
		$visibility: vis bitvalue($repr: ident) $name: ident {
			$(
				$( #[$field_attr: meta] )*
				@[shl = $field_shift: literal]
				pub $field_name: ident: $field_type: ty
			),+ $(,)?
		}
	) => {
		$( #[$attr] )*
		#[derive(Debug, Clone, Copy)]
		$visibility struct $name {
			$(
				$( #[$field_attr] )*
				pub $field_name: $field_type
			),+
		}
		impl $name {
			pub const DEFAULT: Self = {
				use $crate::prelude::ReprType;

				Self {
					$( $field_name: <$field_type>::DEFAULT ),+
				}
			};

			#[allow(dead_code)]
			pub const fn new(
				$( $field_name: $field_type ),+
			) -> Self {
				Self {
					$( $field_name ),+
				}
			}
		}
		impl Default for $name {
			fn default() -> Self {
				Self::DEFAULT
			}
		}
		impl $crate::prelude::BitValue for $name {
			type BitRepr = $repr;

			const MASK: Self::BitRepr = {
				// TODO: Need const fn in traits, otherwise this fails when using u24
				// let mask: $repr = $(
				// 	(<$field_type>::MASK as Self::BitRepr)
				// )|+;
				#[allow(unused_variables)]
				let mask = Self::BitRepr::MAX;
				$( let mask: $repr = $mask; )?

				mask
			};

			fn to_bits(&self) -> Self::BitRepr {
				use $crate::prelude::BitvalueField;

				$(
					BitvalueField::<Self>::to_parent_bits(&self.$field_name)
				)|+
			}

			fn from_bits(bits: Self::BitRepr) -> Option<Self> {
				use $crate::prelude::BitvalueField;

				$(
					let $field_name = BitvalueField::<Self>::from_parent_bits(bits)?;
				)+

				Some(
					Self {
						$( $field_name ),+
					}
				)
			}
		}
		$(
			unsafe impl $crate::prelude::BitvalueField<$name> for $field_type {
				const SHIFT: usize = $field_shift;
			}
		)+
	};
}
