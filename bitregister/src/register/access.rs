use core::mem::MaybeUninit;

use displaydoc::Display;

use crate::{
	prelude::{BitRegister, EndianSetting, MaybeUninitSliceExt, ReprType},
	repr::ReprCastSliceFrom
};

#[derive(Debug, Display)]
pub enum ResourceAccessReadError {
	/// Unable to read from the resource
	ReadError,
	/// Expected to read {expected} words but only read {actual}
	PartialRead { expected: usize, actual: usize }
}

#[derive(Debug, Display)]
pub enum ResourceAccessWriteError {
	/// Unable to write to the resource
	WriteError
}

pub trait ResourceAccessBase {
	/// Repr of register address of the backing resource.
	type AddressRepr: ReprType;
	/// Repr of one word of the backing resource.
	type WordRepr: ReprType;
}

#[allow(unused_unsafe)]
/// ### Safety
/// All read  functions are unsafe. The specified number of words must be safe to read to the specified address for the resource.
pub trait ResourceAccessRead: ResourceAccessBase {
	/// Reads up to `dst.len()` words from `address`.
	///
	/// Returns number of read words.
	///
	/// ### Safety
	/// * See [ResourceAccessRead](ResourceAccessRead)
	unsafe fn read<'dst>(
		&mut self,
		address: Self::AddressRepr,
		dst: &'dst mut [MaybeUninit<Self::WordRepr>]
	) -> Result<usize, ResourceAccessReadError>;

	/// Reads up to `dst.len().min(BUF_LEN) * size_of::<Self::WordRepr>() / size_of::<R::Repr>()` words from `R::ADDRESS` and converts them into values of `R`.
	///
	/// Returns a subslice of `dst` with the read values. Stops reading if any read value is not parseable into `R` (i.e. `R::from_bits` returns `None`) or
	/// if the buffer is full. The buffer has room to hold `BUF_LEN` values of `R` (and thus statically allocates `size_of::<R::Repr> * BUF_LEN` memory).
	///
	/// ### Safety
	/// * See [ResourceAccessRead](ResourceAccessRead)
	unsafe fn read_register<'dst, R: BitRegister, E: EndianSetting, const BUF_LEN: usize>(
		&mut self,
		dst: &'dst mut [MaybeUninit<R>]
	) -> Result<&'dst mut [R], ResourceAccessReadError>
	where
		Self::AddressRepr: From<R::AddressRepr>,
		Self::WordRepr: ReprCastSliceFrom<R::BitRepr>
	{
		// prepare buffer
		let mut buffer: [MaybeUninit<R::BitRepr>; BUF_LEN] = MaybeUninit::new_uninit_array();

		// number of read R::BitRepr items
		let read_count = {
			let address = Self::AddressRepr::from(R::ADDRESS);

			// don't read more than dst.len() elements even if we have a bigger buffer
			let dst_len = dst.len().min(BUF_LEN);
			let word_dst = Self::WordRepr::cast_uninit_slice_mut_from(&mut buffer[.. dst_len]);

			// number of read bus words
			let read_count_words = unsafe { self.read(address, word_dst)? };

			match Self::WordRepr::try_reverse_len(read_count_words) {
				None => {
					return Err(ResourceAccessReadError::PartialRead {
						expected: Self::WordRepr::SIZE_FACTOR,
						actual: Self::WordRepr::SIZE_FACTOR
							- (read_count_words % Self::WordRepr::SIZE_FACTOR)
					})
				}
				Some(read_count) => read_count
			}
		};

		let mut parsed = 0;
		for i in 0 .. read_count {
			// SAFETY: Contract of `read_register_repr`
			let bits = unsafe { buffer[i].assume_init() };

			let bits_ne = E::to_ne(bits);

			match R::from_bits(bits_ne) {
				None => break,
				Some(register) => {
					dst[i].write(register);
				}
			}

			parsed += 1;
		}

		Ok(unsafe { MaybeUninit::peel_slice_mut(&mut dst[.. parsed]) })
	}

	/// Convenience to [Self::read_register](ResourceAccessRead::read_register) with just one result value.
	///
	/// ### Safety
	/// * See [ResourceAccessRead](ResourceAccessRead)
	unsafe fn read_register_once<R: BitRegister, E: EndianSetting>(
		&mut self
	) -> Result<Option<R>, ResourceAccessReadError>
	where
		Self::AddressRepr: From<R::AddressRepr>,
		Self::WordRepr: ReprCastSliceFrom<R::BitRepr>
	{
		let mut dst = MaybeUninit::<R>::uninit();

		let read = unsafe { self.read_register::<R, E, 1>(core::slice::from_mut(&mut dst))? }.len();

		let result = match read {
			1 => Some(unsafe { dst.assume_init() }),
			_ => None
		};

		Ok(result)
	}
}

#[allow(unused_unsafe)]
pub trait ResourceAccessReadIter: ResourceAccessRead {
	type Iter: Iterator<Item = Result<Self::WordRepr, ResourceAccessReadError>>;

	/// Reads up to `count` words from `address`.
	///
	/// ### Safety
	/// * See [ResourceAccessRead](ResourceAccessRead)
	unsafe fn read_iter(&mut self, address: Self::AddressRepr, count: usize) -> Self::Iter;

	// TODO: Needs reverse of flat_map
	// /// Reads up to `count * size_of::<R::Repr>() / size_of::<Self::WordRepr>()` words from `address`.
	// ///
	// /// ### Safety
	// /// * See [ResourceAccessRead](ResourceAccessRead)
	// unsafe fn read_register_iter<R: BitRegister, E: EndianSetting>(&mut self, count: usize) -> core::iter::Map<
	// 	Self::Iter,
	// 	fn(<Self::Iter as Iterator>::Item) -> Result<Option<R>, ResourceAccessReadError>
	// >
	// where
	// 	Self::AddressRepr: From<R::AddressRepr>,
	// 	Self::WordRepr: ReprCastSliceFrom<R::BitRepr>
	// {
	// 	let address = Self::AddressRepr::from(R::ADDRESS);

	// 	unsafe {
	// 		self.read_iter(address, count * Self::WordRepr::SIZE_FACTOR).map(
	// 			|res| res.map(
	// 				|repr| R::from_bits(E::to_ne(repr))
	// 			)
	// 		)
	// 	}
	// }
}

#[allow(unused_unsafe)]
/// ### Safety
/// All write functions are unsafe. The specified number of words must be safe to write to the specified address for the resource.
pub trait ResourceAccessWrite: ResourceAccessBase {
	/// Writes `src.len()` words to `address`.
	///
	/// ### Safety
	/// * See [ResourceAccessWrite](ResourceAccessWrite)
	unsafe fn write(
		&mut self,
		address: Self::AddressRepr,
		src: &[Self::WordRepr]
	) -> Result<(), ResourceAccessWriteError>;

	/// Writes `src.len().min(BUF_LEN) * size_of::<R::Repr>() / size_of::<Self::WordRepr>()` words into `R::ADDRESS`.
	///
	/// ### Safety
	/// * See [ResourceAccessWrite](ResourceAccessWrite)
	unsafe fn write_register<R: BitRegister, E: EndianSetting, const BUF_LEN: usize>(
		&mut self,
		src: &[R]
	) -> Result<(), ResourceAccessWriteError>
	where
		Self::AddressRepr: From<R::AddressRepr>,
		Self::WordRepr: ReprCastSliceFrom<R::BitRepr>
	{
		let mut buffer: [MaybeUninit<R::BitRepr>; BUF_LEN] = MaybeUninit::new_uninit_array();

		let used_len = src.len().min(buffer.len());
		for i in 0 .. used_len {
			let bits_ne = src[i].to_bits();
			let bits = E::from_ne(bits_ne);

			buffer[i].write(bits);
		}

		let src_repr = unsafe { MaybeUninit::peel_slice(&buffer[.. used_len]) };

		{
			let address = Self::AddressRepr::from(R::ADDRESS);

			{
				let word_src = Self::WordRepr::cast_slice_from(src_repr);
				unsafe {
					self.write(address, word_src)?;
				}
			}
		}

		Ok(())
	}

	/// Convenience to [Self::write_register](ResourceAccessWrite::write_register) with just one value.
	///
	/// ### Safety
	/// * See [ResourceAccessWrite](ResourceAccessWrite)
	unsafe fn write_register_once<R: BitRegister, E: EndianSetting>(
		&mut self,
		value: R
	) -> Result<(), ResourceAccessWriteError>
	where
		Self::AddressRepr: From<R::AddressRepr>,
		Self::WordRepr: ReprCastSliceFrom<R::BitRepr>
	{
		unsafe { self.write_register::<R, E, 1>(core::slice::from_ref(&value)) }
	}
}

#[allow(unused_unsafe)]
pub trait ResourceAccessWriteIter: ResourceAccessWrite {
	/// Writes `iter.len()` words to `address`.
	///
	/// ### Safety
	/// * See [ResourceAccessWrite](ResourceAccessWrite)
	unsafe fn write_iter<I: Iterator<Item = Self::WordRepr>>(
		&mut self,
		address: Self::AddressRepr,
		iter: I
	) -> Result<(), ResourceAccessWriteError>;

	/// Writes `iter.len() * size_of::<R::Repr>() / size_of::<Self::WordRepr>()` words to `address`.
	///
	/// TODO: Currently the bound is `R::Repr = Self::WordRepr`, needs a custom flat_map or something
	///
	/// ### Safety
	/// * See [ResourceAccessWrite](ResourceAccessWrite)
	unsafe fn write_register_iter<
		R: BitRegister<BitRepr = Self::WordRepr>,
		E: EndianSetting,
		I: Iterator<Item = R>
	>(
		&mut self,
		iter: I
	) -> Result<(), ResourceAccessWriteError>
	where
		Self::AddressRepr: From<R::AddressRepr>
	{
		let address = Self::AddressRepr::from(R::ADDRESS);

		unsafe { self.write_iter(address, iter.map(|r| E::from_ne(r.to_bits()))) }
	}
}
