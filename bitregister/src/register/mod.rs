use crate::{repr::ReprType, value::BitValue};

pub mod access;

/// Represents a register.
pub trait BitRegister: BitValue {
	type AddressRepr: ReprType;
	const ADDRESS: Self::AddressRepr;

	fn reset_value() -> Self;
}

/// Defines a bitregister containing bitvalues.
///
/// ```
/// bitregister! {
/// 	pub bitregister(u16 @ 0x22) MyReg {
/// 		@[shl = 3]
/// 		pub foo: Foo,
/// 		@[shl = 0]
/// 		pub bar: Bar
/// 	}
/// }
/// ```
#[macro_export]
macro_rules! bitregister {
	(
		$( #[$attr: meta] )*
		$( @[reset = $reset_value: expr] )?
		$visibility: vis bitregister($repr: ident @ $address: literal: $address_repr: ty) $name: ident {
			$(
				$( #[$field_attr: meta] )*
				@[shl = $field_shift: literal]
				pub $field_name: ident: $field_type: ty
			),+ $(,)?
		}
	) => {
		$crate::bitvalue! {
			$( #[$attr] )*
			@[mask = <$repr>::MAX]
			$visibility bitvalue($repr) $name {
				$(
					$( #[$field_attr] )*
					@[shl = $field_shift]
					pub $field_name: $field_type
				),+
			}
		}
		impl $crate::prelude::BitRegister for $name {
			type AddressRepr = $address_repr;
			const ADDRESS: Self::AddressRepr = $address;

			fn reset_value() -> Self {
				let r = Self::default();
				$( let r = $reset_value; )?

				r
			}
		}
	};

	(
		$( #[$attr: meta] )*
		$( @[reset = $reset_value: expr] )?
		$visibility: vis bitregister($repr: ident @ $address: literal: $address_repr: ty) $name: ident {
			$(
				$( #[$field_attr: meta] )*
				@[shl = $field_shift: literal]
				pub $field_name: ident:
				$( #[$field_type_attr: meta] )*
				$( @[mask = $field_type_mask: literal] )?
				bitvalue($field_type_repr: ident) $field_type_name: ident {
					$( $field_type_tok: tt )+
				}
			),+ $(,)?
		}
	) => {
		$(
			$crate::bitvalue! {
				$( #[$field_type_attr] )*
				$( @[mask = $field_type_mask] )?
				$visibility bitvalue($field_type_repr) $field_type_name {
					$( $field_type_tok )+
				}
			}
		)+

		$crate::bitregister! {
			$( #[$attr] )*
			$( @[reset = $reset_value] )?
			$visibility bitregister($repr @ $address: $address_repr) $name {
				$(
					$( #[$field_attr] )*
					@[shl = $field_shift]
					pub $field_name: $field_type_name
				),+
			}
		}
	};
}
