pub use crate::{
	mem::MaybeUninitSliceExt,
	packed::{BitPackedRepr, BitPackedValue},
	register::BitRegister,
	repr::{
		endian::{EndianSetting, EndianSwap},
		small_unsigned::{u24, u40},
		unit::UnitRepr,
		ReprType
	},
	value::{BitValue, BitvalueField}
};
