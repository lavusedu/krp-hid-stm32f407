use crate::prelude::{BitValue, EndianSetting};

pub mod padding;
pub use padding::BitPackedData;

pub unsafe trait BitPackedRepr: bytemuck::Pod {
	fn as_bytes(&self) -> &[u8];

	/// Attempts to convert bytes to `Self`.
	///
	/// If `bytes.len() < size_of::<Self>()`, zeroes the remaining bytes.
	fn try_from_bytes(bytes: &[u8]) -> Option<Self>;
}
unsafe impl<T: bytemuck::Pod> BitPackedRepr for T {
	fn as_bytes(&self) -> &[u8] {
		bytemuck::bytes_of(self)
	}

	fn try_from_bytes(bytes: &[u8]) -> Option<Self> {
		if bytes.len() < core::mem::size_of::<Self>() {
			// create a zeroed value
			let mut zeroed = Self::zeroed();

			// copy the bytes into the zeroed value
			bytemuck::bytes_of_mut(&mut zeroed)[.. bytes.len()].copy_from_slice(bytes);

			// return the value, the padding bytes are zeroed
			Some(zeroed)
		} else {
			bytemuck::try_from_bytes(bytes).ok().map(|v| *v)
		}
	}
}

pub trait BitPackedValue: Sized {
	type PackedRepr: BitPackedRepr;

	fn pack<E: EndianSetting>(&self) -> BitPackedData<Self::PackedRepr>;

	fn unpack<E: EndianSetting>(packed: &Self::PackedRepr) -> Option<Self>;
}
impl<T: BitValue> BitPackedValue for T {
	type PackedRepr = T::BitRepr;

	fn pack<E: EndianSetting>(&self) -> BitPackedData<Self::PackedRepr> {
		BitPackedData {
			padding_len: 0,
			data: E::from_ne(self.to_bits())
		}
	}

	fn unpack<E: EndianSetting>(packed: &Self::PackedRepr) -> Option<Self> {
		Self::from_bits(E::to_ne(*packed))
	}
}
impl<T: BitPackedValue, const N: usize> BitPackedValue for [T; N] {
	type PackedRepr = [T::PackedRepr; N];

	fn pack<E: EndianSetting>(&self) -> BitPackedData<Self::PackedRepr> {
		let mut iter = self.iter().map(|i| i.pack::<E>().data);
		match crate::mem::collect_into_array(&mut iter) {
			Some(data) => BitPackedData {
				padding_len: 0,
				data
			},
			// SAFETY: Self is len N, Self::PackedRepr is len N
			None => unsafe { core::hint::unreachable_unchecked() }
		}
	}

	fn unpack<E: EndianSetting>(packed: &Self::PackedRepr) -> Option<Self> {
		let mut iter = packed.iter().filter_map(T::unpack::<E>);
		// We use filter_map so if any `unpack` returns `None` it will shorten the iterator and won't fit into
		// the collected array, thus returning `None` here
		crate::mem::collect_into_array(&mut iter)
	}
}

#[doc(hidden)]
pub trait PackedDataHelperTypeLeak {
	type PackedData;
}

#[macro_export]
macro_rules! bitpacked {
	(
		$( #[$attr: meta] )*
		$visibility: vis struct $name: ident {
			$(
				pub $field_name: ident: $field_type: ty
			),+ $(,)?
		} -> $packed_name: ident
	) => {
		$( #[$attr] )*
		$visibility struct $name {
			$(
				pub $field_name: $field_type
			),+
		}
		impl $crate::prelude::BitPackedValue for $name {
			type PackedRepr = $packed_name;

			#[allow(unused_assignments)]
			fn pack<E: $crate::prelude::EndianSetting>(&self) -> $crate::packed::BitPackedData<Self::PackedRepr> {
				let mut last_field_padding = 0;
				let data = $packed_name {
					$(
						$field_name: {
							let packed = self.$field_name.pack::<E>();
							last_field_padding = packed.padding_len;

							packed.data
						}
					),+
				};

				$crate::packed::BitPackedData {
					padding_len: last_field_padding,
					data
				}
			}

			fn unpack<E: $crate::prelude::EndianSetting>(packed: &Self::PackedRepr) -> Option<Self> {
				use $crate::prelude::BitPackedValue;

				let unpacked = $name {
					$(
						$field_name: {
							// copy value to avoid creating an unaligned reference
							let value_copy = packed.$field_name;

							<$field_type as BitPackedValue>::unpack::<E>(
								&value_copy
							)?
						}
					),+
				};

				Some(unpacked)
			}
		}

		#[repr(C, packed)]
		#[derive(Clone, Copy)]
		$visibility struct $packed_name {
			$(
				$field_name: <$field_type as $crate::prelude::BitPackedValue>::PackedRepr
			),+
		}
		unsafe impl $crate::bytemuck::Zeroable for $packed_name {}
		unsafe impl $crate::bytemuck::Pod for $packed_name {}
	};

	(
		$( #[$attr: meta] )*
		$visibility: vis enum($tag_name: ident: $tag_type: ident) $name: ident {
			$(
				$variant_name: ident ($variant_data_type: ty) $(= $tag_value: expr)?
			),+ $(,)?
		} -> $packed_name: ident
	) => {
		$( #[$attr] )*
		$visibility enum $name {
			$(
				$variant_name($variant_data_type)
			),+
		}
		impl $name {
			pub const fn tag(&self) -> $tag_name {
				match self {
					$(
						$name::$variant_name(_) => $tag_name::$variant_name
					),+
				}
			}
		}
		impl $crate::prelude::BitPackedValue for $name {
			type PackedRepr = $packed_name;

			fn pack<E: $crate::prelude::EndianSetting>(&self) -> $crate::packed::BitPackedData<Self::PackedRepr> {
				// This exists only to calculate the maximum size for us.
				// This is used to pad all other union variants so that there is no uninitialized memory there
				#[repr(C)]
				#[allow(non_snake_case)]
				union PackedDataForSize {
					$(
						$variant_name: core::mem::ManuallyDrop<
							<$variant_data_type as $crate::prelude::BitPackedValue>::PackedRepr
						>
					),+
				}

				$(
					// we pad this so that it can be bytemuck::Pod
					type $variant_name = $crate::packed::padding::PaddedPackedData<
						<$variant_data_type as $crate::prelude::BitPackedValue>::PackedRepr,
						{
							core::mem::size_of::<PackedDataForSize>()
							- core::mem::size_of::<<$variant_data_type as $crate::prelude::BitPackedValue>::PackedRepr>()
						}
					>;
				)+

				#[repr(C)]
				#[derive(Clone, Copy)]
				#[allow(non_snake_case)]
				pub union PackedData {
					$(
						// Wrapped in manually drop to avoid a bug?
						// https://github.com/rust-lang/rust/issues/90903
						$variant_name: core::mem::ManuallyDrop<$variant_name>
					),+
				}

				$(
					// sanity check that each union variant is the same size as the entire union
					// this ensures that implementing Pod on it is not UB
					let _ = $crate::packed::padding::AssertConstIsSame::<
						{core::mem::size_of::<PackedData>()},
						{core::mem::size_of::<$variant_name>()}
					>::assert();
				)+

				impl $crate::packed::PackedDataHelperTypeLeak for $name {
					type PackedData = PackedData;
				}

				let (packed_data, padding_len) = match self {
					$(
						$name::$variant_name(data) => {
							let packed = data.pack::<E>();
							let padded = $crate::packed::padding::PaddedPackedData::new(
								packed.data
							);

							(
								PackedData {
									$variant_name: core::mem::ManuallyDrop::new(
										padded
									)
								},
								packed.padding_len + padded.padding_len()
							)
						}
					),+
				};

				let data = $packed_name {
					tag: (self.tag() as $tag_type).pack::<E>().data,
					data: packed_data
				};

				$crate::packed::BitPackedData {
					padding_len,
					data
				}
			}

			fn unpack<E: $crate::prelude::EndianSetting>(packed: &Self::PackedRepr) -> Option<Self> {
				let tag = $tag_name::try_from_repr(packed.tag)?;

				let variant = match tag {
					$(
						$tag_name::$variant_name => Self::$variant_name(
							<$variant_data_type as $crate::prelude::BitPackedValue>::unpack::<E>(
								&core::mem::ManuallyDrop::into_inner(
									unsafe { packed.data.$variant_name }
								).data()
							)?
						)
					),+
				};

				Some(variant)
			}
		}

		#[repr($tag_type)]
		#[derive(Debug, Clone, Copy)]
		$visibility enum $tag_name {
			$(
				$variant_name $(= $tag_value)?
			),+
		}
		impl $tag_name {
			pub const fn try_from_repr(tag: $tag_type) -> Option<Self> {
				match tag {
					$(
						tag if tag == Self::$variant_name as _ => Some(Self::$variant_name),
					)+
					_ => None
				}
			}
		}

		#[repr(C, packed)]
		#[derive(Clone, Copy)]
		$visibility struct $packed_name {
			tag: $tag_type,
			data: <$name as $crate::packed::PackedDataHelperTypeLeak>::PackedData
		}
		unsafe impl $crate::bytemuck::Zeroable for $packed_name {}
		unsafe impl $crate::bytemuck::Pod for $packed_name {}
	};
}
