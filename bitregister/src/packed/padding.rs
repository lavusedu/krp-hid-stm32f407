use super::BitPackedRepr;

pub struct BitPackedData<D: BitPackedRepr> {
	/// Number of padding bytes at the end of `data`.
	pub padding_len: usize,
	pub data: D
}
impl<D: BitPackedRepr> BitPackedData<D> {
	pub fn as_bytes_without_padding(&self) -> &[u8] {
		let bytes = self.data.as_bytes();

		debug_assert!(self.padding_len <= bytes.len());
		&bytes[.. bytes.len() - self.padding_len]
	}
}

#[repr(C, packed)]
pub struct PaddedPackedData<D: BitPackedRepr, const PADDING_LEN: usize> {
	data: D,
	padding: [u8; PADDING_LEN]
}
impl<D: BitPackedRepr, const PADDING_LEN: usize> PaddedPackedData<D, PADDING_LEN> {
	pub fn new(data: D) -> Self {
		PaddedPackedData {
			data,
			padding: [0u8; PADDING_LEN]
		}
	}

	pub fn padding_len(&self) -> usize {
		PADDING_LEN
	}

	pub fn data(self) -> D {
		self.data
	}
}
impl<D: BitPackedRepr, const PADDING_LEN: usize> core::clone::Clone
	for PaddedPackedData<D, PADDING_LEN>
{
	fn clone(&self) -> Self {
		Self {
			data: self.data,
			padding: self.padding
		}
	}
}
impl<D: BitPackedRepr, const PADDING_LEN: usize> Copy for PaddedPackedData<D, PADDING_LEN> {}

#[doc(hidden)]
pub struct AssertConstIsSame<const A: usize, const B: usize> {}
impl<const N: usize> AssertConstIsSame<N, N> {
	pub const fn assert() {}
}
