#![no_std]

pub mod mem;
pub mod packed;
pub mod register;
pub mod repr;
pub mod value;

pub mod prelude;

#[doc(hidden)]
pub use bytemuck;

#[cfg(test)]
mod test {
	use crate::{
		bitpacked,
		bitregister,
		bitvalue,
		prelude::{BitPackedRepr, BitPackedValue, BitValue, UnitRepr},
		repr::endian::LittleEndian
	};

	bitvalue! {
		#[derive(PartialEq, Eq)]
		pub bitvalue(u16) Foo {
			A = 0b110,
			B
		}
	}
	bitvalue! {
		#[derive(PartialEq, Eq)]
		pub bitvalue(u8) Bar {
			X = 0b11,
			Y = 0b10
		}
	}
	bitvalue! {
		#[derive(PartialEq, Eq)]
		pub bitvalue(u32) Baz {
			15 ..= 25465
		}
	}
	bitregister! {
		#[derive(PartialEq, Eq)]
		pub bitregister(u16 @ 0x22: u8) MyReg {
			@[shl = 3]
			pub foo: Foo,
			@[shl = 0]
			pub bar: Bar
		}
	}

	#[test]
	fn test_has_default() {
		assert_eq!(
			MyReg::default(),
			MyReg {
				foo: Foo::A,
				bar: Bar::X
			}
		);
	}

	#[test]
	fn test_has_correct_mask() {
		assert_eq!(Foo::MASK, 0b111);

		assert_eq!(Bar::MASK, 0b11);
	}

	#[test]
	fn test_to_bits_correct() {
		assert_eq!(MyReg::default().to_bits(), 0b110011);
	}

	#[test]
	fn test_from_bits_correct() {
		assert_eq!(
			MyReg::from_bits(0b111011),
			Some(MyReg {
				foo: Foo::B,
				bar: Bar::X
			})
		);
	}

	#[test]
	fn test_from_bits_fails() {
		assert_eq!(MyReg::from_bits(0b0), None);
	}

	#[test]
	fn test_bitvalue_range_default() {
		assert_eq!(Baz::from_bits(15), Some(Baz::default()));
	}

	#[test]
	fn test_bitvalue_range_from_bits_fails() {
		assert_eq!(Baz::from_bits(0), None);
	}

	#[test]
	fn test_bitvalue_range_correct_mask() {
		assert_eq!(Baz::MASK, (1 << 15) - 1);
	}

	#[test]
	fn test_bitpacked_struct() {
		bitpacked! {
			#[derive(Debug, PartialEq, Eq)]
			pub struct FooData {
				pub a: u16,
				pub b: u8,
				pub c: u32
			} -> FooDataPacked
		}

		let data = FooData {
			a: 0x00FF,
			b: 0xEE,
			c: 0xA1B2C3D4
		};

		let data_packed = data.pack::<LittleEndian>();
		let bytes = data_packed.as_bytes_without_padding();
		assert_eq!(bytes, &[0xFF, 0x00, 0xEE, 0xD4, 0xC3, 0xB2, 0xA1]);

		assert_eq!(
			FooData::unpack::<LittleEndian>(&FooDataPacked::try_from_bytes(bytes).unwrap())
				.unwrap(),
			data
		);
	}

	#[test]
	fn test_bitpacked_enum() {
		bitpacked! {
			#[derive(Debug, PartialEq, Eq)]
			#[allow(dead_code)]
			pub enum(FooTag: u16) Foo {
				A(u16),
				B(u32),
				C(UnitRepr)
			} -> FooPacked
		}

		bitpacked! {
			#[derive(Debug, PartialEq, Eq)]
			pub enum(BarTag: u8) Bar {
				X(Foo) = 0xAB,
				Y(u8)
			} -> BarPacked
		}

		{
			let data = Bar::X(Foo::B(0x12345678));
			let data_packed = data.pack::<LittleEndian>();
			let bytes = data_packed.as_bytes_without_padding();

			assert_eq!(
				bytes,
				&[
					0xAB, // Bar tag
					0x01, 0x00, // Foo tag
					0x78, 0x56, 0x34, 0x12 // Foo data
				]
			);

			assert_eq!(
				Bar::unpack::<LittleEndian>(&BarPacked::try_from_bytes(bytes).unwrap()).unwrap(),
				data
			);
		}

		{
			let data = Bar::X(Foo::C(UnitRepr));
			let data_packed = data.pack::<LittleEndian>();
			let bytes = data_packed.as_bytes_without_padding();
			assert_eq!(
				bytes,
				&[
					0xAB, // Bar tag
					0x02, 0x00 // Foo tag
				]
			);

			assert_eq!(
				Bar::unpack::<LittleEndian>(&BarPacked::try_from_bytes(bytes).unwrap()).unwrap(),
				data
			);
		}

		{
			let data = Bar::Y(0xFF);
			let data_packed = data.pack::<LittleEndian>();
			let bytes = data_packed.as_bytes_without_padding();
			assert_eq!(
				bytes,
				&[
					0xAC, // Bar tag
					0xFF  // Bar data
				]
			);

			assert_eq!(
				Bar::unpack::<LittleEndian>(&BarPacked::try_from_bytes(bytes).unwrap()).unwrap(),
				data
			);
		}
	}
}
