use crate::repr::ReprType;

pub trait EndianSwap: Sized {
	fn swap_bytes(self) -> Self;

	/// Converts bytes of `value` from little-endian to native-endian.
	fn from_le(value: Self) -> Self {
		#[cfg(target_endian = "big")]
		{
			value.swap_bytes()
		}

		#[cfg(target_endian = "little")]
		{
			value
		}
	}
	/// Converts bytes of `value` from bin-endian to native-endian.
	fn from_be(value: Self) -> Self {
		#[cfg(target_endian = "big")]
		{
			value
		}

		#[cfg(target_endian = "little")]
		{
			value.swap_bytes()
		}
	}

	/// Converts bytes of `self` from native-endian to little-endian.
	fn to_le(self) -> Self {
		#[cfg(target_endian = "big")]
		{
			self.swap_bytes()
		}

		#[cfg(target_endian = "little")]
		{
			self
		}
	}
	/// Converts bytes of `self` from native-endian to big-endian.
	fn to_be(self) -> Self {
		#[cfg(target_endian = "big")]
		{
			self
		}

		#[cfg(target_endian = "little")]
		{
			self.swap_bytes()
		}
	}
}

pub trait EndianSetting {
	/// Converts `value` to native endian, assuming it is in endian described by `Self`.
	fn to_ne<R: ReprType>(value: R) -> R;

	/// Converts `value` to endian described by `Self`, assuming it is in native endian.
	fn from_ne<R: ReprType>(value: R) -> R;
}

#[derive(Debug)]
/// The input is in little endian.
///
/// Swaps when the host is in big endian.
pub struct LittleEndian;
impl EndianSetting for LittleEndian {
	fn to_ne<R: ReprType>(value: R) -> R {
		R::from_le(value)
	}

	fn from_ne<R: ReprType>(value: R) -> R {
		value.to_le()
	}
}

#[derive(Debug)]
/// The input is in big endian.
///
/// Swaps when the host is in little endian.
pub struct BigEndian;
impl EndianSetting for BigEndian {
	fn to_ne<R: ReprType>(value: R) -> R {
		R::from_be(value)
	}

	fn from_ne<R: ReprType>(value: R) -> R {
		value.to_be()
	}
}

#[derive(Debug)]
/// The input is in native endian.
///
/// Never swaps.
pub struct NativeEndian;
impl EndianSetting for NativeEndian {
	fn to_ne<R: ReprType>(value: R) -> R {
		value
	}

	fn from_ne<R: ReprType>(value: R) -> R {
		value
	}
}

#[derive(Debug)]
/// The input is in swapped endian (relative to the host).
///
/// Always swaps.
pub struct SwappedEndian;
impl EndianSetting for SwappedEndian {
	fn to_ne<R: ReprType>(value: R) -> R {
		value.swap_bytes()
	}

	fn from_ne<R: ReprType>(value: R) -> R {
		value.swap_bytes()
	}
}
