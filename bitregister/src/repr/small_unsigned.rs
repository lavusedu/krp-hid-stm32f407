use core::{
	num::TryFromIntError,
	ops::{BitAnd, BitOr, Shl, Shr}
};

macro_rules! small_unsigned {
	(
		$name: ident [$len: literal] {
			native {
				repr = $native_repr: ty;
				to_native_name = $to_native_name: ident;
				from_native_name = $from_native_name: ident;
				byte_indices = $( $byte_index: literal ),+;
				byte_indices_reverse = $( $byte_index_reverse: literal ),+;
			}

			impl From<$( $from_type: ty ),+>;
			impl From<Self> for $( $to_type: ty ),+;
		}
	) => {
		#[allow(non_camel_case_types)]
		#[repr(transparent)]
		#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
		pub struct $name(pub [u8; $len]);
		impl $name {
			pub const MAX: Self = Self([u8::MAX; $len]);

			pub const fn $to_native_name(self) -> $native_repr {
				let mut native_bytes = [0u8; core::mem::size_of::<$native_repr>()];

				$( native_bytes[$byte_index] = self.0[$byte_index]; )+

				<$native_repr>::from_ne_bytes(
					native_bytes
				)
			}

			pub const fn $from_native_name(value: $native_repr) -> Self {
				let bytes = <$native_repr>::to_ne_bytes(value);

				Self(
					[ $( bytes[$byte_index] ),+ ]
				)
			}

			pub const fn swap_bytes(self) -> Self {
				Self(
					[ $( self.0[$byte_index_reverse] ),+ ]
				)
			}
		}
		impl Shl<usize> for $name {
			type Output = Self;

			fn shl(self, rhs: usize) -> Self::Output {
				Self::$from_native_name(self.$to_native_name() << rhs)
			}
		}
		impl Shr<usize> for $name {
			type Output = Self;

			fn shr(self, rhs: usize) -> Self::Output {
				Self::$from_native_name(self.$to_native_name() >> rhs)
			}
		}
		impl BitAnd for $name {
			type Output = Self;

			fn bitand(self, rhs: Self) -> Self::Output {
				Self::$from_native_name(self.$to_native_name() & rhs.$to_native_name())
			}
		}
		impl BitOr for $name {
			type Output = Self;

			fn bitor(self, rhs: Self) -> Self::Output {
				Self::$from_native_name(self.$to_native_name() | rhs.$to_native_name())
			}
		}
		impl core::fmt::Debug for $name {
			fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
				write!(f, "{}", self.$to_native_name())
			}
		}
		unsafe impl bytemuck::Zeroable for $name {}
		unsafe impl bytemuck::Pod for $name {}

		$(
			impl From<$from_type> for $name {
				fn from(value: $from_type) -> Self {
					Self::$from_native_name(
						<$native_repr>::from(value)
					)
				}
			}
			impl TryFrom<$name> for $from_type {
				type Error = TryFromIntError;

				fn try_from(value: $name) -> Result<Self, Self::Error> {
					TryFrom::<$native_repr>::try_from(value.$to_native_name())
				}
			}
		)+
		$(
			impl From<$name> for $to_type {
				fn from(value: $name) -> Self {
					<$to_type>::from(
						value.$to_native_name()
					)
				}
			}
			impl TryFrom<$to_type> for $name {
				type Error = TryFromIntError;

				fn try_from(value: $to_type) -> Result<Self, Self::Error> {
					let value: $native_repr = TryFrom::try_from(value)?;

					if value > Self::MAX.$to_native_name() {
						// artificially manufacture the error because hax
						Err(
							<u8 as TryFrom<u16>>::try_from(0xFFFFu16).unwrap_err()
						)
					} else {
						Ok(
							Self::$from_native_name(value)
						)
					}
				}
			}
		)+
	};
}

small_unsigned! {
	u24 [3] {
		native {
			repr = u32;
			to_native_name = to_u32;
			from_native_name = from_u32;
			byte_indices = 0, 1, 2;
			byte_indices_reverse = 2, 1, 0;
		}

		impl From<u8, u16>;
		impl From<Self> for u32, u64, u128;
	}
}
small_unsigned! {
	u40 [5] {
		native {
			repr = u64;
			to_native_name = to_u64;
			from_native_name = from_u64;
			byte_indices = 0, 1, 2, 3, 4;
			byte_indices_reverse = 4, 3, 2, 1, 0;
		}

		impl From<u8, u16, u24, u32>;
		impl From<Self> for u64, u128;
	}
}
