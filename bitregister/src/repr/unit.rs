use core::ops::{BitAnd, BitOr, Shl, Shr};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct UnitRepr;
impl UnitRepr {
	pub const MAX: Self = UnitRepr;

	pub const fn swap_bytes(self) -> Self {
		self
	}
}
impl Shl<usize> for UnitRepr {
	type Output = Self;

	fn shl(self, _rhs: usize) -> Self::Output {
		self
	}
}
impl Shr<usize> for UnitRepr {
	type Output = Self;

	fn shr(self, _rhs: usize) -> Self::Output {
		self
	}
}
impl BitAnd for UnitRepr {
	type Output = Self;

	fn bitand(self, _rhs: Self) -> Self::Output {
		self
	}
}
impl BitOr for UnitRepr {
	type Output = Self;

	fn bitor(self, _rhs: Self) -> Self::Output {
		self
	}
}
unsafe impl bytemuck::Zeroable for UnitRepr {}
unsafe impl bytemuck::Pod for UnitRepr {}

macro_rules! impl_from {
	(
		$( $target_type: ty ),+
	) => {
		$(
			impl From<UnitRepr> for $target_type {
				fn from(_: UnitRepr) -> $target_type {
					0
				}
			}
			impl TryFrom<$target_type> for UnitRepr {
				type Error = core::num::TryFromIntError;

				fn try_from(value: $target_type) -> Result<Self, Self::Error> {
					if value > 0 {
						// artificially manufacture the error because hax
						Err(
							<u8 as TryFrom<u16>>::try_from(0xFFFFu16).unwrap_err()
						)
					} else {
						Ok(UnitRepr)
					}
				}
			}
		)+
	};
}
impl_from! {
	u8, u16, u32, u64, u128
}
