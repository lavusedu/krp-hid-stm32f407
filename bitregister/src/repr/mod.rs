use core::{
	mem::MaybeUninit,
	num::{
		NonZeroI128,
		NonZeroI16,
		NonZeroI32,
		NonZeroI64,
		NonZeroI8,
		NonZeroU128,
		NonZeroU16,
		NonZeroU32,
		NonZeroU64,
		NonZeroU8
	},
	ops::{BitAnd, BitOr, Shl, Shr}
};

use bytemuck;

use crate::{mem::MaybeUninitPodSliceExt, value::BitValue};

pub mod endian;
pub mod small_unsigned;
pub mod unit;

use endian::EndianSwap;
use small_unsigned::{u24, u40};
use unit::UnitRepr;

/// Marker trait for bit representation types.
///
/// These types
pub trait ReprType:
	core::fmt::Debug
	+ Sized
	+ Shl<usize, Output = Self>
	+ Shr<usize, Output = Self>
	+ BitAnd<Output = Self>
	+ BitOr<Output = Self>
	+ bytemuck::Pod
	+ EndianSwap
{
	const MAX: Self;
	const DEFAULT: Self;
}

/// Represents that `&[Self]` can be cast from `&[R]` without any trucation and that `[Self; N]` can be cast from `R` for a specific integral `N`.
///
/// This also means that `size_of::<Self> <= size_of::<R>`.
pub unsafe trait ReprCastSliceFrom<R: ReprType>: ReprType {
	const SIZE_FACTOR: usize = core::mem::size_of::<R>() / core::mem::size_of::<Self>();

	/// Returns the len of `[R]` from the len of `[Self]`.
	///
	/// Returns `None` if there would be a trucation - e.g. `[0u8; 3] -> [u16; 1] + trucated 1 byte`.
	fn try_reverse_len(self_len: usize) -> Option<usize> {
		let rem = self_len % Self::SIZE_FACTOR;
		if rem != 0 {
			return None
		}

		Some(self_len / Self::SIZE_FACTOR)
	}

	// fn cast_from(r: R) -> [Self; Self::SIZE_FACTOR] {
	// }

	fn cast_slice_from(slice: &[R]) -> &[Self] {
		bytemuck::cast_slice(slice)
	}

	fn cast_slice_mut_from(slice: &mut [R]) -> &mut [Self] {
		bytemuck::cast_slice_mut(slice)
	}

	fn cast_uninit_slice_from(slice: &[MaybeUninit<R>]) -> &[MaybeUninit<Self>] {
		MaybeUninit::cast_slice(slice)
	}

	fn cast_uninit_slice_mut_from(slice: &mut [MaybeUninit<R>]) -> &mut [MaybeUninit<Self>] {
		MaybeUninit::cast_slice_mut(slice)
	}
}

macro_rules! impl_help {
	(
		$(
			$repr_type: ty {
				$( default = $repr_default: expr, )?
				cast_from = $( $castable_from: ty ),+
			}
		),+
	) => {
		$(
			impl EndianSwap for $repr_type {
				fn swap_bytes(self) -> Self {
					self.swap_bytes()
				}
			}
			impl ReprType for $repr_type {
				const MAX: Self = <$repr_type>::MAX;
				const DEFAULT: Self = { 0 $(; $repr_default)? };
			}
			impl_help! {
				__trivial_cast
				$repr_type => $repr_type
			}
			$(
				unsafe impl ReprCastSliceFrom<$repr_type> for $castable_from {}
			)+
		)+
	};

	(
		__trivial_cast
		$(
			$source_type: ty => $target_type: ty
		),+
	) => {
		$(
			impl BitValue for $source_type {
				type BitRepr = $target_type;

				const MASK: Self::BitRepr = Self::BitRepr::MAX;

				fn to_bits(&self) -> Self::BitRepr {
					*self as $target_type
				}

				fn from_bits(bits: Self::BitRepr) -> Option<Self> {
					Some(bits as $source_type)
				}
			}
		)+
	};

	(
		__nonzero
		$(
			$nonzero_type: ty => $target_type: ty
		),+
	) => {
		$(
			impl BitValue for $nonzero_type {
				type BitRepr = $target_type;

				const MASK: Self::BitRepr = Self::BitRepr::MAX;

				fn to_bits(&self) -> Self::BitRepr {
					self.get() as $target_type
				}

				fn from_bits(bits: Self::BitRepr) -> Option<Self> {
					<$nonzero_type>::new(bits as _)
				}
			}
		)+
	};
}
impl_help! {
	UnitRepr { default = UnitRepr, cast_from = UnitRepr },
	u8 { cast_from = u8 },
	u16 { cast_from = u8, u16 },
	u24 { default = u24::from_u32(0), cast_from = u8, u24 },
	u32 { cast_from = u8, u16, u32 },
	u40 { default = u40::from_u64(0), cast_from = u8, u40 },
	u64 { cast_from = u8, u16, u32, u64 },
	u128 { cast_from = u8, u16, u32, u64, u128 }
}
impl_help! {
	__trivial_cast
	i8 => u8,
	i16 => u16,
	i32 => u32,
	i64 => u64,
	i128 => u128
}
impl_help! {
	__nonzero
	NonZeroU8 => u8,
	NonZeroI8 => u8,
	NonZeroU16 => u16,
	NonZeroI16 => u16,
	NonZeroU32 => u32,
	NonZeroI32 => u32,
	NonZeroU64 => u64,
	NonZeroI64 => u64,
	NonZeroU128 => u128,
	NonZeroI128 => u128
}
