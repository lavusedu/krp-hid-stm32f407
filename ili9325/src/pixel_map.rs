use embedded_graphics_core::prelude::{Point, Size};

use bitregister::prelude::BitValue;

use crate::commands::{
	GRamAccessDirection,
	GRamHorizontalAddress,
	GRamIncrementDirection,
	GRamVerticalAddress
};

pub fn try_point_to_address(
	point: Point
) -> (Option<GRamHorizontalAddress>, Option<GRamVerticalAddress>) {
	(
		u16::try_from(point.x)
			.ok()
			.and_then(GRamHorizontalAddress::from_bits),
		u16::try_from(point.y)
			.ok()
			.and_then(GRamVerticalAddress::from_bits)
	)
}

/// Abstracts over pixel mapping between drawing commands and data written to the display GRam.
pub trait PixelMapping {
	/// Size of the display in pixel space.
	const DISPLAY_SIZE: Size;

	/// An integer 2D transform matrix - can do axis flips and 90 degree rotations.
	const TRANSFORM_MATRIX: [i32; 4];
	/// Coordinates of the transformed origin in ram address space.
	const TRANSFORM_ORIGIN: Point;

	/// Access direction to the ram - defines which quadrant we map to when autoincrementing.
	const RAM_ACCESS: GRamAccessDirection;
	/// Defines which axis is the x (the one incremented directly) when autoincrementing.
	const RAM_INCREMENT: GRamIncrementDirection;

	/// Transforms point from pixel space to ram address space.
	fn transform_point(point: Point) -> Point {
		// 1. subtract pixel space origin - this is a noop because by default we map pixel space to address space using identity function
		// 2. transform the point according to transformation matrix - this can flip axes or rotate it by multiples of 90 degrees (or project it)
		// 3. add the new origin coordinates to get the correct ram address space
		Point {
			x: point.x * Self::TRANSFORM_MATRIX[0]
				+ point.y * Self::TRANSFORM_MATRIX[1]
				+ Self::TRANSFORM_ORIGIN.x,
			y: point.x * Self::TRANSFORM_MATRIX[2]
				+ point.y * Self::TRANSFORM_MATRIX[3]
				+ Self::TRANSFORM_ORIGIN.y
		}
	}
}

pub struct PixelMappingIdentity;
impl PixelMapping for PixelMappingIdentity {
	const DISPLAY_SIZE: Size = Size {
		width: 240,
		height: 320
	};
	const RAM_ACCESS: GRamAccessDirection = GRamAccessDirection::HIncVInc;
	const RAM_INCREMENT: GRamIncrementDirection = GRamIncrementDirection::Horizontal;
	const TRANSFORM_MATRIX: [i32; 4] = [1, 0, 0, 1];
	const TRANSFORM_ORIGIN: Point = Point { x: 0, y: 0 };
}

pub struct PixelMappingRotate90;
impl PixelMapping for PixelMappingRotate90 {
	const DISPLAY_SIZE: Size = Size {
		width: 320,
		height: 240
	};
	const RAM_ACCESS: GRamAccessDirection = GRamAccessDirection::HDecVInc;
	const RAM_INCREMENT: GRamIncrementDirection = GRamIncrementDirection::Vertical;
	const TRANSFORM_MATRIX: [i32; 4] = [0, -1, 1, 0];
	const TRANSFORM_ORIGIN: Point = Point { x: 239, y: 0 };
}

pub struct PixelMappingRotate180;
impl PixelMapping for PixelMappingRotate180 {
	const DISPLAY_SIZE: Size = Size {
		width: 240,
		height: 320
	};
	const RAM_ACCESS: GRamAccessDirection = GRamAccessDirection::HDecVDec;
	const RAM_INCREMENT: GRamIncrementDirection = GRamIncrementDirection::Horizontal;
	const TRANSFORM_MATRIX: [i32; 4] = [-1, 0, 0, -1];
	const TRANSFORM_ORIGIN: Point = Point { x: 239, y: 319 };
}

pub struct PixelMappingRotate270;
impl PixelMapping for PixelMappingRotate270 {
	const DISPLAY_SIZE: Size = Size {
		width: 320,
		height: 240
	};
	const RAM_ACCESS: GRamAccessDirection = GRamAccessDirection::HIncVDec;
	const RAM_INCREMENT: GRamIncrementDirection = GRamIncrementDirection::Vertical;
	const TRANSFORM_MATRIX: [i32; 4] = [0, 1, -1, 0];
	const TRANSFORM_ORIGIN: Point = Point { x: 239, y: 0 };
}

pub struct PixelMappingMirrorX;
impl PixelMapping for PixelMappingMirrorX {
	const DISPLAY_SIZE: Size = Size {
		width: 240,
		height: 320
	};
	const RAM_ACCESS: GRamAccessDirection = GRamAccessDirection::HDecVInc;
	const RAM_INCREMENT: GRamIncrementDirection = GRamIncrementDirection::Horizontal;
	const TRANSFORM_MATRIX: [i32; 4] = [-1, 0, 0, 1];
	const TRANSFORM_ORIGIN: Point = Point { x: 239, y: 0 };
}

pub struct PixelMappingMirrorY;
impl PixelMapping for PixelMappingMirrorY {
	const DISPLAY_SIZE: Size = Size {
		width: 240,
		height: 320
	};
	const RAM_ACCESS: GRamAccessDirection = GRamAccessDirection::HIncVDec;
	const RAM_INCREMENT: GRamIncrementDirection = GRamIncrementDirection::Horizontal;
	const TRANSFORM_MATRIX: [i32; 4] = [1, 0, 0, -1];
	const TRANSFORM_ORIGIN: Point = Point { x: 0, y: 319 };
}

pub type PixelMappingMirrorXY = PixelMappingRotate180;
