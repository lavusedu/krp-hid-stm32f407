use bitregister::{bitregister, bitvalue, prelude::BitRegister};

pub trait ReadableRegister: BitRegister {}
pub trait WriteableRegister: BitRegister {}

macro_rules! impl_write_command {
	(
		$( #[$attr: meta] )*
		$visibility: vis bitregister(u16 @ $address: literal: u16) $name: ident {
			$(
				$( #[$field_attr: meta] )*
				@[shl = $field_shift: literal]
				$( @[mask = $field_mask: literal] )?
				pub $field_name: ident: bitvalue(u16) $field_type_name: ident {
					$( $field_toks: tt )+
				}
			),+ $(,)?
		}
	) => {
		bitregister! {
			$( #[$attr] )*
			$visibility bitregister(u16 @ $address: u16) $name {
				$(
					$( #[$field_attr] )*
					@[shl = $field_shift]
					pub $field_name:
					#[allow(non_camel_case_types)]
					$( @[mask = $field_mask] )?
					bitvalue(u16) $field_type_name {
						$( $field_toks )+
					}
				),+
			}
		}
		impl WriteableRegister for $name {}
	};
}

bitvalue! {
	pub bitvalue(u16) LcdId {
		Lcd9325 = 0x9325
	}
}
impl BitRegister for LcdId {
	type AddressRepr = u16;

	const ADDRESS: Self::AddressRepr = 0x00;

	fn reset_value() -> Self {
		Self::default()
	}
}
impl ReadableRegister for LcdId {}


// DISPLAY CONTROL //

impl_write_command! {
	pub bitregister(u16 @ 0x00: u16) StartOscillation {
		@[shl = 0]
		pub start: bitvalue(u16) StartOscillationSetting {
			Enabled = 0b1
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x01: u16) DriverOutputControl {
		@[shl = 10]
		pub sm: bitvalue(u16) DriverScanMode {
			Normal = 0b0,
			/// In this mode the first half of the display is controlled by odd-numbered gates (G1, G3, ... G319)
			/// and the second half is controlled by even-numbered gates (G2, G4, ... G320)
			OddEven = 0b1
		},
		@[shl = 8]
		pub ss: bitvalue(u16) DriverSourceScanDirection {
			Forward = 0b0,
			Reverse = 0b1
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x60: u16) DriverOutputControl2 {
		@[shl = 15]
		pub gs: bitvalue(u16) DriverScanDirection {
			Forward = 0b0,
			Reverse = 0b1
		},
		@[shl = 8]
		pub nl: bitvalue(u16) DriverScanLines {
			Lines240 = 0b011101,
			Lines248,
			Lines256,
			Lines264,
			Lines272,
			Lines280,
			Lines288,
			Lines296,
			Lines304,
			Lines312,
			Lines320
		},
		@[shl = 0]
		pub scn: bitvalue(u16) DriverScanStart {
			Offset0 = 0b000000,
			Offset8,
			Offset16,
			Offset24,
			Offset32,
			Offset40,
			Offset48,
			Offset56,
			Offset64,
			Offset72,
			Offset80,
			Offset88,
			Offset96,
			Offset104,
			Offset112,
			Offset120,
			Offset128,
			Offset136,
			Offset144,
			Offset152,
			Offset160,
			Offset168,
			Offset176,
			Offset184,
			Offset192,
			Offset200,
			Offset208,
			Offset216,
			Offset224,
			Offset232,
			Offset240,
			Offset248,
			Offset256,
			Offset264,
			Offset272,
			Offset280,
			Offset288,
			Offset296,
			Offset304,
			Offset312
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x02: u16) DrivingWaveControl {
		@[shl = 8]
		pub ss: bitvalue(u16) DrivingWaveSetting {
			FieldInversion = 0b100,
			LineInversion = 0b111
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x03: u16) EntryMode {
		@[shl = 14]
		pub dfm_tri: bitvalue(u16) GRamPixelTransferMode {
			/// One transfer - 565 pixels
			OneTransfer = 0b00,
			/// Two transfers - 666 pixels (18 bits), first transfer only transfers low 2 bits
			TwoTransfersLowBits = 0b10,
			/// Two transfers - 666 pixels (18 bits), second transfer only transfers high 2 bits
			TwoTransfersHighBits = 0b11
		},
		@[shl = 12]
		pub bgr: bitvalue(u16) GRamColorFormat {
			Rgb = 0b0,
			Bgr = 0b1
		},
		@[shl = 9]
		pub hwm: bitvalue(u16) GRamHighSpeedWrite {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 7]
		pub org: bitvalue(u16) GRamOriginAddress {
			NoChange = 0b0,
			MoveAddress = 0b1
		},
		@[shl = 4]
		pub id: bitvalue(u16) GRamAccessDirection {
			HDecVDec = 0b00,
			HIncVDec = 0b01,
			HDecVInc = 0b10,
			HIncVInc = 0b11
		},
		@[shl = 3]
		pub am: bitvalue(u16) GRamIncrementDirection {
			Horizontal = 0b0,
			Vertical = 0b1
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x04: u16) ResizingControl {
		@[shl = 8]
		pub rcv: bitvalue(u16) ResizingRemainderVerticalPixels {
			Rem0 = 0b00,
			Rem1,
			Rem2,
			Rem3
		},
		@[shl = 4]
		pub rch: bitvalue(u16) ResizingRemainderHorizontalPixels {
			Rem0 = 0b00,
			Rem1,
			Rem2,
			Rem3
		},
		@[shl = 0]
		pub rsz: bitvalue(u16) ResizingFactor {
			Full = 0b00,
			Half = 0b01,
			Quarter = 0b11
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x07: u16) DisplayControl1 {
		@[shl = 12]
		pub ptde: bitvalue(u16) PanelPartialImage {
			Disabled = 0b00,
			/// Needs `PartialImage` in d_basee
			Enabled = 0b01
		},
		@[shl = 4]
		pub gon_dte: bitvalue(u16) GateDriverOutputLevel {
			Vgh1 = 0b00,
			Vgh2 = 0b01,
			Vgl = 0b10,
			Normal = 0b11
		},
		@[shl = 3]
		pub cl: bitvalue(u16) ColorDisplayMode {
			Full = 0b0,
			EightBit = 0b1
		},
		@[shl = 0]
		@[mask = 0b1_0000_0011]
		pub d_basee: bitvalue(u16) DisplayPanelMode {
			Off = 0b0_0000_0000,
			InputLow = 0b1_0000_0001,
			NonLit = 0b0_0000_0010,
			PartialImage = 0b0_0000_0011,
			On = 0b1_0000_0011
		}
	}
}

impl_write_command! {
	/// Front porch lines + back porch lines must be less than or equal to 16 (fp + bp <= 16).
	///
	/// In case of VSYNC interface this must be _exactly_ 16 (fp + bp == 16).
	pub bitregister(u16 @ 0x08: u16) DisplayControl2 {
		@[shl = 8]
		pub fp: bitvalue(u16) FrontPorchLines {
			2 ..= 14
		},
		@[shl = 0]
		pub bp: bitvalue(u16) BackPorchLines {
			2 ..= 14
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x09: u16) DisplayControl3 {
		@[shl = 10]
		pub pts2: bitvalue(u16) SourceOutputLevelNonDisplayPower {
			/// Grayscale amplifier: 6.3V to 0V, step-up clock frequency: register setting (DC1, DC0)
			Full = 0b0,
			/// Grayscale amplifier: 6.3V and 0V, step-up clock frequency: frequency setting by DC1, DC0
			Reduced = 0b1
		},
		@[shl = 8]
		pub pts: bitvalue(u16) SourceOutputLevelNonDisplay {
			/// Positive polarity: 6.3V, Negative polarity: 0V
			P6_3__N0 = 0b00,
			/// Positive polarity: GND, Negative polarity: GND
			PGND__NGND = 0b10,
			/// Positive polarity: Hi-Z, Negative polarity: Hi-Z
			PHiZ__NHiZ = 0b11,
		},
		@[shl = 4]
		pub ptg: bitvalue(u16) ScanModeNonDisplay {
			Normal = 0b00,
			Interval = 0b10
		},
		@[shl = 0]
		pub isc: bitvalue(u16) ScanCycleInterval {
			Frames0 = 0b0000,
			Frames1,
			Frames3,
			Frames5,
			Frames7,
			Frames9,
			Frames11,
			Frames13,
			Frames15,
			Frames17,
			Frames19,
			Frames21,
			Frames23,
			Frames25,
			Frames27,
			Frames29
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x0A: u16) DisplayControl4 {
		@[shl = 0]
		pub fmark: bitvalue(u16) FMarkSetting {
			Disabled = 0b0000,
			EnabledFrames1 = 0b1000,
			EnabledFrames2 = 0b1001,
			EnabledFrames4 = 0b1011,
			EnabledFrames6 = 0b1101
		}
	}
}

impl_write_command! {
	pub bitregister(u16 @ 0x0C: u16) RgbDisplayInterfaceControl1 {
		@[shl = 12]
		pub enc: bitvalue(u16) RgbGRamWriteCycles {
			1 ..= 7
		},
		@[shl = 8]
		pub rm: bitvalue(u16) GramAccessInterface {
			SystemOrVsync = 0b0,
			Rgb = 0b1
		},
		@[shl = 4]
		pub dm: bitvalue(u16) DisplayOperationMode {
			InternalSystemClock = 0b00,
			RgbInterface = 0b01,
			VSyncInterface = 0b10
		},
		@[shl = 0]
		pub rim: bitvalue(u16) RgbInterfaceDataWdith {
			Bits18 = 0b00,
			Bits16 = 0b01,
			Bits6 = 0b10
		}
	}
}

impl_write_command! {
	pub bitregister(u16 @ 0x0D: u16) FrameMarkerPosition {
		@[shl = 0]
		pub fmp: bitvalue(u16) FrameMarkerPositionValue {
			0 ..= 0x177
		}
	}
}

impl_write_command! {
	pub bitregister(u16 @ 0x0F: u16) RgbDisplayInterfaceControl2 {
		@[shl = 4]
		pub vspl: bitvalue(u16) VsyncPinPolarity {
			LowActive = 0b0,
			HighActive = 0b1
		},
		@[shl = 3]
		pub hspl: bitvalue(u16) HsyncPinPolarity {
			LowActive = 0b0,
			HighActive = 0b1
		},
		@[shl = 1]
		pub epl: bitvalue(u16) EnablePinPolarity {
			LowActive = 0b0,
			HighActive = 0b1
		},
		@[shl = 0]
		pub dpl: bitvalue(u16) DotclkPinPolarity {
			RisingEdge = 0b0,
			FallingEdge = 0b1
		}
	}
}

impl_write_command! {
	pub bitregister(u16 @ 0x61: u16) BaseImageDisplayControl {
		@[shl = 2]
		pub ndl: bitvalue(u16) NonDisplayAreaLevel {
			P6_3__N0 = 0b0,
			P0__N6_3 = 0b1
		},
		@[shl = 1]
		pub vle: bitvalue(u16) VerticalScroll {
			Disabled = 0b0,
			Enabled = 0b1
		},
		/// Controls how GRam data maps to grayscale.
		///
		/// In low-active mode, low data (0) maps to P6.3V, N0V and going up to P0V, N6.3V at high data (1).
		/// In high-active mode, low data (0) maps to P0V, N6.3V and going op to P6.3V, N0V at high data (1).
		///
		/// This effectively controls whether bits in pixel values are low-active or high-active - i.e. 0b0 is white on `LowActive` and black on `HighActive`.
		@[shl = 0]
		pub rev: bitvalue(u16) GrayscalePolarityMode {
			LowActive = 0b0,
			HighActive = 0b1
		}
	}
}

// POWER CONTROL //

impl_write_command! {
	pub bitregister(u16 @ 0x10: u16) PowerControl1 {
		@[shl = 12]
		pub sap: bitvalue(u16) SourceDriver {
			Disabled = 0b0,
			Enabled = 0b1
		},
		@[shl = 8]
		pub bt: bitvalue(u16) StepUpFactors {
			/// DDVDH = Vci1 x 2.0, VCL = -Vci1, VGH = Vci1 x 6, VGL = -Vci1 x 5
			VGHx6_VGLx5 = 0b000,
			/// DDVDH = Vci1 x 2.0, VCL = -Vci1, VGH = Vci1 x 6, VGL = -Vci1 x 4
			VGHx6_VGLx4,
			/// DDVDH = Vci1 x 2.0, VCL = -Vci1, VGH = Vci1 x 6, VGL = -Vci1 x 3
			VGHx6_VGLx3,
			/// DDVDH = Vci1 x 2.0, VCL = -Vci1, VGH = Vci1 x 5, VGL = -Vci1 x 5
			VGHx5_VGLx5,
			/// DDVDH = Vci1 x 2.0, VCL = -Vci1, VGH = Vci1 x 5, VGL = -Vci1 x 4
			VGHx5_VGLx4,
			/// DDVDH = Vci1 x 2.0, VCL = -Vci1, VGH = Vci1 x 5, VGL = -Vci1 x 3
			VGHx5_VGLx3,
			/// DDVDH = Vci1 x 2.0, VCL = -Vci1, VGH = Vci1 x 4, VGL = -Vci1 x 4
			VGHx4_VGLx4,
			/// DDVDH = Vci1 x 2.0, VCL = -Vci1, VGH = Vci1 x 4, VGL = -Vci1 x 3
			VGHx4_VGLx3
		},
		@[shl = 7]
		pub ape: bitvalue(u16) PowerSupply {
			Disabled = 0b0,
			Enabled = 0b1
		},
		/// Gamma and Source amplify factors
		@[shl = 4]
		pub ap: bitvalue(u16) GammaSourceCurrentAmplify {
			Halt = 0b000,
			Gx1_00__Sx1_00 = 0b001,
			Gx1_00__Sx0_75,
			Gx1_00__Sx0_50,
			Gx0_75__Sx1_00,
			Gx0_75__Sx0_75,
			Gx0_75__Sx0_50,
			Gx0_50__Sx0_50
		},
		@[shl = 0]
		pub mode: bitvalue(u16) OperationMode {
			On = 0b000,
			Standby = 0b001,
			Sleep = 0b010,
			DeepSleep = 0b100
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x11: u16) PowerControl2 {
		@[shl = 8]
		pub dc1: bitvalue(u16) StepUp2Frequency {
			/// x1/4
			d4 = 0b000,
			/// x1/8
			d8,
			/// x1/16
			d16,
			/// x1/32
			d32,
			/// x1/64
			d64,
			/// x1/128
			d128,
			/// x1/256
			d256,
			Halt
		},
		@[shl = 4]
		pub dc0: bitvalue(u16) StepUp1Frequency {
			Full = 0b000,
			/// x1/2
			d2,
			/// x1/4
			d4,
			/// x1/8
			d8,
			/// x1/16
			d16,
			/// x1/32
			d32,
			/// x1/64
			d64,
			Halt
		},
		@[shl = 0]
		pub vc: bitvalue(u16) ReferenceVoltageFactor {
			x0_95 = 0b000,
			x0_90,
			x0_85,
			x0_80,
			x0_75,
			x0_70,
			x1_00
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x12: u16) PowerControl3 {
		@[shl = 7]
		pub vcire: bitvalue(u16) VCIRReferenceVoltage {
			/// Use Vci.
			External = 0b0,
			/// Use internal 2.5V reference.
			Internal = 0b1
		},
		@[shl = 4]
		pub pon: bitvalue(u16) VglOutput {
			Disabled = 0b0,
			Enabled = 0b1
		},
		/// Amplification rate of VCIR reference voltage with respect to VREG1OUT.
		@[shl = 0]
		pub vrh: bitvalue(u16) VReg1OutAmplify {
			Halt = 0b0000,
			x2_00,
			x2_05,
			x2_10,
			x2_20,
			x2_30,
			x2_40a,
			x2_40b,
			x1_60,
			x1_65,
			x1_70,
			x1_75,
			x1_80,
			x1_85,
			x1_90,
			x1_95
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x13: u16) PowerControl4 {
		/// Amplification rate of VREG1OUT voltage with respect to VCOM.
		@[shl = 8]
		pub vdv: bitvalue(u16) VComAmplify {
			x0_70 = 0b00000,
			x0_72,
			x0_74,
			x0_76,
			x0_78,
			x0_80,
			x0_82,
			x0_84,
			x0_86,
			x0_88,
			x0_90,
			x0_92,
			x0_94a = 0b01100,
			x0_96a = 0b01101,
			x0_98a = 0b01110,
			x1_00a = 0b01111,
			x0_94b = 0b10000,
			x0_96b = 0b10001,
			x0_98b = 0b10010,
			x1_00b = 0b10011,
			x1_02,
			x1_04,
			x1_06,
			x1_08,
			x1_10,
			x1_12,
			x1_14,
			x1_16,
			x1_18,
			x1_20,
			x1_22,
			x1_24
		}
	}
}
impl_write_command! {
	pub bitregister(u16 @ 0x29: u16) PowerControl7 {
		/// Amplification rate of VREG1OUT voltage with respect to VCOMH.
		@[shl = 0]
		pub vcm: bitvalue(u16) VComHAmplify {
			x0_685 = 0b000000,
			x0_690 = 0b000001,
			x0_695 = 0b000010,

			x0_700 = 0b000011,
			x0_705,
			x0_710,
			x0_715,
			x0_720,
			x0_725,
			x0_730,
			x0_735,
			x0_740,
			x0_745,
			x0_750,
			x0_755,
			x0_760,
			x0_765,
			x0_770,
			x0_775,
			x0_780,
			x0_785,
			x0_790,
			x0_795,

			x0_800 = 0b010111,
			x0_805,
			x0_810,
			x0_815,
			x0_820,
			x0_825,
			x0_830,
			x0_835,
			x0_840,
			x0_845,
			x0_850,
			x0_855,
			x0_860,
			x0_865,
			x0_870,
			x0_875,
			x0_880,
			x0_885,
			x0_890,
			x0_895,

			x0_900 = 0b101011,
			x0_905,
			x0_910,
			x0_915,
			x0_920,
			x0_925,
			x0_930,
			x0_935,
			x0_940,
			x0_945,
			x0_950,
			x0_955,
			x0_960,
			x0_965,
			x0_970,
			x0_975,
			x0_980,
			x0_985,
			x0_990,
			x0_995,

			x1_000 = 0b111111
		}
	}
}

// GRAM MANIPULATION //

bitvalue! {
	pub bitvalue(u16) GRamHorizontalAddress {
		0 ..= 0xEF
	}
}
bitvalue! {
	pub bitvalue(u16) GRamVerticalAddress {
		0 ..= 0x13F
	}
}

bitregister! {
	pub bitregister(u16 @ 0x20: u16) GRamSetHorizontalAddress {
		@[shl = 0]
		pub x: GRamHorizontalAddress
	}
}
impl WriteableRegister for GRamSetHorizontalAddress {}
bitregister! {
	pub bitregister(u16 @ 0x21: u16) GRamSetVerticalAddress {
		@[shl = 0]
		pub y: GRamVerticalAddress
	}
}
impl WriteableRegister for GRamSetVerticalAddress {}

bitregister! {
	pub bitregister(u16 @ 0x22: u16) GRamData {
		@[shl = 0]
		pub data: u16
	}
}
impl WriteableRegister for GRamData {}

bitregister! {
	pub bitregister(u16 @ 0x50: u16) GRamSetHorizontalWindowAreaStart {
		@[shl = 0]
		pub hsa: GRamHorizontalAddress
	}
}
impl WriteableRegister for GRamSetHorizontalWindowAreaStart {}
bitregister! {
	pub bitregister(u16 @ 0x51: u16) GRamSetHorizontalWindowAreaEnd {
		@[shl = 0]
		pub hea: GRamHorizontalAddress
	}
}
impl WriteableRegister for GRamSetHorizontalWindowAreaEnd {}
bitregister! {
	pub bitregister(u16 @ 0x52: u16) GRamSetVerticalWindowAreaStart {
		@[shl = 0]
		pub vsa: GRamVerticalAddress
	}
}
impl WriteableRegister for GRamSetVerticalWindowAreaStart {}
bitregister! {
	pub bitregister(u16 @ 0x53: u16) GRamSetVerticalWindowAreaEnd {
		@[shl = 0]
		pub vea: GRamVerticalAddress
	}
}
impl WriteableRegister for GRamSetVerticalWindowAreaEnd {}

// IMAGE CONTROLS //

impl_write_command! {
	pub bitregister(u16 @ 0x6A: u16) VerticalScrollControl {
		@[shl = 0]
		pub vl: bitvalue(u16) VerticalScrollValue {
			0 ..= 320
		}
	}
}

// PANEL CONTROL //

impl_write_command! {
	pub bitregister(u16 @ 0x90: u16) PanelInterfaceControl1 {
		@[shl = 8]
		pub divi: bitvalue(u16) InternalClockFrequencyDivision {
			d1 = 0b00,
			d2 = 0b01,
			d4 = 0b10,
			d8 = 0b11
		},
		@[shl = 0]
		pub rtni: bitvalue(u16) InternalClockTicksPerLine {
			16 ..= 31
		}
	}
}

impl_write_command! {
	pub bitregister(u16 @ 0x92: u16) PanelInterfaceControl2 {
		@[shl = 8]
		pub nowi: bitvalue(u16) InternalClockGateOutputNonOverlapPeriod {
			0 ..= 7
		}
	}
}

impl_write_command! {
	pub bitregister(u16 @ 0x95: u16) PanelInterfaceControl4 {
		@[shl = 8]
		pub dive: bitvalue(u16) RgbDotclockDivision {
			d4 = 0b01,
			d8 = 0b10,
			d16 = 0b11
		},
		@[shl = 0]
		pub rtne: bitvalue(u16) RgbTicksPerLine {
			16 ..= 63
		}
	}
}
