#![no_std]

use displaydoc::Display;

use embedded_graphics_core::{
	pixelcolor::{Rgb565, Rgb888},
	prelude::{Dimensions, DrawTarget, IntoStorage, OriginDimensions, Point, Size},
	primitives::Rectangle,
	Pixel
};
use embedded_hal::{blocking::delay::DelayUs, digital::v2::OutputPin};

use bitregister::{
	prelude::BitValue,
	register::access::{
		ResourceAccessBase,
		ResourceAccessRead,
		ResourceAccessReadError,
		ResourceAccessWriteError,
		ResourceAccessWriteIter
	},
	repr::endian::NativeEndian
};

pub mod commands;
pub use commands::*;

pub mod pixel_map;
use pixel_map::{PixelMapping, PixelMappingIdentity};

#[derive(Debug, Display)]
pub enum DriverError {
	/// failed to read from display: {0}
	ResourceAccessReadError(ResourceAccessReadError),
	/// failed to write to display: {0}
	ResourceAccessWriteError(ResourceAccessWriteError),
	/// failed to manipulate output pin
	OutputPinError,
	/// cannot hard reset when no reset pin is set
	NoResetPinError,
	/// invalid device code was read
	InvalidDeviceCode
}
impl From<ResourceAccessReadError> for DriverError {
	fn from(err: ResourceAccessReadError) -> Self {
		DriverError::ResourceAccessReadError(err)
	}
}
impl From<ResourceAccessWriteError> for DriverError {
	fn from(err: ResourceAccessWriteError) -> Self {
		DriverError::ResourceAccessWriteError(err)
	}
}

/// Inconstructible dummy pin which can be used for places where `Option<OutputPin>` is `None`.
pub enum DummpyPin {}
impl OutputPin for DummpyPin {
	type Error = &'static str;

	fn set_low(&mut self) -> Result<(), Self::Error> {
		unreachable!()
	}

	fn set_high(&mut self) -> Result<(), Self::Error> {
		unreachable!()
	}
}

pub trait DisplayAccess:
	ResourceAccessBase<AddressRepr = u16, WordRepr = u16> + ResourceAccessRead + ResourceAccessWriteIter
{
}
impl<
		T: ResourceAccessBase<AddressRepr = u16, WordRepr = u16>
			+ ResourceAccessRead
			+ ResourceAccessWriteIter
	> DisplayAccess for T
{
}

pub struct DisplayDriver<
	A: DisplayAccess,
	P: PixelMapping = PixelMappingIdentity,
	Rp: OutputPin = DummpyPin
> {
	access: A,
	reset_pin: Option<Rp>,
	pixel_mapping: core::marker::PhantomData<P>
}
impl<A: DisplayAccess, P: PixelMapping> DisplayDriver<A, P, DummpyPin> {
	/// ### Safety
	/// * `A` must be an access valid for commands written to it by this driver.
	pub unsafe fn new(access: A) -> Self {
		DisplayDriver {
			access,
			reset_pin: None,
			pixel_mapping: core::marker::PhantomData
		}
	}
}
impl<A: DisplayAccess, P: PixelMapping, Rp: OutputPin> DisplayDriver<A, P, Rp> {
	/// ### Safety
	/// * `A` must be an access valid for commands written to it by this driver.
	pub unsafe fn with_reset(access: A, reset_pin: Rp) -> Self {
		DisplayDriver {
			access,
			reset_pin: Some(reset_pin),
			pixel_mapping: core::marker::PhantomData
		}
	}

	fn read<R: ReadableRegister<BitRepr = A::WordRepr, AddressRepr = A::AddressRepr>>(
		&mut self
	) -> Result<Option<R>, DriverError> {
		unsafe {
			self.access
				.read_register_once::<_, NativeEndian>()
				.map_err(DriverError::from)
		}
	}

	fn write(
		&mut self,
		register: impl WriteableRegister<BitRepr = A::WordRepr, AddressRepr = A::AddressRepr>
	) -> Result<(), DriverError> {
		unsafe {
			self.access
				.write_register_once::<_, NativeEndian>(register)
				.map_err(DriverError::from)
		}
	}

	fn write_iter<R: WriteableRegister<BitRepr = A::WordRepr, AddressRepr = A::AddressRepr>>(
		&mut self,
		data: impl Iterator<Item = R>
	) -> Result<(), DriverError> {
		unsafe {
			self.access
				.write_register_iter::<_, NativeEndian, _>(data)
				.map_err(DriverError::from)
		}
	}

	pub fn initialize(&mut self, delay_source: &mut impl DelayUs<u32>) -> Result<(), DriverError> {
		if self.reset_pin.is_some() {
			self.hard_reset(delay_source)?;
		}

		self.power_on(delay_source)?;
		self.soft_reset()?;

		Ok(())
	}

	pub fn hard_reset(&mut self, delay_source: &mut impl DelayUs<u32>) -> Result<(), DriverError> {
		match self.reset_pin {
			None => Err(DriverError::NoResetPinError),
			Some(ref mut reset_pin) => {
				reset_pin
					.set_high()
					.map_err(|_| DriverError::OutputPinError)?;
				delay_source.delay_us(10);
				reset_pin
					.set_low()
					.map_err(|_| DriverError::OutputPinError)?;
				delay_source.delay_us(2_000);
				reset_pin
					.set_high()
					.map_err(|_| DriverError::OutputPinError)?;
				delay_source.delay_us(10);

				Ok(())
			}
		}
	}

	pub fn power_on(&mut self, delay_source: &mut impl DelayUs<u32>) -> Result<(), DriverError> {
		self.write(PowerControl1::default())?;
		self.write(PowerControl2::default())?;
		self.write(PowerControl3::default())?;
		self.write(PowerControl4::default())?;
		delay_source.delay_us(200_000);
		self.write(PowerControl1::new(
			SourceDriver::Enabled,
			StepUpFactors::VGHx4_VGLx3,
			PowerSupply::Enabled,
			GammaSourceCurrentAmplify::Gx1_00__Sx0_50,
			OperationMode::On
		))?;
		self.write(PowerControl2::new(
			StepUp2Frequency::d8,
			StepUp1Frequency::d8,
			ReferenceVoltageFactor::x1_00
		))?;
		delay_source.delay_us(50_000);
		self.write(PowerControl3::new(
			VCIRReferenceVoltage::External,
			VglOutput::Enabled,
			VReg1OutAmplify::x1_65
		))?;
		delay_source.delay_us(50_000);
		self.write(PowerControl4::new(VComAmplify::x0_70))?;
		self.write(PowerControl7::new(VComHAmplify::x0_765))?;
		delay_source.delay_us(50_000);

		Ok(())
	}

	pub fn soft_reset(&mut self) -> Result<(), DriverError> {
		match self.read::<LcdId>()? {
			Some(LcdId::Lcd9325) => (),
			_ => return Err(DriverError::InvalidDeviceCode)
		};

		// just a convenience macro
		macro_rules! execute_commands {
			(
				$(
					$command: expr
				),+ $(,)?
			) => {
				(
					$(
						self.write($command)?
					),+
				)
			};

			(
				@raw
				$(
					$reg: expr => $value: expr
				),+ $(,)?
			) => {
				{
					$(
						self.bus.write_command($reg)?;
						self.bus.write_data($value)?;
					)+
				}
			};
		}

		execute_commands!(
			StartOscillation::default(),
			// driver
			DriverOutputControl::new(DriverScanMode::Normal, DriverSourceScanDirection::Forward),
			DrivingWaveControl::new(DrivingWaveSetting::FieldInversion),
			EntryMode::new(
				GRamPixelTransferMode::OneTransfer,
				GRamColorFormat::Bgr,
				GRamHighSpeedWrite::Disabled,
				GRamOriginAddress::NoChange,
				P::RAM_ACCESS,
				P::RAM_INCREMENT,
				// GRamAccessDirection::HIncVInc,
				// GRamIncrementDirection::Horizontal
			),
			ResizingControl::default(),
			DisplayControl1::new(
				PanelPartialImage::Disabled,
				GateDriverOutputLevel::Normal,
				ColorDisplayMode::Full,
				DisplayPanelMode::On
			),
			DisplayControl2::new(
				FrontPorchLines::from_bits(2).unwrap(),
				BackPorchLines::from_bits(14).unwrap()
			),
			DisplayControl3::default(),
			DisplayControl4::default(),
			RgbDisplayInterfaceControl1::new(
				RgbGRamWriteCycles::default(),
				GramAccessInterface::SystemOrVsync,
				DisplayOperationMode::InternalSystemClock,
				RgbInterfaceDataWdith::Bits16
			),
			FrameMarkerPosition::default(),
			RgbDisplayInterfaceControl2::default(),
			DriverOutputControl2::new(
				DriverScanDirection::Forward,
				DriverScanLines::Lines320,
				DriverScanStart::Offset0
			),
			// image and gram
			BaseImageDisplayControl::new(
				NonDisplayAreaLevel::default(),
				VerticalScroll::Disabled,
				GrayscalePolarityMode::HighActive
			),
			VerticalScrollControl::default(),
			GRamSetHorizontalWindowAreaStart::default(),
			GRamSetHorizontalWindowAreaEnd::new(GRamHorizontalAddress::MAX),
			GRamSetVerticalWindowAreaStart::default(),
			GRamSetVerticalWindowAreaEnd::new(GRamVerticalAddress::MAX),
			// panel interface
			PanelInterfaceControl1::new(
				InternalClockFrequencyDivision::d1,
				InternalClockTicksPerLine::from_bits(16).unwrap()
			),
			PanelInterfaceControl2::default(),
			PanelInterfaceControl4::default()
		);

		// gamma curve
		// {
		// 	execute_commands!(
		// 		@raw
		// 		48 => 0x0007,
		// 		49 => 0x0302,
		// 		50 => 0x0105,
		// 		53 => 0x0206,
		// 		54 => 0x0808,
		// 		55 => 0x0206,
		// 		56 => 0x0504,
		// 		57 => 0x0007,
		// 		60 => 0x0105,
		// 		61 => 0x0808
		// 	);
		// }

		// partial display
		// {
		// 	execute_commands!(
		// 		@raw
		// 		128 => 0x0000,
		// 		129 => 0x0000,
		// 		130 => 0x0000,
		// 		131 => 0x0000,
		// 		132 => 0x0000,
		// 		133 => 0x0000
		// 	);
		// }

		Ok(())
	}

	fn try_set_address(&mut self, point: Point) -> Result<bool, DriverError> {
		let x_addr = u16::try_from(point.x)
			.ok()
			.and_then(GRamHorizontalAddress::from_bits);
		let y_addr = u16::try_from(point.y)
			.ok()
			.and_then(GRamVerticalAddress::from_bits);

		let res = match (x_addr, y_addr) {
			(Some(x), Some(y)) => {
				self.write(GRamSetHorizontalAddress::new(x))?;
				self.write(GRamSetVerticalAddress::new(y))?;

				true
			}
			_ => false
		};

		Ok(res)
	}

	pub fn debug_pattern(&mut self) -> Result<(), DriverError> {
		let address_set = self.try_set_address(P::TRANSFORM_ORIGIN)?;
		debug_assert!(address_set);

		let width = P::DISPLAY_SIZE.width as usize;
		let height = P::DISPLAY_SIZE.height as usize;

		// issue ram write command
		self.write_iter(
			(0usize .. width * height)
				.map(|index| {
					let x = index % width;
					let y = index / width;

					let red = x as f32 / width as f32 * 255.0;
					let green = y as f32 / height as f32 * 255.0;

					Rgb565::from(Rgb888::new(red as u8, green as u8, 0)).into_storage()
				})
				.map(GRamData::new)
		)?;

		Ok(())
	}
}
impl<A: DisplayAccess, P: PixelMapping, Rp: OutputPin> OriginDimensions
	for DisplayDriver<A, P, Rp>
{
	fn size(&self) -> Size {
		P::DISPLAY_SIZE
	}
}
impl<A: DisplayAccess, P: PixelMapping, Rp: OutputPin> DrawTarget for DisplayDriver<A, P, Rp> {
	type Color = Rgb565;
	type Error = DriverError;

	fn draw_iter<I: IntoIterator<Item = Pixel<Self::Color>>>(
		&mut self,
		pixels: I
	) -> Result<(), Self::Error> {
		for Pixel(coords, color) in pixels.into_iter() {
			if self.try_set_address(P::transform_point(coords))? {
				self.write(GRamData::new(color.into_storage()))?;
			}
		}

		Ok(())
	}

	fn fill_contiguous<I: IntoIterator<Item = Self::Color>>(
		&mut self,
		area: &Rectangle,
		colors: I
	) -> Result<(), Self::Error> {
		fn advance_by(iter: &mut impl Iterator, n: usize) -> Result<(), usize> {
			for i in 0 .. n {
				iter.next().ok_or(i)?;
			}

			Ok(())
		}

		let mut pixels = colors.into_iter();

		let draw_area = area.intersection(&self.bounding_box());
		let draw_area_bottom_right = match draw_area.bottom_right() {
			None => return Ok(()),
			Some(br) => br
		};
		let area_bottom_right = area.bottom_right().unwrap();

		// skip leading rows
		let skip_rows_before = usize::try_from(draw_area.top_left.y - area.top_left.y).unwrap_or(0);
		let _ = advance_by(&mut pixels, area.size.width as usize * skip_rows_before);

		// skipped pixels before and after each row
		let skip_before = usize::try_from(draw_area.top_left.x - area.top_left.x).unwrap_or(0);
		let skip_after =
			usize::try_from(area_bottom_right.x - draw_area_bottom_right.x).unwrap_or(0);

		// starting x position and y iterator
		let x_start = draw_area.top_left.x;
		let y_iter = draw_area.top_left.y ..= draw_area_bottom_right.y;

		for y_start in y_iter {
			let _ = advance_by(&mut pixels, skip_before);

			let address_set = self.try_set_address(P::transform_point(Point {
				x: x_start,
				y: y_start
			}))?;
			debug_assert!(address_set);

			self.write_iter(
				(&mut pixels)
					.take(draw_area.size.width as usize)
					.map(|color| GRamData::new(color.into_storage()))
			)?;


			let _ = advance_by(&mut pixels, skip_after);
		}

		Ok(())
	}

	fn clear(&mut self, color: Self::Color) -> Result<(), Self::Error> {
		let address_set = self.try_set_address(P::TRANSFORM_ORIGIN)?;
		debug_assert!(address_set);

		self.write_iter(
			core::iter::repeat(GRamData::new(color.into_storage()))
				.take(P::DISPLAY_SIZE.width as usize * P::DISPLAY_SIZE.height as usize)
		)?;

		Ok(())
	}
}
