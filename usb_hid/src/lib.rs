#![no_std]

use bitregister::{
	prelude::{BitPackedRepr, BitPackedValue},
	repr::endian::LittleEndian
};
use usb_device::{
	class::{ControlIn, ControlOut, UsbClass},
	class_prelude::{
		DescriptorWriter,
		EndpointIn,
		EndpointOut,
		InterfaceNumber,
		UsbBus,
		UsbBusAllocator
	},
	control::{Recipient, Request, RequestType},
	UsbError
};

#[doc(hidden)]
pub use bitregister;
#[doc(hidden)]
pub use bytemuck;

mod class;
pub mod report;

pub mod known {
	#[derive(Debug, Clone, Copy)]
	pub enum HidSubclass {
		None,
		Boot(HidBootProtocol)
	}
	impl HidSubclass {
		pub const CLASS: u8 = 0x03;

		/// Returns `(USB_CLASS, USB_SUBCLASS, USB_PROTOCOL)`.
		pub const fn interface_config(self) -> (u8, u8, u8) {
			let (subclass, protocol) = match self {
				HidSubclass::None => (0, 0),
				HidSubclass::Boot(protocol) => (1, protocol as u8)
			};

			(Self::CLASS, subclass, protocol)
		}
	}

	#[repr(u8)]
	#[derive(Debug, Clone, Copy)]
	pub enum HidBootProtocol {
		None = 0x00,
		Keyboard = 0x01,
		Mouse = 0x02
	}
}

pub trait HidReport: BitPackedValue {
	type DescriptorType: BitPackedValue;

	fn build_descriptor() -> <Self::DescriptorType as BitPackedValue>::PackedRepr;
}

pub struct HidUsbClass<'alloc, B: UsbBus, R: HidReport> {
	subclass_config: known::HidSubclass,
	interface: InterfaceNumber,
	endpoint_in: EndpointIn<'alloc, B>,
	endpoint_out: EndpointOut<'alloc, B>,
	_report: core::marker::PhantomData<R>
}
impl<'alloc, B: UsbBus, R: HidReport> HidUsbClass<'alloc, B, R> {
	pub fn new(allocator: &'alloc UsbBusAllocator<B>) -> Self {
		HidUsbClass {
			subclass_config: known::HidSubclass::None,
			interface: allocator.interface(),
			endpoint_in: allocator.interrupt(64, 10),
			endpoint_out: allocator.interrupt(64, 10),
			_report: core::marker::PhantomData
		}
	}

	pub fn push_report(&mut self, report: R) -> usb_device::Result<()> {
		let report_packed = report.pack::<LittleEndian>();
		let report_bytes = report_packed.as_bytes_without_padding();

		let written = self.endpoint_in.write(report_bytes)?;
		assert_eq!(written, report_bytes.len());

		Ok(())
	}

	pub fn pull_report(&mut self) -> usb_device::Result<R> {
		// let mut buffer: [u8; core::mem::size_of::<R::PackedRepr>()] = [0u8; core::mem::size_of::<R::PackedRepr>()];
		let mut buffer: [u8; 64] = [0u8; 64];

		let read = self.endpoint_out.read(&mut buffer)?;
		let read_bytes = &buffer[.. read];

		BitPackedRepr::try_from_bytes(read_bytes)
			.and_then(|packed| R::unpack::<LittleEndian>(&packed))
			.ok_or(UsbError::ParseError)
	}

	fn build_class_descriptor() -> class::ClassDescriptorHidMessage {
		class::known::build_hid_class_descriptor(
			core::mem::size_of::<<R::DescriptorType as BitPackedValue>::PackedRepr>()
				.try_into()
				.unwrap()
		)
	}

	fn request_addressed_to_us(&self, request: &Request) -> bool {
		request.recipient == Recipient::Interface
			&& request.index == u8::from(self.interface) as u16
	}
}
impl<'alloc, B: UsbBus, R: HidReport> UsbClass<B> for HidUsbClass<'alloc, B, R> {
	fn get_configuration_descriptors(
		&self,
		writer: &mut DescriptorWriter
	) -> usb_device::Result<()> {
		// register the interface
		let (class, subclass, protocol) = self.subclass_config.interface_config();
		writer.interface(self.interface, class, subclass, protocol)?;

		// report hid descriptor configuration
		// interleaved between interface and endpoints per spec
		let class_descriptor = Self::build_class_descriptor();
		let descriptor_without_prefix = class_descriptor.descriptor.pack::<LittleEndian>();
		writer.write(
			class_descriptor.descriptor_type,
			descriptor_without_prefix.as_bytes_without_padding()
		)?;

		// register the endpoints
		writer.endpoint(&self.endpoint_out)?;
		writer.endpoint(&self.endpoint_in)?;

		Ok(())
	}

	fn control_out(&mut self, transfer: ControlOut<B>) {
		let request = transfer.request();

		if !self.request_addressed_to_us(request) {
			return
		}

		let _ = match request.request {
			class::known::request::SET_IDLE => transfer.accept(),
			class::known::request::SET_REPORT => transfer.reject(),
			class::known::request::SET_PROTOCOL => transfer.reject(),
			_ => transfer.reject()
		};
	}

	fn control_in(&mut self, transfer: ControlIn<B>) {
		let request = transfer.request();

		if !self.request_addressed_to_us(request) {
			return
		}

		let _ = match (request.request_type, request.request) {
			(RequestType::Standard, Request::GET_DESCRIPTOR) => match (request.value >> 8) as u8 {
				class::known::descriptor::CLASS_DESCRIPTOR => {
					let class_descriptor = Self::build_class_descriptor().pack::<LittleEndian>();
					transfer.accept_with(class_descriptor.as_bytes_without_padding())
				}
				class::known::descriptor::REPORT_DESCRIPTOR => {
					let report_descriptor = R::build_descriptor();
					transfer.accept_with(report_descriptor.as_bytes())
				}
				_ => Ok(())
			},
			(RequestType::Class, class::known::request::GET_REPORT) => transfer.reject(),
			(RequestType::Class, class::known::request::GET_IDLE) => transfer.reject(),
			(RequestType::Class, class::known::request::GET_PROTOCOL) => transfer.reject(),
			_ => Ok(())
		}
		.unwrap();
	}
}
