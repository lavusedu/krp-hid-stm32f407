use bitregister::bitpacked;

pub mod known;

bitpacked! {
	pub struct ClassDescriptorHidMessage {
		pub length: u8,
		pub descriptor_type: u8,
		pub descriptor: ClassDescriptorHid
	} -> ClassDescriptorHidMessagePacked
}

bitpacked! {
	pub struct ClassDescriptorHid {
		pub spec_version: u16,
		pub language_id: u8,
		pub descriptor_count: u8,
		pub report_descriptor_type: u8,
		pub report_descriptor_len: u16
	} -> ClassDescriptorHidPacked
}

#[cfg(test)]
mod test {
	use bitregister::{prelude::BitPackedValue, repr::endian::LittleEndian};

	use super::known;

	#[test]
	fn test_hid_class_descriptor_correct() {
		let class_descriptor = known::build_hid_class_descriptor(0xABCD).pack::<LittleEndian>();

		assert_eq!(
			class_descriptor.as_bytes_without_padding(),
			&[9, 0x21, 0x11, 0x01, 0, 1, 0x22, 0xCD, 0xAB]
		);
	}
}
