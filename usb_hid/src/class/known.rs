use super::{ClassDescriptorHid, ClassDescriptorHidMessage, ClassDescriptorHidMessagePacked};

pub mod request {
	pub const GET_REPORT: u8 = 0x01;
	pub const GET_IDLE: u8 = 0x02;
	pub const GET_PROTOCOL: u8 = 0x03;

	pub const SET_REPORT: u8 = 0x09;
	pub const SET_IDLE: u8 = 0x0A;
	pub const SET_PROTOCOL: u8 = 0x0B;
}

pub mod descriptor {
	pub const CLASS_DESCRIPTOR: u8 = 0x21;
	pub const REPORT_DESCRIPTOR: u8 = 0x22;
}

pub fn build_hid_class_descriptor(report_descriptor_len: u16) -> ClassDescriptorHidMessage {
	ClassDescriptorHidMessage {
		length: core::mem::size_of::<ClassDescriptorHidMessagePacked>()
			.try_into()
			.unwrap(),
		descriptor_type: descriptor::CLASS_DESCRIPTOR,
		descriptor: ClassDescriptorHid {
			spec_version: 0x0111,
			language_id: 0,
			descriptor_count: 1,
			report_descriptor_type: descriptor::REPORT_DESCRIPTOR,
			report_descriptor_len
		}
	}
}
