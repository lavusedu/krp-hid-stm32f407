use bitregister::bitvalue;

bitvalue! {
	pub bitvalue(u8) ItemTag {
		0x00 ..= 0x0F
	}
}
bitvalue! {
	pub bitvalue(u8) ItemType {
		Main = 0b00,
		Global = 0b01,
		Local = 0b10
	}
}
bitvalue! {
	pub bitvalue(u8) ItemSize {
		Bytes0 = 0b00,
		Bytes1 = 0b01,
		Bytes2 = 0b10,
		Bytes4 = 0b11
	}
}
bitvalue! {
	pub bitvalue(u8) ItemPrefix {
		@[shl = 4]
		pub tag: ItemTag,
		@[shl = 2]
		pub ty: ItemType,
		@[shl = 0]
		pub size: ItemSize
	}
}

bitvalue! {
	pub bitvalue(u8) MainItemTag {
		/// Refers to the data from one or more similar controls on a device.
		///
		/// For example, variable data such as reading the position of a single
		/// axis or a group of levers or array data such as one or more push buttons or switches.
		Input = 0b1000,
		/// Refers to the data to one or more similar controls on a device such as setting the position of a single axis or a group of levers (variable data).
		///
		/// Or, it can represent data to one or more LEDs (array data).
		Output = 0b1001,
		/// Describes device input and output not intended for consumption by the end user — for example, a software feature or Control Panel toggle.
		Feature = 0b1011,
		/// A meaningful grouping of Input, Output, and Feature items — for example, mouse, keyboard, joystick, and pointer.
		Collection = 0b1010,
		/// A terminating item used to specify the end of a collection of items.
		EndCollection = 0b1100
	}
}

bitvalue! {
	/// Indicates whether the item is data or a constant value.
	///
	/// Data indicates the item is defining report fields that contain
	/// modifiable device data. Constant indicates the item is a static
	/// read-only field in a report and cannot be modified (written) by the host.
	pub bitvalue(u8) MainItemConstant {
		Data = 0b0,
		Constant = 0b1
	}
}
bitvalue! {
	/// Indicates whether the item creates variable or array data fields in reports.
	///
	/// In variable fields, each field represents data from a physical control.
	/// The number of bits reserved for each field is determined by preceding Report
	/// Size/Report Count items. For example, a bank of eight on/off switches could be
	/// reported in 1 byte declared by a variable Input item where each bit represents one switch,
	/// on (1) or off (0) (Report Size = 1, Report Count = 8). Alternatively, a variable
	/// Input item could add 1 report byte used to represent the state of four three-position buttons,
	/// where the state of each button is represented by two bits (Report Size = 2, Report Count = 4).
	/// Or 1 byte from a variable Input item could represent the x position of a joystick (Report Size = 8, Report Count = 1).
	///
	/// An array provides an alternate means for describing the data returned from a group of buttons.
	/// Arrays are more efficient, if less flexible than variable items. Rather than returning
	/// a single bit for each button in the group, an array returns an index in each field that
	/// corresponds to the pressed button (like keyboard scan codes). An out-of range value in an
	/// array field is considered no controls asserted. Buttons or keys in an array that are simultaneously
	/// pressed need to be reported in multiple fields. Therefore, the number of fields in an array input
	/// item (Report Count) dictates the maximum number of simultaneous controls that can be reported.
	/// A keyboard could report up to three simultaneous keys using an array with three 8-bit fields (Report Size = 8, Report Count = 3).
	/// Logical Minimum specifies the lowest index value returned by the array and Logical Maximum specifies the largest.
	/// The number of elements in the array can be deduced by examining the difference between Logical Minimum
	/// and Logical Maximum (number of elements = Logical Maximum - Logical Minimum + 1).
	pub bitvalue(u8) MainItemVariable {
		Array = 0b0,
		Variable = 0b1
	}
}
bitvalue! {
	/// Indicates whether the data is absolute (based on a fixed origin) or relative (indicating the change in value from the last report).
	///
	/// Mouse devices usually provide relative data, while tablets usually provide absolute data.
	pub bitvalue(u8) MainItemRelative {
		Absolute = 0b0,
		Relative = 0b1
	}
}
bitvalue! {
	/// Indicates whether the data "rolls over" when reaching either the extreme high or low value.
	///
	/// For example, a dial that can spin freely 360 degrees might output values from 0 to 10. If Wrap is indicated, the next value reported after passing the 10 position in the increasing direction would be 0.
	pub bitvalue(u8) MainItemWrap {
		NoWrap = 0b0,
		Wrap = 0b1
	}
}
bitvalue! {
	/// Indicates whether the raw data from the device has been processed in some way,
	/// and no longer represents a linear relationship between what is measured and the data that is reported.
	///
	/// Acceleration curves and joystick dead zones are examples of this kind of data.
	/// Sensitivity settings would affect the Units item, but the data would still be linear.
	pub bitvalue(u8) MainItemLinear {
		Linear = 0b0,
		NonLinear = 0b1
	}
}
bitvalue! {
	/// Indicates whether the control has a preferred state to which it will return when the user is not physically interacting with the control.
	///
	/// Push buttons (as opposed to toggle buttons) and self-centering joysticks are examples.
	pub bitvalue(u8) MainItemPreferredState {
		PreferredState = 0b0,
		NoPreferredState = 0b1
	}
}
bitvalue! {
	/// Indicates whether the control has a state in which it is not sending meaningful data.
	///
	/// One possible use of the null state is for controls that require
	/// the user to physically interact with the control in order for
	/// it to report useful data. For example, some joysticks have
	/// a multidirectional switch (a hat switch). When a hat switch is
	/// not being pressed it is in a null state. When in a null state,
	/// the control will report a value outside of the specified Logical
	/// Minimum and Logical Maximum (the most negative value, such as -128
	/// for an 8-bit value).
	pub bitvalue(u8) MainItemNull {
		NonNull = 0b0,
		HasNull = 0b1
	}
}
bitvalue! {
	/// Indicates whether the Feature or Output control's value should be changed by the host or not.
	///
	/// Volatile output can change with or without host interaction.
	/// To avoid synchronization problems, volatile controls should be
	/// relative whenever possible. If volatile output is absolute, when
	/// issuing a Set Report (Output), request set the value of any control
	/// you don't want to change to a value outside of the specified Logical
	/// Minimum and Logical Maximum (the most negative value, such as -128 for
	/// an 8-bit value). Invalid output to a control is ignored by the device.
	///
	/// Must be 0 for Input items.
	pub bitvalue(u8) MainItemVolatile {
		NonVolatile = 0b0,
		Volatile = 0b1
	}
}
bitvalue! {
	/// Indicates that the control emits a fixed-sizestream of bytes.
	///
	/// The contents of the data field are determined by the application.
	/// The contents of the buffer are not interpreted as a single numeric
	/// quantity. Report data defined by a Buffered Bytes item must be
	/// aligned on an 8-bit boundary. The data from a bar code reader is an example.
	pub bitvalue(u8) MainItemBufferedBytes {
		BitField = 0b0,
		BufferedBytes = 0b1
	}
}

bitvalue! {
	pub bitvalue(u8) MainItemInputData {
		@[shl = 0]
		pub constant: MainItemConstant,
		@[shl = 1]
		pub variable: MainItemVariable,
		@[shl = 2]
		pub relative: MainItemRelative,
		@[shl = 3]
		pub wrap: MainItemWrap,
		@[shl = 4]
		pub linear: MainItemLinear,
		@[shl = 5]
		pub preferred_state: MainItemPreferredState,
		@[shl = 6]
		pub null: MainItemNull,
		@[shl = 8]
		pub buffered_bytes: MainItemBufferedBytes
	}
}

bitvalue! {
	pub bitvalue(u8) MainItemOutputData {
		@[shl = 0]
		pub constant: MainItemConstant,
		@[shl = 1]
		pub variable: MainItemVariable,
		@[shl = 2]
		pub relative: MainItemRelative,
		@[shl = 3]
		pub wrap: MainItemWrap,
		@[shl = 4]
		pub linear: MainItemLinear,
		@[shl = 5]
		pub preferred_state: MainItemPreferredState,
		@[shl = 6]
		pub null: MainItemNull,
		@[shl = 7]
		pub volatile: MainItemVolatile,
		@[shl = 8]
		pub buffered_bytes: MainItemBufferedBytes
	}
}
pub type MainItemFeatureData = MainItemOutputData;

bitvalue! {
	pub bitvalue(u8) MainItemCollectionData {
		/// A physical collection is used for a set of data items that represent data points collected at one geometric point.
		///
		/// This is useful for sensing devices which may need to associate sets of measured or sensed data with a single point.
		/// It does not indicate that a set of data values comes from one device, such as a keyboard. In the case of device which
		/// reports the position of multiple sensors, physical collections are used to show which data comes from each separate sensor.
		Physical = 0x00,
		/// A group of Main items that might be familiar to applications.
		///
		/// It could also be used to identify item groups serving different purposes in a single device.
		/// Common examples are a keyboard or mouse. A keyboard with an integrated pointing device could
		/// be defined as two different application collections. Data reports are usually (but not necessarily)
		/// associated with application collections (at least one report ID per application).
		Application = 0x01,
		/// A logical collection is used when a set of data items form a composite data structure.
		///
		/// An example of this is the association between a data buffer and a byte count of the data.
		/// The collection establishes the link between the count and the buffer.
		Logical = 0x02,
		/// Defines a logical collection that wraps all the fields in a report.
		///
		/// A unique report ID will be contained in this collection.
		/// An application can easily determine whether a device supports a certain function.
		/// Note that any valid Report ID value can be declared for a Report collection.
		Report = 0x03,
		/// A named array is a logical collection contains an array of selector usages.
		///
		/// For a given function the set of selectors used by similar devices may vary.
		/// The naming of fields is common practice when documenting hardware registers.
		/// To determine whether a device supports a particular function like Status, an application might
		/// have to query for several known Status selector usages before it could determine whether the device
		/// supported Status. The Named Array usages allows the Array field that contains the selectors to be named,
		/// thus the application only needs to query for the Status usage to determine that a device supports status information.
		NamedArray = 0x04,
		/// A Usage Switch is a logical collection that modifies the meaning of the usages that it contains.
		///
		/// This collection type indicates to an application that the usages found in this collection must be
		/// special cased. For instance, rather than declaring a usage on the LED page for every possible function,
		/// an Indicator usage can be applied to a Usage Switch collection and the standard usages defined in that
		/// collection can now be identified as indicators for a function rather than the function itself.
		/// Note that this collection type is not used for the labeling Ordinal collections, a Logical collection type is used for that.
		UsageSwitch = 0x05,
		/// Modifies the meaning of the usage attached to the encompassing collection.
		///
		/// A usage typically defines a single operating mode for a control. The usage modifier
		/// allows the operating mode of a control to be extended. For instance, an LED is
		/// typically on or off. For particular states a device may want a generic method of
		/// blinking or choosing the color of a standard LED. Attaching the LED usage to a Usage Modifier
		/// collection will indicate to an application that the usage supports a new operating mode.
		UsageModifier = 0x06
	}
}

bitvalue! {
	pub bitvalue(u8) GlobalItemTag {
		/// Unsigned integer specifying the current Usage Page.
		///
		/// Since a usage are 32 bit values, Usage Page items can be used to conserve
		/// space in a report descriptor by setting the high order 16 bits of a subsequent
		/// usages. Any usage that follows which is defines 16 bits or less is interpreted
		/// as a Usage ID and concatenated with the Usage Page to form a 32 bit Usage.
		UsagePage = 0b0000,
		/// Extent value in logical units.
		///
		/// This is the minimum value that a variable or array item will report.
		/// For example, a mouse reporting x position values from 0 to 128 would
		/// have a Logical Minimum of 0 and a Logical Maximum of 128.
		LogicalMinimum,
		/// Extent value in logical units.
		///
		/// This is the maximum value that a variable or array item will report.
		LogicalMaximum,
		/// Minimum value for the physical extent of a variable item.
		///
		/// This represents the Logical Minimum with units applied to it.
		PhysicalMinimum,
		/// Maximum value for the physical extent of a variable item.
		PhysicalMaximum,
		/// Value of the unit exponent in base 10.
		UnitExponent,
		/// Unit values.
		Unit,
		/// Unsigned integer specifying the size of the report fields in bits.
		///
		/// This allows the parser to build an item map for the report handler to use.
		ReportSize,
		/// Unsigned value that specifies the Report ID.
		///
		/// If a Report ID tag is used anywhere in Report descriptor,
		/// all data reports for the device are preceded by a single byte ID field.
		/// All items succeeding the first Report ID tag but preceding a second
		/// Report ID tag are included in a report prefixed by a 1-byte ID.
		/// All items succeeding the second but preceding a third Report ID tag
		/// are included in a second report prefixed by a second ID, and so on.
		///
		/// This Report ID value indicates the prefix added to a particular report.
		/// For example, a Report descriptor could define a 3-byte report with a
		/// Report ID of 01. This device would generate a 4-byte data report in
		/// which the first byte is 01. The device may also generate other reports,
		/// each with a unique ID. This allows the host to distinguish different types
		/// of reports arriving over a single interrupt in pipe. And allows the device
		/// to distinguish different types of reports arriving over a single interrupt
		/// out pipe. Report ID zero is reserved and should not be used.
		ReportId,
		/// Unsigned integer specifying the number of data fields for the item.
		///
		/// Determines how many fields are included in the report for this particular
		/// item (and consequently how many bits are added to the report).
		ReportCount,
		/// Places a copy of the global item state table on the stack.
		Push,
		/// Replaces the item state table with the top structure from the stack.
		Pop
	}
}

bitvalue! {
	pub bitvalue(u8) LocalItemTag {
		/// Usage index for an item usage; represents a suggested usage for the item or collection.
		///
		/// In the case where an item represents multiple controls,
		/// a Usage tag may suggest a usage for every variable or element in an array.
		Usage = 0b0000,
		/// Defines the starting usage associated with an array or bitmap.
		UsageMinimum,
		/// Defines the ending usage associated with an array or bitmap.
		UsageMaximum,
		/// Determines the body part used for a control. Index points to a designator in the Physical descriptor.
		DesignatorIndex,
		/// Defines the index of the starting designator associated with an array or bitmap.
		DesignatorMinimum,
		/// Defines the index of the ending designator associated with an array or bitmap.
		DesignatorMaximum,
		/// String index for a String descriptor; allows a string to be associated with a particular item or control.
		StringIndex = 0b0111,
		/// Specifies the first string index when assigning a group of sequential strings to controls in an array or bitmap.
		StringMinimum,
		/// Specifies the last string index when assigning a group of sequential strings to controls in an array or bitmap.
		StringMaximum,
		/// Defines the beginning or end of a set of local items (1 = open set, 0 = close set).
		Delimeter
	}
}
