#[macro_export]
macro_rules! hid_descriptor {
	(
		$visibility: vis descriptor $name: ident {
			$(
				$field_name: ident:
				$item_type: ident $item_tag: ident
				$( ($item_data: expr)[$item_data_type: ty] )?
			),+ $(,)?
		}
	) => {
		$crate::bitregister::bitpacked! {
			$visibility struct $name {
				$(
					pub $field_name: $crate::hid_descriptor!(__field_type $item_type $item_tag $( $item_data_type )?)
				),+
			} -> PackedDescriptor
		}
		impl $name {
			pub fn build() -> Self {
				use $crate::bitregister::prelude::BitPackedValue;

				Self {
					$(
						$field_name: $crate::hid_descriptor!(__field_value $item_type $item_tag $( $item_data )?)
					),+
				}
			}

			pub fn build_packed() -> PackedDescriptor {
				Self::build().pack::<$crate::bitregister::repr::endian::LittleEndian>().data
			}
		}
	};

	// Field type
	// __field_type $( $data_type: ty )?

	( __field_type Inline $data_type: ty ) => { $data_type };
	( __field_type $item_type: ident $item_tag: ident $data_type: ty ) => { $crate::report::DescriptorItem<$data_type> };
	( __field_type $item_type: ident $item_tag: ident ) => { $crate::report::DescriptorItem<$crate::bitregister::prelude::UnitRepr> };

	// Field value
	// __field_value $ty: ident $tag: ident $( $data: expr )?

	( __field_value Global $tag: ident $( $data: expr )? ) => {
		$crate::report::DescriptorItem::global(
			$crate::report::items::GlobalItemTag::$tag,
			{ $crate::bitregister::prelude::UnitRepr $(; $data as _)? }
		)
	};
	( __field_value Local $tag: ident $( $data: expr )? ) => {
		$crate::report::DescriptorItem::local(
			$crate::report::items::LocalItemTag::$tag,
			{ $crate::bitregister::prelude::UnitRepr $(; $data as _)? }
		)
	};
	( __field_value Main Input $data: expr ) => {
		$crate::report::DescriptorItem::main_input(
			$data
		)
	};
	( __field_value Main Output $data: expr ) => {
		$crate::report::DescriptorItem::main_output(
			$data
		)
	};
	( __field_value Main Feature $data: expr ) => {
		$crate::report::DescriptorItem::main_feature(
			$data
		)
	};
	( __field_value Main Collection $data: expr ) => {
		$crate::report::DescriptorItem::main_collection(
			$data
		)
	};
	( __field_value Main EndCollection ) => {
		$crate::report::DescriptorItem::main_end_collection()
	};
	( __field_value Inline $data_type: ty ) => {
		<$data_type>::build()
	};
}
