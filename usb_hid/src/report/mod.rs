pub mod definition;
pub mod items;
pub mod known;

use bitregister::prelude::{BitValue, BitvalueField, ReprType, UnitRepr};

use items::{
	GlobalItemTag,
	ItemPrefix,
	ItemSize,
	ItemTag,
	ItemType,
	LocalItemTag,
	MainItemCollectionData,
	MainItemFeatureData,
	MainItemInputData,
	MainItemOutputData,
	MainItemTag
};

pub struct DescriptorItem<D: BitValue>
where
	ReprTypePlus8Value: ReprTypePlus8<D::BitRepr>,
	ItemSize: ItemSizeFromReprType<D::BitRepr>
{
	prefix: ItemPrefix,
	data: D
}
impl<D: BitValue> DescriptorItem<D>
where
	ReprTypePlus8Value: ReprTypePlus8<D::BitRepr>,
	ItemSize: ItemSizeFromReprType<D::BitRepr>
{
	const DATA_ITEM_SIZE: ItemSize = ItemSize::DATA_ITEM_SIZE;

	pub fn global(tag: GlobalItemTag, data: D) -> Self {
		DescriptorItem {
			prefix: ItemPrefix::new(
				ItemTag::from_bits(tag.to_bits()).unwrap(),
				ItemType::Global,
				Self::DATA_ITEM_SIZE
			),
			data
		}
	}

	pub fn local(tag: LocalItemTag, data: D) -> Self {
		DescriptorItem {
			prefix: ItemPrefix::new(
				ItemTag::from_bits(tag.to_bits()).unwrap(),
				ItemType::Local,
				Self::DATA_ITEM_SIZE
			),
			data
		}
	}
}
impl DescriptorItem<MainItemInputData> {
	pub fn main_input(data: MainItemInputData) -> Self {
		DescriptorItem {
			prefix: ItemPrefix::new(
				ItemTag::from_bits(MainItemTag::Input.to_bits()).unwrap(),
				ItemType::Main,
				Self::DATA_ITEM_SIZE
			),
			data
		}
	}
}
impl DescriptorItem<MainItemOutputData> {
	pub fn main_output(data: MainItemOutputData) -> Self {
		DescriptorItem {
			prefix: ItemPrefix::new(
				ItemTag::from_bits(MainItemTag::Output.to_bits()).unwrap(),
				ItemType::Main,
				Self::DATA_ITEM_SIZE
			),
			data
		}
	}
}
impl DescriptorItem<MainItemFeatureData> {
	pub fn main_feature(data: MainItemFeatureData) -> Self {
		DescriptorItem {
			prefix: ItemPrefix::new(
				ItemTag::from_bits(MainItemTag::Feature.to_bits()).unwrap(),
				ItemType::Main,
				Self::DATA_ITEM_SIZE
			),
			data
		}
	}
}
impl DescriptorItem<MainItemCollectionData> {
	pub fn main_collection(data: MainItemCollectionData) -> Self {
		DescriptorItem {
			prefix: ItemPrefix::new(
				ItemTag::from_bits(MainItemTag::Collection.to_bits()).unwrap(),
				ItemType::Main,
				Self::DATA_ITEM_SIZE
			),
			data
		}
	}
}
impl DescriptorItem<UnitRepr> {
	pub fn main_end_collection() -> Self {
		DescriptorItem {
			prefix: ItemPrefix::new(
				ItemTag::from_bits(MainItemTag::EndCollection.to_bits()).unwrap(),
				ItemType::Main,
				Self::DATA_ITEM_SIZE
			),
			data: UnitRepr
		}
	}
}

impl<D: BitValue> BitValue for DescriptorItem<D>
where
	ReprTypePlus8Value: ReprTypePlus8<D::BitRepr>,
	ItemSize: ItemSizeFromReprType<D::BitRepr>
{
	type BitRepr = <ReprTypePlus8Value as ReprTypePlus8<D::BitRepr>>::ReprTypePlus8;

	const MASK: Self::BitRepr = Self::BitRepr::MAX;

	fn to_bits(&self) -> Self::BitRepr {
		BitvalueField::<Self>::to_parent_bits(&self.prefix)
			| BitvalueField::<Self>::to_parent_bits(DescriptorItemDataWrap::from_ref(&self.data))
	}

	fn from_bits(bits: Self::BitRepr) -> Option<Self> {
		let prefix: ItemPrefix = BitvalueField::<Self>::from_parent_bits(bits)?;
		let data: DescriptorItemDataWrap<D> = BitvalueField::<Self>::from_parent_bits(bits)?;

		Some(Self {
			prefix,
			data: data.0
		})
	}
}
unsafe impl<D: BitValue> BitvalueField<DescriptorItem<D>> for ItemPrefix
where
	ReprTypePlus8Value: ReprTypePlus8<D::BitRepr>,
	ItemSize: ItemSizeFromReprType<D::BitRepr>
{
	const SHIFT: usize = 0;
}
unsafe impl<D: BitValue> BitvalueField<DescriptorItem<D>> for DescriptorItemDataWrap<D>
where
	ReprTypePlus8Value: ReprTypePlus8<D::BitRepr>,
	ItemSize: ItemSizeFromReprType<D::BitRepr>
{
	const SHIFT: usize = 8;
}

mod sealed {
	pub trait Sealed {}

	impl Sealed for super::items::ItemSize {}
	impl Sealed for super::ReprTypePlus8Value {}
}

#[repr(transparent)]
struct DescriptorItemDataWrap<D>(pub D);
impl<D> DescriptorItemDataWrap<D> {
	pub fn from_ref(d: &D) -> &Self {
		// SAFETY: Completely safe, we are repr(transparent)
		unsafe { &*(d as *const _ as *const Self) }
	}
}
impl<D: BitValue> BitValue for DescriptorItemDataWrap<D> {
	type BitRepr = D::BitRepr;

	const MASK: Self::BitRepr = D::MASK;

	fn to_bits(&self) -> Self::BitRepr {
		self.0.to_bits()
	}

	fn from_bits(bits: Self::BitRepr) -> Option<Self> {
		D::from_bits(bits).map(Self)
	}
}

#[doc(hidden)]
pub trait ItemSizeFromReprType<R: ReprType>: Sized + sealed::Sealed {
	const DATA_ITEM_SIZE: Self;
}
impl ItemSizeFromReprType<bitregister::prelude::UnitRepr> for ItemSize {
	const DATA_ITEM_SIZE: Self = ItemSize::Bytes0;
}
impl ItemSizeFromReprType<u8> for ItemSize {
	const DATA_ITEM_SIZE: Self = ItemSize::Bytes1;
}
impl ItemSizeFromReprType<u16> for ItemSize {
	const DATA_ITEM_SIZE: Self = ItemSize::Bytes2;
}
impl ItemSizeFromReprType<u32> for ItemSize {
	const DATA_ITEM_SIZE: Self = ItemSize::Bytes4;
}

#[doc(hidden)]
pub trait ReprTypePlus8<R: ReprType>: sealed::Sealed {
	type ReprTypePlus8: ReprType + From<R> + TryInto<R> + From<u8> + TryInto<u8>;
}
#[doc(hidden)]
pub struct ReprTypePlus8Value;
impl ReprTypePlus8<bitregister::prelude::UnitRepr> for ReprTypePlus8Value {
	type ReprTypePlus8 = u8;
}
impl ReprTypePlus8<u8> for ReprTypePlus8Value {
	type ReprTypePlus8 = u16;
}
impl ReprTypePlus8<u16> for ReprTypePlus8Value {
	type ReprTypePlus8 = bitregister::prelude::u24;
}
impl ReprTypePlus8<u32> for ReprTypePlus8Value {
	type ReprTypePlus8 = bitregister::prelude::u40;
}
