pub enum UsagePageUndefined {}
impl UsagePageUndefined {
	pub const PAGE: u16 = 0x00;
}

#[repr(u16)]
pub enum UsagePageGenericDesktop {
	Undefined = 0x00,
	Pointer,
	Mouse,
	Joystick = 0x04,
	Gamepad,
	Keyboard,
	Keypad,
	X = 0x30,
	Y = 0x31,
	Z = 0x32,
	Wheel = 0x38,
	SystemControl = 0x80
}
impl UsagePageGenericDesktop {
	pub const PAGE: u16 = 0x01;
}

#[repr(u16)]
pub enum UsagePageSimulationControls {
	Undefined = 0x00
}
impl UsagePageSimulationControls {
	pub const PAGE: u16 = 0x02;
}

#[repr(u16)]
pub enum UsagePageVrControls {
	Undefined = 0x00
}
impl UsagePageVrControls {
	pub const PAGE: u16 = 0x03;
}

#[repr(u16)]
pub enum UsagePageSportControls {
	Undefined = 0x00
}
impl UsagePageSportControls {
	pub const PAGE: u16 = 0x04;
}

#[repr(u16)]
pub enum UsagePageGameControls {
	Undefined = 0x00
}
impl UsagePageGameControls {
	pub const PAGE: u16 = 0x05;
}

#[repr(u16)]
pub enum UsagePageGenericDeviceControls {
	Undefined = 0x00
}
impl UsagePageGenericDeviceControls {
	pub const PAGE: u16 = 0x06;
}

#[repr(u16)]
pub enum UsagePageKeyboard {
	ErrorRollOver = 0x01,
	POSTFail = 0x02,
	ErrorUndefined = 0x03,
	A = 0x04,
	KeypadHexadecimal = 0xDD,
	LeftControl = 0xE0,
	LeftShift,
	LeftAlt,
	LeftGui,
	RightControl,
	RightShift,
	RightAlt,
	RightGui
}
impl UsagePageKeyboard {
	pub const PAGE: u16 = 0x07;
}

#[repr(u16)]
pub enum UsagePageLed {
	Undefined = 0x00,
	NumLock,
	CapsLock,
	ScrollLock,
	Compose,
	Kana,
	Shift,
	Power,
	DoNotDisturb,
	Mute,
	BatteryOperation = 0x1B,
	BatteryOk = 0x1C,
	BatteryLow = 0x1D,
	StandBy = 0x27,
	Busy = 0x2C,
	Ready = 0x2D
}
impl UsagePageLed {
	pub const PAGE: u16 = 0x08;
}

#[repr(u16)]
pub enum UsagePageButton {
	None = 0x00,
	Button1,
	Button2,
	Button3,
	Button4,
	Button5,
	Button6,
	Button7,
	Button8
}
impl UsagePageButton {
	pub const PAGE: u16 = 0x09;
}

pub enum UsagePageOrdinal {}
impl UsagePageOrdinal {
	pub const PAGE: u16 = 0x0A;
}

#[repr(u16)]
pub enum UsagePageTelephonyDevice {
	Undefined = 0x00
}
impl UsagePageTelephonyDevice {
	pub const PAGE: u16 = 0x0B;
}

#[repr(u16)]
pub enum UsagePageConsumer {
	Undefined = 0x00,
	ConsumerControl
}
impl UsagePageConsumer {
	pub const PAGE: u16 = 0x0C;
}

#[repr(u16)]
pub enum UsagePageDigitizers {
	Undefined = 0x00
}
impl UsagePageDigitizers {
	pub const PAGE: u16 = 0x0D;
}
