# STM32F407 USB HID mouse + keyboard

## Structure

The project is separated into multiple Rust crates (packages).

### `docs`

Contains datasheets, example projects and user manuals for hardware used. Also contains assignment reports.

### `bitregister`

Defines abstraction around values and registers which are maniapulated on bit level.

### `ili9325`

Driver for ILI9325 touchscreen display.

### `stmpe811`

Driver for STMPE811 IO expander.

### `usb_hid`

USB HID class implementation for `usb_device` community crate.

### `touch_keyboard`

The main application, uses rtic framework to handle task scheduling.

### `helloworld`

Quick and dirty playground project for testing.

## Building & Running

The project requires the host system to have:
* stable Rust (https://rustup.rs/)
* ARM target (`rustup target add thumbv7em-none-eabihf`)
* flip-link (`cargo install flip-link`)
* probe-run (`cargo install probe-run`)
* [optional] nightly Rust for formatting (`rustup toolchain install nightly`) - to format: `cargo +nightly fmt`

To flash the binary and run the app using STLink:

```
cargo run --bin touch_keyboard --release
```

By default, the project runs in semihosting environment where the board can pass messages back to the host. To disable semihosting compilation disable the feature `semihosting` in `touch_keyboard` crate.